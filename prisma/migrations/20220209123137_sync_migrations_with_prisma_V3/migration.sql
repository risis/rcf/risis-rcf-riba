-- DropForeignKey
ALTER TABLE `job` DROP FOREIGN KEY `job_ibfk_4`;

-- DropForeignKey
ALTER TABLE `job` DROP FOREIGN KEY `job_ibfk_2`;

-- DropForeignKey
ALTER TABLE `job` DROP FOREIGN KEY `job_ibfk_3`;

-- DropForeignKey
ALTER TABLE `job` DROP FOREIGN KEY `job_ibfk_1`;

-- DropForeignKey
ALTER TABLE `notification` DROP FOREIGN KEY `notification_ibfk_1`;

-- DropForeignKey
ALTER TABLE `project` DROP FOREIGN KEY `project_ibfk_1`;

-- DropForeignKey
ALTER TABLE `project` DROP FOREIGN KEY `project_ibfk_2`;

-- DropForeignKey
ALTER TABLE `project` DROP FOREIGN KEY `project_ibfk_3`;

-- DropForeignKey
ALTER TABLE `service_provider_credentials` DROP FOREIGN KEY `service_provider_credentials_ibfk_1`;

-- AddForeignKey
ALTER TABLE `project` ADD CONSTRAINT `project_author_id_fkey` FOREIGN KEY (`author_id`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `project` ADD CONSTRAINT `project_main_scenario_id_fkey` FOREIGN KEY (`main_scenario_id`) REFERENCES `scenario`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `project` ADD CONSTRAINT `project_state_id_fkey` FOREIGN KEY (`state_id`) REFERENCES `state`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `job` ADD CONSTRAINT `job_author_id_fkey` FOREIGN KEY (`author_id`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `job` ADD CONSTRAINT `job_project_id_fkey` FOREIGN KEY (`project_id`) REFERENCES `project`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `job` ADD CONSTRAINT `job_scenario_id_fkey` FOREIGN KEY (`scenario_id`) REFERENCES `scenario`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `job` ADD CONSTRAINT `job_state_id_fkey` FOREIGN KEY (`state_id`) REFERENCES `state`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `service_provider_credentials` ADD CONSTRAINT `service_provider_credentials_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `notification` ADD CONSTRAINT `notification_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- RenameIndex
ALTER TABLE `keyword` RENAME INDEX `keyword.value_unique` TO `keyword_value_key`;

-- RenameIndex
ALTER TABLE `state` RENAME INDEX `state.slug_unique` TO `state_slug_key`;

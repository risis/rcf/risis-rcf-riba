/*
  Warnings:

  - The primary key for the `ar_access_request` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to alter the column `id` on the `ar_access_request` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.
  - You are about to alter the column `access_request_id` on the `ar_access_request_dataset` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Int`.

*/
-- DropForeignKey
ALTER TABLE `ar_access_request_dataset` DROP FOREIGN KEY `ar_access_request_dataset_access_request_id_fkey`;

-- AlterTable
ALTER TABLE `ar_access_request` DROP PRIMARY KEY,
    MODIFY `id` INTEGER NOT NULL AUTO_INCREMENT,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `ar_access_request_dataset` MODIFY `access_request_id` INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE `ar_access_request_dataset` ADD CONSTRAINT `ar_access_request_dataset_access_request_id_fkey` FOREIGN KEY (`access_request_id`) REFERENCES `ar_access_request`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

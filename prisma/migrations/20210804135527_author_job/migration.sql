/*
  Warnings:

  - Added the required column `author_id` to the `job` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `job` ADD COLUMN `author_id` VARCHAR(191) NOT NULL;

-- AddForeignKey
ALTER TABLE `job` ADD FOREIGN KEY (`author_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

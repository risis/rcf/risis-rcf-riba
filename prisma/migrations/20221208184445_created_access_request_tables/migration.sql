-- CreateTable
CREATE TABLE `ar_dataset` (
    `id` INTEGER NOT NULL,
    `label` VARCHAR(191) NOT NULL,
    `description` MEDIUMTEXT NOT NULL,
    `keywords` MEDIUMTEXT NULL,
    `use_case` MEDIUMTEXT NULL,
    `dataset_owner` VARCHAR(191) NOT NULL,
    `dataset_access_manager` VARCHAR(191) NULL,
    `short_description` MEDIUMTEXT NOT NULL,
    `documentation` MEDIUMTEXT NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `ar_access_request` (
    `id` INTEGER NOT NULL,
    `project_title` MEDIUMTEXT NOT NULL,
    `project_acronym` MEDIUMTEXT NOT NULL,
    `project_objectives` MEDIUMTEXT NOT NULL,
    `project_summary` MEDIUMTEXT NOT NULL,
    `data_needed` MEDIUMTEXT NULL,
    `user_id` VARCHAR(191) NOT NULL,
    `step` INTEGER NOT NULL DEFAULT 0,
    `first_question_am` VARCHAR(191) NULL,
    `second_question_am` VARCHAR(191) NULL,
    `third_question_am` VARCHAR(191) NULL,
    `forth_question_am` VARCHAR(191) NULL,
    `decision_am` VARCHAR(191) NULL,
    `reasoning_am` MEDIUMTEXT NULL,
    `first_question_prb` VARCHAR(191) NULL,
    `second_question_prb` VARCHAR(191) NULL,
    `third_question_prb` VARCHAR(191) NULL,
    `forth_question_prb` VARCHAR(191) NULL,
    `decision_prb` VARCHAR(191) NULL,
    `reasoning_prb` MEDIUMTEXT NOT NULL,
    `decision_pc` VARCHAR(191) NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL,
    `cv_url` MEDIUMTEXT NOT NULL,
    `cancelled` INTEGER NOT NULL DEFAULT 0,
    `is_submitted` INTEGER NOT NULL DEFAULT 0,
    `should_be_revised` INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `ar_access_request_dataset` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `access_request_id` INTEGER NOT NULL,
    `dataset_id` INTEGER NOT NULL,
    `access_type` VARCHAR(191) NOT NULL,
    `host_location` VARCHAR(191) NULL,
    `preferred_visit_dates` VARCHAR(191) NULL,
    `re_imbursement` VARCHAR(191) NULL,
    `is_main` INTEGER NOT NULL DEFAULT 0,
    `decision` INTEGER NOT NULL,
    `decision_date` DATETIME(3) NOT NULL,
    `am_email` VARCHAR(191) NOT NULL,
    `description` MEDIUMTEXT NOT NULL,
    `is_cancelled` INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

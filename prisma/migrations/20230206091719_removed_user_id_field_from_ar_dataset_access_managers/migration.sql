/*
  Warnings:

  - You are about to drop the column `user_id` on the `ar_dataset_access_managers` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE `ar_dataset_access_managers` DROP FOREIGN KEY `ar_dataset_access_managers_user_id_fkey`;

-- AlterTable
ALTER TABLE `ar_dataset_access_managers` DROP COLUMN `user_id`;

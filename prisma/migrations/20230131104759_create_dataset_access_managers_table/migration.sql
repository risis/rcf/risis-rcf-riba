-- AlterTable
ALTER TABLE `ar_access_request` ADD COLUMN `am_email_sent` INTEGER NOT NULL DEFAULT 0,
    ADD COLUMN `pc_email_sent` INTEGER NOT NULL DEFAULT 0,
    ADD COLUMN `prb_email_sent` INTEGER NOT NULL DEFAULT 0;

-- AlterTable
ALTER TABLE `ar_access_request_dataset` ADD COLUMN `email_sent` INTEGER NOT NULL DEFAULT 0,
    ADD COLUMN `email_sent_at` DATETIME(3) NULL;

-- CreateTable
CREATE TABLE `ar_dataset_access_managers` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `dataset_id` INTEGER NOT NULL,
    `user_id` VARCHAR(191) NOT NULL,
    `user_email` VARCHAR(191) NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `ar_dataset_access_managers` ADD CONSTRAINT `ar_dataset_access_managers_dataset_id_fkey` FOREIGN KEY (`dataset_id`) REFERENCES `ar_dataset`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `ar_dataset_access_managers` ADD CONSTRAINT `ar_dataset_access_managers_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

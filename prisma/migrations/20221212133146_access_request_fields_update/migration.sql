-- AlterTable
ALTER TABLE `ar_access_request` MODIFY `reasoning_prb` MEDIUMTEXT NULL,
    MODIFY `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3);

-- AlterTable
ALTER TABLE `ar_access_request_dataset` MODIFY `decision` INTEGER NULL,
    MODIFY `decision_date` DATETIME(3) NULL,
    MODIFY `description` MEDIUMTEXT NULL;

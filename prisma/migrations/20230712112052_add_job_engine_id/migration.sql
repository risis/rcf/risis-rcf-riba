/*
  Warnings:

  - Added the required column `engine_id` to the `job` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `job` ADD COLUMN `engine_id` VARCHAR(191) NOT NULL;

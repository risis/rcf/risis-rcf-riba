-- AddForeignKey
ALTER TABLE `ar_access_request_dataset` ADD CONSTRAINT `ar_access_request_dataset_access_request_id_fkey` FOREIGN KEY (`access_request_id`) REFERENCES `ar_access_request`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

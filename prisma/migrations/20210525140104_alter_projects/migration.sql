/*
  Warnings:

  - Made the column `state_id` on table `project` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE `project` DROP FOREIGN KEY `project_ibfk_3`;

-- AlterTable
ALTER TABLE `project` MODIFY `state_id` INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE `project` ADD FOREIGN KEY (`state_id`) REFERENCES `state`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

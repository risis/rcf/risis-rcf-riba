-- CreateTable
CREATE TABLE `notification` (
    `id` VARCHAR(191) NOT NULL,
    `title` VARCHAR(191) NOT NULL,
    `details` VARCHAR(191),
    `url` VARCHAR(191),
    `type` VARCHAR(191) NOT NULL DEFAULT 'info',
    `seen` BOOLEAN NOT NULL DEFAULT false,
    `reason` VARCHAR(191),
    `user_id` VARCHAR(191) NOT NULL,
    `creation_datetime` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `delete_datetime` DATETIME(3),

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `notification` ADD FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

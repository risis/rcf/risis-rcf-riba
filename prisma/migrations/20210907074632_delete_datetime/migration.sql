-- AlterTable
ALTER TABLE `job` ADD COLUMN `delete_datetime` DATETIME(3);

-- AlterTable
ALTER TABLE `project` ADD COLUMN `delete_datetime` DATETIME(3);

-- AlterTable
ALTER TABLE `service_provider_credentials` ADD COLUMN `delete_datetime` DATETIME(3);

-- AlterTable
ALTER TABLE `user` ADD COLUMN `creation_datetime` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    ADD COLUMN `delete_datetime` DATETIME(3);

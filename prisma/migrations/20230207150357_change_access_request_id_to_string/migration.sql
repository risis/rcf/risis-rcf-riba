/*
  Warnings:

  - The primary key for the `ar_access_request` table will be changed. If it partially fails, the table could be left without primary key constraint.

*/
-- DropForeignKey
ALTER TABLE `ar_access_request_dataset` DROP FOREIGN KEY `ar_access_request_dataset_access_request_id_fkey`;

-- AlterTable
ALTER TABLE `ar_access_request` DROP PRIMARY KEY,
    MODIFY `id` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- AlterTable
ALTER TABLE `ar_access_request_dataset` MODIFY `access_request_id` VARCHAR(191) NOT NULL;

-- AddForeignKey
ALTER TABLE `ar_access_request_dataset` ADD CONSTRAINT `ar_access_request_dataset_access_request_id_fkey` FOREIGN KEY (`access_request_id`) REFERENCES `ar_access_request`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

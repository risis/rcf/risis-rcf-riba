-- AddForeignKey
ALTER TABLE `ar_access_request_dataset` ADD CONSTRAINT `ar_access_request_dataset_dataset_id_fkey` FOREIGN KEY (`dataset_id`) REFERENCES `ar_dataset`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

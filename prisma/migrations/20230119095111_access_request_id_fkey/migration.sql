-- AddForeignKey
ALTER TABLE `ar_access_request` ADD CONSTRAINT `ar_access_request_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- CreateTable
CREATE TABLE `presets_query_factory` (
    `id` VARCHAR(191) NOT NULL,
    `title` VARCHAR(191) NOT NULL,
    `description` VARCHAR(191) NOT NULL,
    `query` MEDIUMTEXT NOT NULL,
    `user_id` VARCHAR(191) NOT NULL,
    `scenario_id` VARCHAR(191) NOT NULL,
    `creation_datetime` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `delete_datetime` DATETIME(3) NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `presets_query_factory` ADD CONSTRAINT `presets_query_factory_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `presets_query_factory` ADD CONSTRAINT `presets_query_factory_scenario_id_fkey` FOREIGN KEY (`scenario_id`) REFERENCES `scenario`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

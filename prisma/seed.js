const { PrismaClient } = require('@prisma/client');
const slugify = require('slugify');
const prisma = new PrismaClient();

async function main () {
  const titles = ['Not ready', 'Ready', 'Pending', 'Running', 'Stopping', 'Stopped', 'Shutting down', 'Finished'];
  for (let i = 0; i < titles.length; i++) {
    const title = titles[i];
    const slug = slugify(title, { lower: true });
    await prisma.state.upsert({
      where: {
        slug
      },
      update: {
        title
      },
      create: {
        slug,
        title
      }
    });
  }
}

async function init () {
  try {
    await main();
  } catch (e) {
    console.error(e);
    process.exit(1);
  } finally {
    await prisma.$disconnect();
  }
}

init();

## Summary

<!-- A concise, to the point description of the request -->

## Context

<!--
  A more elaborate statement on the feature or enhancement, perhaps with screenshots, that helps make the request clear.
-->

## Why

<!--
  Why is the feature or enhancement proposed?

  i.e.
  - General user request
  - Better usability
  - A clearer interface
  - ...

  Inconveniences caused by the feature not being there, if any.

-->

## Possible approaches

<!--
  If possible, suggest some resources or examples on how it could be resolved.
-->

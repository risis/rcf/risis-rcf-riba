const express = require('express');
const bodyParser = require('body-parser');
const notificationController = require('../../controllers/notification');

const {
  validate,
  addContextEntity,
  parsePaginationParams,
  parseSearchParams
} = require('../middleware');

const router = express.Router();

router.get(
  '/',
  validate,
  addContextEntity('notification'),
  parsePaginationParams,
  parseSearchParams,
  async (req, res) => {
    const result = await notificationController.listAllUserNotifications(req);
    res.json(result);
  });

router.delete('/:notificationId', validate, async (req, res) => {
  const result = await notificationController.deleteNotification(req);
  res.json(result);
});

router.post('/', bodyParser.json(), validate, async (req, res) => {
  const result = await notificationController.createNotification(req);
  res.json(result);
});

router.patch('/seen', validate, async (req, res) => {
  const result = await notificationController.notificationSeen(req);
  res.json(result);
});

module.exports = router;

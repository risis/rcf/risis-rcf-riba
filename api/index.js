const express = require('express');
const bearerToken = require('express-bearer-token');
const log4js = require('log4js');
const { v4: uuid } = require('uuid');
const userController = require('../controllers/users');
const projects = require('./projects');
const datasets = require('./datasets');
const scenarios = require('./scenarios');
const accessRequests = require('./access_requests');
const openAccess = require('./open_access');
const jobs = require('./jobs');
const notifications = require('./notifications');
const users = require('./users');
const jwt = require('jsonwebtoken');
const errors = require('../utils/errors');
const {
  setDatastoreClient
} = require('./middleware');
const keycloak = require('../utils/keycloak');

async function authMiddleware (req, res, next) {
  const { token, context: { logger } } = req;
  if (token === undefined) {
    throw new errors.UnauthenticatedError('No token in the request');
  } else {
    let tokenDecoded;
    try {
      const keycloakPublicKey = await keycloak.getPublicKey();
      tokenDecoded = jwt.verify(req.token, keycloakPublicKey, { algorithms: ['RS256'] });
    } catch (error) {
      logger.error(error);
      throw new errors.UnauthenticatedError(error.message || 'Error verifying the token');
    }
    tokenDecoded.id = tokenDecoded.sub;
    req.tokenDecoded = tokenDecoded;
    const roles = tokenDecoded.realm_access.roles;
    if (roles && !roles.includes('Service')) {
      req.isService = false;
      req.user = await userController.getOrCreateUser(req);
      if (req.user.jti !== tokenDecoded.jti) {
        req.user = await userController.updateUserInfo(req);
      }
    } else {
      req.user = {
        id: tokenDecoded.id,
        username: `Service ${tokenDecoded.clientId || ''}`
      };
      req.isService = true;
    }
    next();
  }
}

async function decodeDatasetId (req, res, next) {
  if (req.params.datasetId) {
    req.params.datasetId = decodeURIComponent(req.params.datasetId);
  }
  next();
}

// Should handle request related context to pass around in the internals and annotate the response
// - UUID for logging:
//    important for tracing a request's lifetime and debugging
function contextHandler (req, res, next) {
  // Check if request comes with an UUID (header)
  const context = {};
  if (req.header('X-Request-Id')) {
    context.traceid = req.header('X-Request-Id');
  } else {
    context.traceid = uuid();
  }
  // Add a default logger instance to context
  const defaultLogger = log4js.getLogger('default');
  defaultLogger.addContext('traceid', context.traceid);
  context.logger = defaultLogger;

  // Add traceid as a response header
  res.set('X-Request-Id', context.traceid);

  req.context = context;
  next();
}

function requestLogger (req, res, next) {
  const { method, url, context: { logger } } = req;

  // Set response logging
  res.on('finish', () => {
    logger.info(res.statusCode);
  });

  // Log request method and url
  logger.info(method + ' ' + url);

  next();
}

const router = express.Router();

router.use(contextHandler, requestLogger);

router.use('/projects', bearerToken(), authMiddleware, decodeDatasetId, projects);
router.use('/datasets', bearerToken(), authMiddleware, setDatastoreClient, decodeDatasetId, datasets);
router.use('/scenarios', bearerToken(), authMiddleware, scenarios);
router.use('/jobs', bearerToken(), authMiddleware, jobs);
router.use('/notifications', bearerToken(), authMiddleware, notifications);
router.use('/users', bearerToken(), authMiddleware, users);
router.use('/access_requests', bearerToken(), authMiddleware, accessRequests);
router.use('/open_access', openAccess);

router.use(errors.middleware.errorHandler);

module.exports = (app) => {
  app.set('json replacer', (key, value) => (value === null ? undefined : value));
  app.use('/api', router);
};

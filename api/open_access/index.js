const express = require('express');
const accessRequestController = require('../../controllers/access_request');

const router = express.Router();

router.get(
  '/datasets',
  async (req, res) => {
    const result = await accessRequestController.listAllDatasets(req);
    res.json(result);
  }
);

router.get(
  '/datasets/:id/metadata',
  async (req, res) => {
    const result = await accessRequestController.getDataset(req);
    res.json(result);
  }
);

module.exports = router;

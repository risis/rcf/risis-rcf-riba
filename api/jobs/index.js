const express = require('express');
const bodyParser = require('body-parser');

const jobController = require('../../controllers/job');

const {
  validate,
  addContextEntity,
  parsePaginationParams,
  parseSearchParams
} = require('../middleware');

const router = express.Router();

router.patch(
  '/:jobId',
  bodyParser.json(),
  validate, async (req, res) => {
    const result = await jobController.updateJob(req);
    res.json(result);
  });

router.get(
  '/',
  validate,
  addContextEntity('job'),
  parsePaginationParams,
  parseSearchParams,
  async (req, res) => {
    const files = await jobController.getJobs(req);
    res.json(files);
  });

router.get('/:jobId', validate, async (req, res) => {
  const result = await jobController.getSingleJob(req);
  res.json(result);
});

router.delete(
  '/:jobId',
  validate,
  async (req, res) => {
    const result = await jobController.deleteJob(req);
    res.json(result);
  });

/**
 * POST /jobs/reset
 * Sets all "running" jobs as failed.
 * Part of RISIS week patch due to random orchestrator crashes.
 * Should be removed once orchestrator is stable.
 */
router.post(
  '/reset',
  bodyParser.json(),
  validate,
  async (req, res) => {
    await jobController.resetRunning(req);
    res.send();
  });

router.get('/:jobId/config', validate, async (req, res) => {
  const result = await jobController.getSpecificJobConfig(req);
  res.json(result);
});

router.patch('/:jobId/config', bodyParser.json(), validate, async (req, res) => {
  const result = await jobController.restoreJobConfig(req);
  res.json(result);
});

module.exports = router;

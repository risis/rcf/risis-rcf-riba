const express = require('express');
const bodyParser = require('body-parser');
const projectController = require('../../controllers/projects');
const scenarioController = require('../../controllers/scenario');
const accessRequestController = require('../../controllers/access_request');
const errors = require('../../utils/errors');

const {
  validate,
  parsePaginationParams,
  parseSearchParams,
  addContextEntity,
  setDatastoreClient
} = require('../middleware');

async function updateConfigScenario (req) {
  // Update configuration if this endpoint is called by rest API
  const bodyRunScenario = req.body;
  let objMerged = {};
  Object.keys(req.body.params).forEach(key => {
    objMerged = {
      ...objMerged,
      ...req.body.params[key]
    };
  });
  req.body = objMerged;
  await projectController.updateConfigScenario(req);
  return bodyRunScenario;
}

const router = express.Router();

// ------------------ Datasets ------------------

router.get(
  '/:projectId/datasets',
  validate,
  addContextEntity('project'),
  setDatastoreClient,
  async (req, res) => {
    const result = await projectController.getDatasetsByProject(req);
    res.json(result);
  });

router.get(
  '/datasets',
  validate,
  async (req, res) => {
    const result = await projectController.getDatasetsByProject(req);
    res.json(result);
  }
);

router.put(
  '/:projectId/datasets/:datasetId',
  bodyParser.json(),
  validate,
  setDatastoreClient,
  async (req, res) => {
    const result = await projectController.importDataset(req);
    res.json(result);
  });

router.delete(
  '/:projectId/datasets/:datasetId',
  bodyParser.json(),
  validate,
  setDatastoreClient,
  async (req, res) => {
    const result = await projectController.unlinkDataset(req);
    res.json(result);
  });

router.get(
  '/',
  validate,
  addContextEntity('project'),
  parsePaginationParams,
  parseSearchParams,
  async (req, res) => {
    const result = await projectController.listAllUserProjects(req);
    res.json(result);
  });

router.post('/', bodyParser.json(), validate, async (req, res) => {
  const result = await projectController.createProject(req);
  res.json(result);
});

router.get('/:projectId', validate, async (req, res) => {
  const result = await projectController.getSingleProject(req);
  res.json(result);
});

router.get('/:projectId/scenarios', validate, async (req, res) => {
  const result = await projectController.getScenariosByProject(req);
  res.json(result);
});

router.get('/:projectId/jobs', validate, async (req, res) => {
  const result = await projectController.getJobsByProject(req);
  res.json(result);
});

router.patch('/:projectId', bodyParser.json(), validate, async (req, res) => {
  const result = await projectController.patchProject(req);
  res.json(result);
});

router.delete('/:projectId', validate, async (req, res) => {
  const result = await projectController.deleteProject(req);
  res.json(result);
});

router.get('/:projectId/scenarios/:scenarioId', validate, async (req, res) => {
  const result = await projectController.getScenarioFromProject(req);
  res.json(result);
});

router.put('/:projectId/scenarios/:scenarioId', validate, async (req, res) => {
  const result = await projectController.importScenario(req);
  res.json(result);
});

router.post(
  '/:projectId/scenarios/:scenarioId/actions',
  validate,
  bodyParser.json(),
  async (req, res) => {
    const result = await projectController.processScenarioAction(req);
    res.json(result);
  });

router.delete('/:projectId/scenarios/:scenarioId', validate, async (req, res) => {
  const result = await projectController.unlinkScenario(req);
  res.json(result);
});

router.get('/:projectId/scenarios/:scenarioId/config', validate, async (req, res) => {
  const result = await projectController.getConfigScenario(req);
  res.json(result);
});

router.patch('/:projectId/scenarios/:scenarioId/config', bodyParser.json(), validate, async (req, res) => {
  const result = await projectController.updateConfigScenario(req);
  res.json(result);
});

router.patch('/:projectId/scenarios/:scenarioId/job', bodyParser.json(), validate, async (req, res) => {
  const result = await projectController.changeTitleLastScenarioJob(req);
  res.json(result);
});

router.post('/:projectId/run',
  bodyParser.json(),
  validate,
  async (req, res) => {
    req.body = await updateConfigScenario(req);
    if (!req.project.main_scenario) {
      throw new errors.ArgumentError('No main scenario for running');
    }
    req.params.scenarioId = req.project.main_scenario.id;
    const result = await scenarioController.runScenario(req);
    res.json(result);
  });

router.post(
  '/:projectId/scenarios/:scenarioId/run',
  bodyParser.json(),
  validate,
  async (req, res) => {
    req.body = await updateConfigScenario(req);
    const result = await scenarioController.runScenario(req);
    res.json(result);
  });

router.get('/:projectId/jobs/:jobId/outputs', async (req, res) => {
  const response = await projectController.downloadOutputs(req);
  res.attachment(req.project.title + '.zip').type('zip');
  response.on('end', () => res.end());
  response.pipe(res);
});

router.get(
  '/datasets',
  validate,
  async (req, res) => {
    const result = await accessRequestController.listAllDatasets(req);
    res.json(result);
  }
);

module.exports = router;

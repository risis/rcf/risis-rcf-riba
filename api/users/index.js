const express = require('express');
const bodyParser = require('body-parser');
const userController = require('../../controllers/users');

const {
  validate,
  addContextEntity,
  setDatastoreClient
} = require('../middleware');

const router = express.Router();

router.get(
  '/stats',
  validate,
  addContextEntity('user'),
  setDatastoreClient,
  async (req, res) => {
    const result = await userController.stats(req);
    res.json(result);
  });

router.get(
  '/profile/:userId',
  validate,
  addContextEntity('user'),
  async (req, res) => {
    const result = await userController.getUserInformation(req);
    delete result.service_providers;
    res.json(result);
  });

router.get(
  '/profile/email/:userId',
  validate,
  addContextEntity('user'),
  async (req, res) => {
    const result = await userController.getUserInformation(req, 'email');
    delete result.service_providers;
    res.json(result);
  });

router.get(
  '/profile',
  validate,
  addContextEntity('user'),
  async (req, res) => {
    delete req.user.service_providers;
    res.json(req.user);
  });

router.post(
  '/service/credentials',
  bodyParser.json(),
  validate,
  addContextEntity('user'),
  async (req, res) => {
    const user = await userController.createServiceCredentials(req);
    delete user.service_providers;
    res.json(user);
  });

router.patch(
  '/service/credentials/:serviceId',
  bodyParser.json(),
  validate,
  addContextEntity('user'),
  async (req, res) => {
    const user = await userController.updateServiceCredentials(req);
    delete user.service_providers;
    res.json(user);
  });

module.exports = router;

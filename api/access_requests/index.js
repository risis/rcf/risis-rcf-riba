const express = require('express');
const accessRequestController = require('../../controllers/access_request');
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer({ dest: 'data/uploads/' });
const appRoot = require('app-root-path').toString();

const {
  validate
} = require('../middleware');

const router = express.Router();

router.post(
  '/new',
  validate,
  bodyParser.json(),
  async (req, res) => {
    const result = await accessRequestController.createAccessRequest(req);
    res.json(result);
  }
);

router.post(
  '/:id/cv',
  validate,
  upload.array('files'),
  bodyParser.json(),
  async (req, res) => {
    const result = await accessRequestController.uploadCv(req);
    res.json(result);
  }
);

router.get(
  '/:id/cv',
  async (req, res) => {
    accessRequestController.downloadCv(req).then(filePath => {
      const path = `${appRoot}/${filePath}`;
      res.download(path);
    });
  }
);

router.post(
  '/:id/evaluation/submit',
  validate,
  bodyParser.json(),
  async (req, res) => {
    const result = await accessRequestController.submitEvaluation(req);
    res.json(result);
  }
);

router.get(
  '/evaluations',
  validate,
  async (req, res) => {
    const result = await accessRequestController.listEvaluations(req);
    res.json(result);
  }
);

router.get(
  '/all',
  validate,
  async (req, res) => {
    const result = await accessRequestController.getAllAccessRequests(req);
    res.json(result);
  }
);

router.get(
  '/my',
  validate,
  async (req, res) => {
    const result = await accessRequestController.getMyAccessRequests(req);
    res.json(result);
  }
);

router.get(
  '/:id/data',
  validate,
  async (req, res) => {
    const result = await accessRequestController.getAccessRequestData(req);
    res.json(result);
  }
);

router.get(
  '/:id/evaluation',
  validate,
  async (req, res) => {
    const result = await accessRequestController.getEvaluationData(req);
    res.json(result);
  }
);

router.post(
  '/:id/revise',
  validate,
  bodyParser.json(),
  async (req, res) => {
    const result = await accessRequestController.submitRevision(req);
    res.json(result);
  }
);

router.post(
  '/:id/cancel',
  validate,
  async (req, res) => {
    const result = await accessRequestController.cancelAccessRequest(req);
    res.json(result);
  }
);

module.exports = router;

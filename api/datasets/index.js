const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer({ dest: 'data/uploads/' });
const datasetController = require('../../controllers/dataset');

const {
  validate,
  addContextEntity
} = require('../middleware');

const router = express.Router();

router.post(
  '/',
  upload.array('files'),
  async (req, res) => {
    const result = await datasetController.createDataset(req);
    res.json(result);
  });

router.get(
  '/',
  validate,
  addContextEntity('dataset'),
  async (req, res) => {
    const result = await datasetController.listAllDatasets(req);
    res.json(result);
  });

router.post(
  '/:datasetId/files',
  upload.array('files'),
  async (req, res) => {
    const files = await datasetController.uploadFiles(req);
    res.json(files);
  });

router.get(
  '/:datasetId/files/:fileId/metadata',
  validate,
  async (req, res) => {
    const result = await datasetController.getFileMetadata(req);
    res.json(result);
  });

router.get(
  '/:datasetId/files',
  async (req, res) => {
    const response = await datasetController.getFiles(req);
    res.setHeader('content-disposition', `attachment; filename="${req.dataset.title}.zip"`);
    res.setHeader('content-type', 'application/zip;charset=UTF-8');
    res.setHeader('x-suggested-filename', `${req.dataset.title}.zip`);
    res.send(response);
  });

router.get('/:datasetId',
  validate,
  async (req, res) => {
    const result = await datasetController.getSingleDataset(req);
    res.json(result);
  });

router.delete(
  '/:datasetId',
  validate,
  async (req, res) => {
    const result = await datasetController.deleteDataset(req);
    res.json(result);
  });

router.patch(
  '/:datasetId',
  bodyParser.json(),
  validate,
  async (req, res) => {
    const result = await datasetController.updateDataset(req);
    res.json(result);
  });

module.exports = router;

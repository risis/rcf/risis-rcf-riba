const express = require('express');

const bodyParser = require('body-parser');
const scenarioController = require('../../controllers/scenario');
const projectController = require('../../controllers/projects');

const {
  validate,
  addContextEntity
} = require('../middleware');

const router = express.Router();

router.get(
  '/',
  validate,
  addContextEntity('scenario'),
  async (req, res) => {
    const result = await scenarioController.listAllScenarios(req);
    res.json(result);
  });

router.get(
  '/:scenarioId/projects',
  validate,
  addContextEntity('project'),
  async (req, res) => {
    const result = await projectController.getProjectsByScenario(req);
    res.json(result);
  });

router.get('/:scenarioId', validate, async (req, res) => {
  const result = await scenarioController.getSingleScenario(req);
  res.json(result);
});

router.get('/:scenarioId/presets', validate, async (req, res) => {
  const result = await scenarioController.getPresetsScenario(req);
  res.json(result);
});

router.post('/:scenarioId/presets', bodyParser.json(), validate, async (req, res) => {
  const result = await scenarioController.createPresetsScenario(req);
  res.json(result);
});

router.delete('/:scenarioId/presets/:presetId', validate, async (req, res) => {
  const result = await scenarioController.deletePresetsScenario(req);
  res.json(result);
});

router.patch('/:scenarioId/presets/:presetId', bodyParser.json(), validate, async (req, res) => {
  const result = await scenarioController.updatePresetsScenario(req);
  res.json(result);
});

module.exports = router;

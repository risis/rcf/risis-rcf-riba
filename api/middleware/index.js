/**
 * Middlewate of pagination and Search query params.
 * @namespace Middleware_Pagination_Search
 */

const errors = require('../../utils/errors');
const validate = require('./validate');
const datastoreService = require('../../services/datastore');

const settings = global.config.settings.api;

/**
 * Add context entity to the request
 * @method
 * @memberof Middleware_Pagination_Search
 * @param {string} entity - Name of entity
 * @param {function} next - Continue function
 */
function addContextEntity (entity) {
  return (req, res, next) => {
    req.context.entity = entity;
    next();
  };
}

/**
 * Validate correct query params to search in each enpoint
 * @method
 * @memberof Middleware_Pagination_Search
 * @param {object} req - Request by User
 * @param {object} res - Response to User
 * @param {function} next - Continue function
 */
function validateSearchParams (req, res, next) {
  if (
    (req.query.match && !['exact', 'regex'].includes(req.query.match)) ||
    (!req.query.search && (req.query.match || req.query.in)) ||
    (req.query.in && req.query.in !== 'all' && !settings.search.fields[req.context.entity].includes(req.query.in))
  ) {
    throw new errors.ArgumentError('Search parameters');
  }
  next();
}

/**
 * Validate correct query params to paginate in each enpoint
 * @method
 * @memberof Middleware_Pagination_Search
 * @param {object} req - Request by User
 * @param {object} res - Response to User
 * @param {function} next - Continue function
 */
function validatePaginationParams (req, res, next) {
  if (
    (req.query.dir && !req.query.order) ||
    (req.query.dir && !['asc', 'desc'].includes(req.query.dir)) ||
    (req.query.order && !settings.orderBy.fields[req.context.entity].includes(req.query.order))
  ) {
    throw new errors.ArgumentError('Paging parameters');
  }
  next();
}

/**
 * Assign pagination to req parameter.
 * @method
 * @memberof Middleware_Pagination_Search
 * @param {object} req - Request by User
 * @param {object} res - Response to User
 * @param {function} next - Continue function
 */
function parsePaginationParams (req, res, next) {
  if (req.query.offset || req.query.limit) {
    req.pagination = {};
    if (req.query.offset) {
      req.pagination.offset = req.query.offset;
    }
    if (req.query.limit) {
      req.pagination.limit = req.query.limit;
    }
  }

  const order = req.query.order || settings.order.field;
  const dir = req.query.dir || settings.order.dir;

  req.order = {
    field: order,
    dir
  };

  next();
}

/**
 * Assign searchFilter to req parameter.
 * @method
 * @memberof Middleware_Pagination_Search
 * @param {object} req - Request by User
 * @param {object} res - Response to User
 * @param {function} next - Continue function
 */
function parseSearchParams (req, res, next) {
  const search = req.query.search;

  let inFiltersEntity = settings.search.fields[req.context.entity];
  if (req.query.in && req.query.in !== 'all') {
    inFiltersEntity = req.query.in;
  }
  req.searchFilter = {
    search,
    match: req.query.match || settings.search.match,
    in: inFiltersEntity
  };
  next();
}

async function setDatastoreClient (req, res, next) {
  req.context.datastoreClient = datastoreService.createUserClient(req);
  next();
}

module.exports = {
  parsePaginationParams: [validatePaginationParams, parsePaginationParams],
  parseSearchParams: [validateSearchParams, parseSearchParams],
  addContextEntity,
  validate,
  setDatastoreClient
};

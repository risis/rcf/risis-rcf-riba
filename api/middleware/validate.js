const path = require('path');
const appRoot = require('app-root-path').toString();
const openApiValidator = require('express-openapi-validator');
const specPath = path.join(appRoot, 'openapi/bundle.yml');
const errors = require('../../utils/errors');

const validationMiddleware = openApiValidator.middleware({
  apiSpec: specPath,
  validateRequests: true,
  validateResponses: true,
  validateSecurity: {
    handlers: {
      BearerAuth: (req, scopes) => {
        const tokenDecoded = req.tokenDecoded;
        if (tokenDecoded.realm_access && tokenDecoded.realm_access.roles) {
          const roles = tokenDecoded.realm_access.roles;
          const scopesLower = scopes.map(s => s.toLowerCase());
          const rolesLower = roles.map(r => r.toLowerCase());
          const hasRol = rolesLower.some(role => scopesLower.includes(role));
          if (hasRol) {
            return true;
          }
          throw new errors.UnauthorizedError('No permissions to access the resource');
        }
      }
    }
  }
});

function serializeDeserialize (req, res, next) {
  const original = res.json;
  res.json = function jsonp (json) {
    return original.call(this, JSON.parse(JSON.stringify(json)));
  };
  next();
}

module.exports = [validationMiddleware, serializeDeserialize];

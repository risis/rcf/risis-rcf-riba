const request = require('supertest');
const chai = require('chai');
const path = require('path');
const chaiResponseValidator = require('chai-openapi-response-validator');
const appRoot = require('app-root-path').toString();

const utils = require('./setup/utils');

const expect = chai.expect;

const specPath = path.join(appRoot, 'openapi/bundle.yml');
// const spec = yaml.parse(fs.readFileSync(specPath, 'utf-8'));
chai.use(chaiResponseValidator(specPath));

const server = require('../../index');

describe('GET /api/projects', () => {
  before(async () => {
    const user = await utils.randomUserPrisma();
    this.token = await utils.getUserToken(user);
  });

  it('Should not be authorized when no bearer token is provided', async () => {
    const res = await request(server)
      .get('/api/projects')
      .expect(401);
    return expect(res).to.satisfyApiSpec;
  });
  it('Should return a list of projects', async () => {
    const res = await request(server)
      .get('/api/projects')
      .set('Authorization', `Bearer ${this.token}`)
      .expect(200);
    return expect(res).to.satisfyApiSpec;
  });
});

const request = require('supertest');
const chai = require('chai');
const path = require('path');
const utils = require('../setup/utils');
const chaiResponseValidator = require('chai-openapi-response-validator');
const appRoot = require('app-root-path').toString();
const expect = chai.expect;
const specPath = path.join(appRoot, 'openapi/bundle.yml');
const nock = require('nock');
const url = require('url');
chai.use(chaiResponseValidator(specPath));

const server = require('../../../index');

describe('Test bad request parameters', () => {
  before(async () => {
    const user = await utils.randomUserPrisma();
    this.token = await utils.getUserToken(user);
    const risorConfig = global.config.services.risor;
    this.scope = nock(url.format(risorConfig.uri)).persist()
      .get('/scenarios')
      .replyWithFile(200, path.join(appRoot, 'test/api/setup/replies/listScenarios.json'), {
        'Content-Type': 'application/json'
      });
  });

  const routes = ['/api/projects', '/api/notifications', '/api/jobs'];
  const orders = ['title', 'title', 'slug'];
  const tests = [];

  for (let i = 0; i < routes.length; i++) {
    const route = routes[i];
    const order = orders[i];
    tests.push({
      name: `Test dis parameter without order parameter (${route})`,
      route: `${route}?dir=asc`,
      statusCode: 400
    });
    tests.push({
      name: `Test dir distinct to asc or desc (${route})`,
      route: `${route}?dir=test&order=id`,
      statusCode: 400
    });
    tests.push({
      name: `Test match distinct to match or regex (${route})`,
      route: `${route}?search=name&match=test`,
      statusCode: 400
    });
    tests.push({
      name: `Test no search with match (${route})`,
      route: `${route}?match=all`,
      statusCode: 400
    });
    tests.push({
      name: `Test no search with in (${route})`,
      route: `${route}?in=name`,
      statusCode: 400
    });
    tests.push({
      name: `Test parameter in is'nt on list of possibilities of configuration on route (${route})`,
      route: `${route}?in=deleted_at`,
      statusCode: 400
    });
    tests.push({
      name: `Test parameter order is'nt on list of possibilities of configuration on route (${route})`,
      route: `${route}?order=deleted_at`,
      statusCode: 400
    });

    // Test with status code 200

    tests.push({
      name: `Test dis parameter with order parameter (${route})`,
      route: `${route}?dir=asc&order=${order}`,
      statusCode: 200
    });
    tests.push({
      name: `Test dir distinct to asc or desc (${route})`,
      route: `${route}?dir=desc&order=${order}`,
      statusCode: 200
    });
    tests.push({
      name: `Test match exact (${route})`,
      route: `${route}?search=name&match=exact`,
      statusCode: 200
    });
    tests.push({
      name: `Test match regex (${route})`,
      route: `${route}?search=name&match=regex`,
      statusCode: 200
    });
    tests.push({
      name: `Test search query param 'in' (${route})`,
      route: `${route}?search=name&in=${order}`,
      statusCode: 200
    });
    tests.push({
      name: `Test offset (${route})`,
      route: `${route}?offset=1`,
      statusCode: 200
    });
    tests.push({
      name: `Test limit (${route})`,
      route: `${route}?limit=1`,
      statusCode: 200
    });
  }

  tests.forEach(test => {
    it(test.name, async () => {
      const res = await request(server)
        .get(test.route)
        .set('Authorization', `Bearer ${this.token}`)
        .expect(test.statusCode);
      return expect(res).to.satisfyApiSpec;
    });
  });
});

const request = require('supertest');
const chai = require('chai');
const path = require('path');
const chaiResponseValidator = require('chai-openapi-response-validator');
const appRoot = require('app-root-path').toString();
const nock = require('nock');
const url = require('url');
const utils = require('../setup/utils');
const server = require('../../../index');

const expect = chai.expect;

const specPath = path.join(appRoot, 'openapi/bundle.yml');
chai.use(chaiResponseValidator(specPath));

describe('Scenarios', () => {
  before(async () => {
    const user = await utils.randomUserPrisma();
    this.token = await utils.getUserToken(user);
    const risorConfig = global.config.services.risor;
    nock(url.format(risorConfig.uri)).persist()
      .get('/scenarios')
      .replyWithFile(200, path.join(appRoot, 'test/api/setup/replies/listScenarios.json'), {
        'Content-Type': 'application/json'
      });
    nock(url.format(risorConfig.uri)).persist()
      .get('/scenarios/timeout.test')
      .query(true)
      .replyWithFile(200, path.join(appRoot, 'test/api/setup/replies/timeoutScenario.json'), {
        'Content-Type': 'application/json'
      });
  });

  it('Should not be authorized when no bearer token is provided', async () => {
    const res = await request(server)
      .get('/api/scenarios');
    return expect(res).to.satisfyApiSpec;
  });

  it('Return list of the scenarios', async () => {
    this.scenarios = await request(server)
      .get('/api/scenarios')
      .set('Authorization', `Bearer ${this.token}`).expect(200);
    return expect(this.scenarios).to.satisfyApiSpec;
  });

  it('Return a single scenario', async () => {
    const res = await request(server)
      .get('/api/scenarios/timeout.test')
      .set('Authorization', `Bearer ${this.token}`)
      .expect(200);
    return expect(res).to.satisfyApiSpec;
  });
});

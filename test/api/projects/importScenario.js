const url = require('url');
const request = require('supertest');
const chai = require('chai');
const path = require('path');
const chaiResponseValidator = require('chai-openapi-response-validator');
const appRoot = require('app-root-path').toString();
const nock = require('nock');

const utils = require('../setup/utils');
const server = require('../../../index');

const config = global.config;

const expect = chai.expect;

const specPath = path.join(appRoot, 'openapi/bundle.yml');
chai.use(chaiResponseValidator(specPath));

describe('Import scenario into project', () => {
  before(async () => {
    // Mock scenario bundle
    const risorConfig = config.services.risor;
    this.user = await utils.randomUserPrisma();
    this.project = await utils.getRandomProject({ user: this.user });
    this.scenarioNotInProject = await utils.getScenarioNotInProject({ project: this.project });
    nock(url.format(risorConfig.uri)).persist()
      .get(`/scenarios/${this.scenarioNotInProject.id}`)
      .query(true)
      .replyWithFile(200, path.join(appRoot, 'test/api/setup/replies/timeoutScenario.json'), {
        'Content-Type': 'application/json'
      });
    nock(url.format(risorConfig.uri)).persist()
      .get(`/scenarios/${this.scenarioNotInProject.id}/bundle`)
      .replyWithFile(200, path.join(appRoot, 'test/api/setup/replies/scenario_bundle.zip'), {
        'Content-Type': 'application/zip'
      });
    this.token = await utils.getUserToken(this.user);
  });
  describe('PUT /api/projects', () => {
    it('Should not be authorized when no bearer token is provided', async () => {
      const res = await request(server)
        .put('/api/projects/x/scenarios/y');
      return expect(res).to.satisfyApiSpec;
    });
    it('Import a scenario in a project', async () => {
      const res = await request(server)
        .put(`/api/projects/${this.project.id}/scenarios/${this.scenarioNotInProject.id}`)
        .set('Authorization', `Bearer ${this.token}`)
        .expect(200);
      return expect(res).to.satisfyApiSpec;
    });
  });
});

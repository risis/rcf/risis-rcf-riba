const url = require('url');
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const Route = require('routable');
const fs = require('fs');
const appRoot = require('app-root-path').toString();
const config = require('config-lite')({
  config_basedir: appRoot,
  config_dir: 'config'
});
const jwt = require('jsonwebtoken');
const faker = require('faker');
const path = require('path');
const tmpRootPath = path.join(appRoot, config.settings.files.tmpDir);
const fetch = require('node-fetch');
const configKeycloak = config.services.keycloak;

async function randomUserPrisma () {
  const userCount = await prisma.user.count();
  const randomInt = Math.round(Math.random() * (userCount - 1));
  const user = await prisma.user.findMany({
    skip: randomInt,
    take: 1
  });
  return user[0];
}

async function getSuperAdminToken (user) {
  return await getAccessToken(user.id, user.jti, 'Super Admin');
}

async function getUserToken (user) {
  return await getAccessToken(user.id, user.jti, 'User');
}

async function getAccessTokenFromKeycloak (rol) {
  const body = new URLSearchParams();
  body.append('username', configKeycloak.users[rol].username);
  body.append('password', configKeycloak.users[rol].password);
  body.append('grant_type', 'password');
  body.append('client_secret', configKeycloak.client_secret);
  body.append('client_id', configKeycloak.client_id);
  const httpUriKeycloak = url.format(configKeycloak.uri);
  return new Promise((resolve, reject) => {
    fetch(`${httpUriKeycloak}/auth/realms/${configKeycloak.realm}/protocol/openid-connect/token`, {
      method: 'post',
      body,
      headers: { 'Content-Type': 'application/x-www-form-urlencoded', Accept: 'application/json' }
    }).then(res => resolve(res.json())).catch(error => {
      console.debug(error.message);
      reject(new Error(error));
    });
  });
}

async function getAccessToken (sub, jti, rol) {
  const token = jwt.sign({
    exp: Math.floor(Date.now() / 1000) + (60 * 60),
    iat: Math.floor(Date.now() / 1000) + (60 * 60),
    sub,
    jti,
    realm_access: {
      roles: [rol]
    },
    resource_access: {
      account: {
        roles: [
          'manage-account',
          'manage-account-links',
          'view-profile'
        ]
      }
    },
    scope: 'email profile',
    email_verified: true,
    preferred_username: faker.name.findName()
  }, configKeycloak.privateKey.replace(/\\n/gm, '\n'), { algorithm: 'RS256' });
  return token;
}

function createFileTemp () {
  const nameFile = 'test.txt';
  const localInputFilePath = `${tmpRootPath}/${nameFile}`;
  fs.writeFileSync(localInputFilePath, 'Test Test');
  return localInputFilePath;
}

async function getRandomScenario () {
  const scenarioCount = await prisma.scenario.count();
  const randomInt = Math.round(Math.random() * (scenarioCount - 1));
  const scenario = prisma.scenario.findMany({
    skip: randomInt,
    take: 1
  });
  return (await scenario)[0];
}

async function getScenarioNotInProject ({ project }) {
  const scenarios = await prisma.scenario.findMany({
    where: {
      NOT: [
        {
          projects: {
            some: {
              id: project.id
            }
          }
        },
        {
          project_main_scenario: {
            some: {
              id: project.id
            }
          }
        }
      ]
    }
  });
  const scenarioCount = scenarios.length;
  const randomInt = Math.round(Math.random() * (scenarioCount - 1));
  const scenario = scenarios[randomInt];
  const result = await prisma.scenario.findUnique({
    where: {
      id: scenario.id
    }
  });
  return result;
}

async function getRandomProject ({ user }) {
  const userProjects = await prisma.project.findMany({
    where: {
      OR: [
        { author_id: user.id },
        { owners: { some: { id: user.id } } },
        { members: { some: { id: user.id } } }
      ]
    }
  });
  const projectCount = userProjects.length;
  const randomInt = Math.round(Math.random() * (projectCount - 1));
  return userProjects[randomInt];
}

// @see https://github.com/3rd-Eden/nock-knock
function route (path, validators = {}) {
  const route = new Route(path);

  return function parser (url) {
    const params = route.exec(url) || {};

    return Object.keys(params).every(function every (key) {
      if (!(key in validators)) return true;

      return validators[key](params[key]);
    });
  };
}

module.exports = {
  randomUserPrisma,
  getUserToken,
  getSuperAdminToken,
  getAccessToken,
  getAccessTokenFromKeycloak,
  getRandomScenario,
  getScenarioNotInProject,
  getRandomProject,
  createFileTemp,
  prisma,
  route
};

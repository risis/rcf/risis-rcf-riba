const appRoot = require('app-root-path').toString();

const config = require('config-lite')({
  config_basedir: appRoot,
  config_dir: 'config'
});
global.config = config;

module.exports.mochaHooks = {

};

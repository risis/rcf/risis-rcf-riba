const request = require('supertest');
const chai = require('chai');
const path = require('path');
const nock = require('nock');
const fs = require('fs-extra');
const chaiResponseValidator = require('chai-openapi-response-validator');
const appRoot = require('app-root-path').toString();
const faker = require('faker');
const FormData = require('form-data');
const utils = require('../setup/utils');
const server = require('../../../index');

const expect = chai.expect;

const specPath = path.join(appRoot, 'openapi/bundle.yml');
chai.use(chaiResponseValidator(specPath));

describe('Datasets', () => {
  before(async () => {
    this.user = await utils.randomUserPrisma();
    this.token = await utils.getUserToken(this.user);
    this.doi = 'doi:10.5072/FK2/YA0VTC';
    this.doiEncoded = encodeURIComponent('doi:10.5072/FK2/YA0VTC');
    const datastoreConfig = global.config.services.datastore;
    nock(datastoreConfig.apiUrl).persist()
      .get(`/api/dataverses/${datastoreConfig.userDataverseID}/contents`)
      .replyWithFile(200, path.join(appRoot, 'test/api/setup/replies/datasets/list.json'), {
        'Content-Type': 'application/json'
      });
    nock(datastoreConfig.apiUrl).persist()
      .get(`/api/datasets/:persistentId?persistentId=${this.doi}`)
      .replyWithFile(200, path.join(appRoot, 'test/api/setup/replies/datasets/dataset.json'), {
        'Content-Type': 'application/json'
      });

    nock(datastoreConfig.apiUrl).persist()
      .post(`/api/dataverses/${datastoreConfig.userDataverseID}/datasets`)
      .replyWithFile(200, path.join(appRoot, 'test/api/setup/replies/datasets/created.json'), {
        'Content-Type': 'application/json'
      });
    nock(datastoreConfig.apiUrl).persist()
      .post(`/api/datasets/:persistentId/add?persistentId=${this.doi}`)
      .replyWithFile(200, path.join(appRoot, 'test/api/setup/replies/datasets/file.json'), {
        'Content-Type': 'application/json'
      });
    nock(datastoreConfig.apiUrl).persist()
      .get(`/api/access/dataset/:persistentId/?persistentId=${this.doi}`)
      .replyWithFile(200, path.join(appRoot, 'test/api/setup/replies/datasets/file.zip'), {
        'Content-Type': 'application/zip'
      });
    nock(datastoreConfig.apiUrl).persist()
      .put(`/api/datasets/:persistentId/editMetadata?persistentId=${this.doi}&replace=true`)
      .replyWithFile(200, path.join(appRoot, 'test/api/setup/replies/datasets/dataset.json'), {
        'Content-Type': 'application/json'
      });
    nock(datastoreConfig.apiUrl).persist()
      .delete(`/api/datasets/:persistentId/destroy?persistentId=${this.doi}`)
      .replyWithFile(200, path.join(appRoot, 'test/api/setup/replies/datasets/deleted.json'), {
        'Content-Type': 'application/json'
      });
  });

  it('Should not be authorized when no bearer token is provided (GET /api/datasets)', async () => {
    const res = await request(server)
      .get('/api/datasets');
    return expect(res).to.satisfyApiSpec;
  });

  it('Return list of the datasets (GET /api/datasets)', async () => {
    this.datasets = await request(server)
      .get('/api/datasets')
      .set('Authorization', `Bearer ${this.token}`);
    return expect(this.datasets).to.satisfyApiSpec;
  });

  it('Return a single dataset (GET /api/datasets/:id)', async () => {
    const form = new FormData();
    form.append('title', faker.name.findName());
    form.append('short_description', faker.lorem.sentence(7));
    const res = await request(server)
      .get(`/api/datasets/${this.doiEncoded}`)
      .set('Authorization', `Bearer ${this.token}`)
      .expect(200);
    return expect(res).to.satisfyApiSpec;
  });

  it('Create a dataset (POST /api/datasets)', async () => {
    const form = new FormData();
    form.append('title', faker.name.findName());
    form.append('short_description', faker.lorem.sentence(7));
    this.datasetCreated = await request(server)
      .post('/api/datasets')
      .set('Content-Type', 'multipart/form-data; boundary=' + form.getBoundary())
      .set('Authorization', `Bearer ${this.token}`);
    return expect(this.datasetCreated).to.satisfyApiSpec;
  });

  it('Uploading files to a dataset (POST /api/datasets/:id/files)', async () => {
    const pathFile = utils.createFileTemp();
    const form = new FormData();
    form.append('files', fs.createReadStream(pathFile));
    this.datasetCreated.body._id = encodeURIComponent(this.datasetCreated.body._id);
    const res = await request(server)
      .post(`/api/datasets/${this.doiEncoded}/files`)
      .attach('files', pathFile)
      .set('Authorization', `Bearer ${this.token}`)
      .set('Content-Type', 'multipart/form-data; boundary=' + form.getBoundary())
      .expect(200);
    return expect(res).to.satisfyApiSpec;
  });

  it('Get files of one dataset (GET /api/datasets/:id/files)', async () => {
    const res = await request(server)
      .get(`/api/datasets/${this.doiEncoded}/files`)
      .set('Authorization', `Bearer ${this.token}`)
      .expect(200);
    return expect(res).to.satisfyApiSpec;
  });

  it('Edit a dataset (PATCH /api/datasets/:id)', async () => {
    const res = await request(server)
      .patch(`/api/datasets/${this.doiEncoded}`)
      .send({ title: faker.name.findName(), short_description: faker.lorem.sentence(7) })
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.token}`)
      .expect(200);
    return expect(res).to.satisfyApiSpec;
  });

  it('Try Delete a dataset (DELETE /api/datasets/:id)', async () => {
    const res = await request(server)
      .delete(`/api/datasets/${this.doiEncoded}`)
      .set('Authorization', `Bearer ${this.token}`)
      .expect(401);
    return expect(res).to.satisfyApiSpec;
  });
});

const request = require('supertest');
const chai = require('chai');
const path = require('path');
const url = require('url');
const nock = require('nock');
const chaiResponseValidator = require('chai-openapi-response-validator');
const appRoot = require('app-root-path').toString();
const fs = require('fs-extra');
const utils = require('../setup/utils');
const server = require('../../../index');

const expect = chai.expect;

const specPath = path.join(appRoot, 'openapi/bundle.yml');
chai.use(chaiResponseValidator(specPath));

describe('Jobs', () => {
  before(async () => {
    this.user = await utils.randomUserPrisma();
    this.token = await utils.getUserToken(this.user);
    this.project = await utils.getRandomProject({ user: this.user });
    const storageConfig = global.config.services.storage;
    nock(url.format(storageConfig.uri)).persist()
      .get('/users-workspace?location')
      .replyWithFile(200, path.join(appRoot, 'test/api/setup/replies/jobs/location.xml'), {
        'Content-Type': 'application/xml'
      });
    nock(url.format(storageConfig.uri)).persist()
      .filteringPath((path) => {
        return path.startsWith('/users-workspace?delimiter=&encoding-type=url&list-type=2&max-keys=1000&metadata=true&prefix=') ? '/dataResponse' : '/';
      })
      .get('/dataResponse')
      .reply(200, () => {
        return fs.createReadStream('test/api/setup/replies/jobs/response.xml');
      });
  });

  it('Should not be authorized when no bearer token is provided (GET /api/jobs)', async () => {
    const res = await request(server)
      .get('/api/jobs');
    return expect(res).to.satisfyApiSpec;
  });

  it('Return list of the jobs (GET /api/jobs)', async () => {
    this.jobs = await request(server)
      .get('/api/jobs')
      .set('Authorization', `Bearer ${this.token}`);
    return expect(this.jobs).to.satisfyApiSpec;
  });

  it('Get list of the jobs by project id (GET /api/projects/:id/jobs)', async () => {
    const res = await request(server)
      .get(`/api/projects/${this.project.id}/jobs`)
      .set('Authorization', `Bearer ${this.token}`)
      .expect(200);
    return expect(res).to.satisfyApiSpec;
  });

  it('Return a single job (GET /api/jobs/:id)', async () => {
    if (this.jobs && this.jobs.body.length > 0) {
      this.jobId = this.jobs.body[Math.floor(Math.random() * this.jobs.body.length)].id;
      this.job = await request(server)
        .get(`/api/jobs/${this.jobId}`)
        .set('Authorization', `Bearer ${this.token}`);
      return expect(this.job).to.satisfyApiSpec;
    }
    return expect.fail();
  });

  it('Wrong data to update a job (PATCH /api/jobs/:id)', async () => {
    this.slugStates = ['not-ready', 'ready', 'pending', 'running', 'stopping', 'stopped', 'shutting-down', 'finished'];
    this.slugStatesIds = [1, 2, 3, 4, 5, 6, 7, 8];
    if (this.job && this.job.statusCode === 200) {
      const dataToChange = {
        states: this.slugStates[Math.floor(Math.random() * 7)],
        percentages: Math.floor(Math.random() * 100) - 1
      };
      const res = await request(server)
        .patch(`/api/jobs/${this.jobId}`)
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.token}`)
        .send(dataToChange)
        .expect(400);
      return expect(res).to.satisfyApiSpec;
    }
    return expect.fail();
  });

  it('Wrong range in state and percentage to update a job (PATCH /api/jobs/:id)', async () => {
    if (this.job && this.job.statusCode === 200) {
      const dataToChange = {
        state: this.slugStates[Math.floor(Math.random() * 7)],
        percentage: Math.floor(Math.random() * 200) + 101
      };
      const res = await request(server)
        .patch(`/api/jobs/${this.jobId}`)
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.token}`)
        .send(dataToChange)
        .expect(400);
      return expect(res).to.satisfyApiSpec;
    }
    return expect.fail();
  });

  it('Update a job (PATCH /api/jobs/:id)', async () => {
    if (this.job && this.job.statusCode === 200) {
      const randomId = Math.floor(Math.random() * 7);
      const dataToChange = {
        state: this.slugStates[randomId],
        percentage: Math.floor(Math.random() * 100) - 1
      };
      const res = await request(server)
        .patch(`/api/jobs/${this.jobId}`)
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.token}`)
        .send(dataToChange);
      return expect(res).to.satisfyApiSpec &&
        expect(res.body.state.id).to.equal(this.slugStatesIds[randomId]) &&
        expect(res.body.percentage).to.equal(dataToChange.percentage);
    }
    return expect.fail();
  });

  it('Try Delete a single job (DELETE /api/jobs/:id)', async () => {
    if (this.job && this.job.statusCode === 200) {
      const res = await request(server)
        .delete(`/api/jobs/${this.jobId}`)
        .set('Authorization', `Bearer ${this.token}`);
      return expect(res).to.satisfyApiSpec;
    }
    return expect.fail();
  });

  it('Delete a single job (DELETE /api/jobs/:id)', async () => {
    if (this.job && this.job.statusCode === 200) {
      this.token = await utils.getSuperAdminToken(this.user);
      const res = await request(server)
        .delete(`/api/jobs/${this.jobId}`)
        .set('Authorization', `Bearer ${this.token}`)
        .expect(200);
      return expect(res).to.satisfyApiSpec;
    }
    return expect.fail();
  });
});

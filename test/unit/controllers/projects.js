const chai = require('chai');
const rewire = require('rewire');

const expect = chai.expect;

const projectController = rewire('../../../controllers/projects');

describe('controllers/projects', () => {
  describe('flattenKeywords', () => {
    it("Should not change the object if it has no 'keywords' attribute", () => {
      const flattenKeywords = projectController.__get__('flattenKeywords');

      // arrange
      const obj = {
        not: 'a keyword'
      };
      const copy = Object.assign({}, obj);

      // act
      flattenKeywords(obj);

      // assert
      expect(obj).to.deep.equal(copy);
    });
  });
});

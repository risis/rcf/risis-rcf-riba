# Check https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md for reference

FROM node:14-alpine3.17 AS base

RUN apk add --no-cache curl openssl1.1-compat

RUN mkdir -p /home/node/app/data /home/node/app/log && chown -R node:node /home/node/app

WORKDIR /home/node/app

RUN printf '@cortext:registry=https://lib.cortext.net\n//lib.cortext.net/:_authToken=${NPM_TOKEN}' >> .npmrc

ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

ARG PORT=7777
ENV PORT=${PORT}

COPY --chown=node:node package*.json ./

USER node

CMD ["npm", "start"]

# production image

FROM base AS prod

ARG NPM_TOKEN

RUN npm ci --only=production && npm cache clean --force && rm -f .npmrc

COPY . .

RUN npx prisma generate

# development image

FROM base AS dev

ARG NPM_TOKEN

RUN npm install && rm -f .npmrc

COPY . .

RUN npx prisma generate

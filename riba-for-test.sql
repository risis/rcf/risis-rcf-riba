
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

USE `riba_app`;
DROP TABLE IF EXISTS `_memberProjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_memberProjects` (
  `A` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `B` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  UNIQUE KEY `_memberProjects_AB_unique` (`A`,`B`),
  KEY `_memberProjects_B_index` (`B`),
  CONSTRAINT `_memberProjects_ibfk_1` FOREIGN KEY (`A`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `_memberProjects_ibfk_2` FOREIGN KEY (`B`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `_memberProjects` WRITE;
/*!40000 ALTER TABLE `_memberProjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `_memberProjects` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `_ownedProjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_ownedProjects` (
  `A` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `B` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  UNIQUE KEY `_ownedProjects_AB_unique` (`A`,`B`),
  KEY `_ownedProjects_B_index` (`B`),
  CONSTRAINT `_ownedProjects_ibfk_1` FOREIGN KEY (`A`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `_ownedProjects_ibfk_2` FOREIGN KEY (`B`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `_ownedProjects` WRITE;
/*!40000 ALTER TABLE `_ownedProjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `_ownedProjects` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `_prisma_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_prisma_migrations` (
  `id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checksum` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `finished_at` datetime(3) DEFAULT NULL,
  `migration_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logs` text COLLATE utf8mb4_unicode_ci,
  `rolled_back_at` datetime(3) DEFAULT NULL,
  `started_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `applied_steps_count` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `_prisma_migrations` WRITE;
/*!40000 ALTER TABLE `_prisma_migrations` DISABLE KEYS */;
INSERT INTO `_prisma_migrations` VALUES ('02f7ddbf-078e-4759-9622-30d6927cfe01','1326ba5190af0f382291a9ef180265f7effdbcd4405244acd41e5c3b60905738','2022-08-23 11:06:32.957','20210907074632_delete_datetime',NULL,NULL,'2022-08-23 11:06:32.896',1),('0c8f0099-5b71-474b-92c7-f4b7f6e3c6af','12512dc4b12cb91e5f862f725764cfd3fe3dd8abff5dd9b14f77488219aacfe6','2022-08-23 11:06:32.801','20210604144256_service_providers',NULL,NULL,'2022-08-23 11:06:32.758',1),('3ea1b970-14c6-4798-805a-b46b837c7b6a','aee97117fc76efe70df483a2937653b771a8407fbeabb24905ac47c84e2102ba','2022-08-23 11:06:33.363','20220209123137_sync_migrations_with_prisma_V3',NULL,NULL,'2022-08-23 11:06:32.959',1),('58df7a09-b665-49f6-bfbb-93b4b618ab80','0f2b20bf2fa681f28a628129713f351500ddb5ff899a954f2f35cf51cf85da25','2022-08-23 11:06:32.756','20210525140104_alter_projects',NULL,NULL,'2022-08-23 11:06:32.645',1),('b0269775-5e7d-4df8-93fe-b40adee32dc7','02a171c0f08828e78a293ba0bf31fab8811b0fbea2ce47bdd0fc43465c28de3d','2022-08-23 11:06:32.643','20210429112242_init',NULL,NULL,'2022-08-23 11:06:32.060',1),('c7d5074b-07dc-4c84-bb27-a8ea3f9be734','1fae3216f316fb81a92feff085b647936ffc47da5fc93ef977013f20f6b3d71e','2022-08-23 11:06:32.843','20210802161227_notifications',NULL,NULL,'2022-08-23 11:06:32.802',1),('e56af9f3-57f6-420f-a9e8-8ac0dbbba8a4','0708afc213b327abce548dc8868c80ebfa9be490f9cbd4a531a640ac21001b97','2022-08-23 11:06:32.894','20210804135527_author_job',NULL,NULL,'2022-08-23 11:06:32.845',1);
/*!40000 ALTER TABLE `_prisma_migrations` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `_projectDatasets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_projectDatasets` (
  `A` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `B` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  UNIQUE KEY `_projectDatasets_AB_unique` (`A`,`B`),
  KEY `_projectDatasets_B_index` (`B`),
  CONSTRAINT `_projectDatasets_ibfk_1` FOREIGN KEY (`A`) REFERENCES `dataset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `_projectDatasets_ibfk_2` FOREIGN KEY (`B`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `_projectDatasets` WRITE;
/*!40000 ALTER TABLE `_projectDatasets` DISABLE KEYS */;
INSERT INTO `_projectDatasets` VALUES ('doi:10.5072/FK2/RJOMJY','k6m9TvnjtSX'),('doi:10.5072/FK2/MOIFCB','LIceRVp5HxP');
/*!40000 ALTER TABLE `_projectDatasets` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `_projectKeywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_projectKeywords` (
  `A` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `B` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  UNIQUE KEY `_projectKeywords_AB_unique` (`A`,`B`),
  KEY `_projectKeywords_B_index` (`B`),
  CONSTRAINT `_projectKeywords_ibfk_1` FOREIGN KEY (`A`) REFERENCES `keyword` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `_projectKeywords_ibfk_2` FOREIGN KEY (`B`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `_projectKeywords` WRITE;
/*!40000 ALTER TABLE `_projectKeywords` DISABLE KEYS */;
/*!40000 ALTER TABLE `_projectKeywords` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `_projectScenarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_projectScenarios` (
  `A` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `B` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  UNIQUE KEY `_projectScenarios_AB_unique` (`A`,`B`),
  KEY `_projectScenarios_B_index` (`B`),
  CONSTRAINT `_projectScenarios_ibfk_1` FOREIGN KEY (`A`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `_projectScenarios_ibfk_2` FOREIGN KEY (`B`) REFERENCES `scenario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `_projectScenarios` WRITE;
/*!40000 ALTER TABLE `_projectScenarios` DISABLE KEYS */;
INSERT INTO `_projectScenarios` VALUES ('k6m9TvnjtSX','eupro'),('LIceRVp5HxP','eupro'),('G3LWlpTjLnb','ipc-analysis'),('G3LWlpTjLnb','timeout.test'),('k6m9TvnjtSX','timeout.test'),('KxXWhHqgu0B','timeout.test'),('LIceRVp5HxP','timeout.test');
/*!40000 ALTER TABLE `_projectScenarios` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dataset` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataset` WRITE;
/*!40000 ALTER TABLE `dataset` DISABLE KEYS */;
INSERT INTO `dataset` VALUES ('doi:10.5072/FK2/MOIFCB'),('doi:10.5072/FK2/RJOMJY');
/*!40000 ALTER TABLE `dataset` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `job` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int NOT NULL,
  `project_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scenario_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response` json DEFAULT NULL,
  `percentage` int NOT NULL DEFAULT '-1',
  `execution_datetime` datetime(3) DEFAULT NULL,
  `creation_datetime` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `author_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `engine_id` VARCHAR(191) NULL,
  `delete_datetime` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `job_author_id_fkey` (`author_id`),
  KEY `job_project_id_fkey` (`project_id`),
  KEY `job_scenario_id_fkey` (`scenario_id`),
  KEY `job_state_id_fkey` (`state_id`),
  CONSTRAINT `job_author_id_fkey` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `job_project_id_fkey` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `job_scenario_id_fkey` FOREIGN KEY (`scenario_id`) REFERENCES `scenario` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `job_state_id_fkey` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `job` WRITE;
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
INSERT INTO `job` VALUES ('6HO_5FcwuNj',NULL,'User-Project-eupro-1-0-0-0',1,'k6m9TvnjtSX','eupro',NULL,-1,NULL,'2022-08-23 11:27:54.806','00d2e735-7ab5-4c21-ae86-15aabf04a9dc',NULL,NULL),('apWSb4kwXXJ',NULL,'User-Project-timeout.test-1-0-0-0',1,'k6m9TvnjtSX','timeout.test',NULL,-1,NULL,'2022-08-23 11:27:57.835','00d2e735-7ab5-4c21-ae86-15aabf04a9dc',NULL,NULL),('d6rqlBYX1Ta',NULL,'Project-Admin-timeout.test-1-0-0-0',1,'LIceRVp5HxP','timeout.test',NULL,-1,NULL,'2022-08-23 11:20:07.095','f898d8d3-e088-4077-a649-18c6d2db9a8b',NULL,NULL),('EfT-oYgDofl',NULL,'User-Project-timeout.test-1-0-0-0',8,'k6m9TvnjtSX','timeout.test','{\"self\": {\"uid\": \"T1UnqIBI_\", \"hash\": \"de6bda692210bc16a453f8c30cf78586\", \"name\": \"timeout.test\", \"response\": {\"status\": 200}}, \"sleep\": {\"message\": \"ZZzzz... slept for 1000 milliseconds\"}, \"params\": {\"inputs\": {\"project-id\": \"k6m9TvnjtSX\", \"milliseconds\": \"1000\", \"workspace-uri\": \"m4://users-workspace/00d2e735-7ab5-4c21-ae86-15aabf04a9dc/projects/k6m9TvnjtSX/scenarios/timeout.test/EfT-oYgDofl\"}, \"datastore_public_uri\": \"https://datastore.risis.localhost\"}, \"context\": {\"datastore\": {\"token\": \"132e8fc1-8be4-4c8f-9e21-91144e2e5d7a\"}, \"callback_url\": \"http://rcf-riba:7778/api/jobs/EfT-oYgDofl\"}, \"options\": {\"followRedirect\": true}}',100,'2022-08-23 11:27:57.804','2022-08-23 11:27:22.770','00d2e735-7ab5-4c21-ae86-15aabf04a9dc',NULL,NULL),('f-EGO-5-KAw',NULL,'Project-Admin-2-timeout.test-1-0-0-0',1,'KxXWhHqgu0B','timeout.test',NULL,-1,NULL,'2022-08-23 11:21:06.730','f898d8d3-e088-4077-a649-18c6d2db9a8b',NULL,NULL),('INCM_vNA8bN',NULL,'User-Project-2-timeout.test-1-0-0-0',8,'G3LWlpTjLnb','timeout.test','{\"self\": {\"uid\": \"xtXXuXXH8\", \"hash\": \"4af2bf9085f2f8a1318ca863a875f476\", \"name\": \"timeout.test\", \"response\": {\"status\": 200}}, \"sleep\": {\"message\": \"ZZzzz... slept for 1000 milliseconds\"}, \"params\": {\"inputs\": {\"project-id\": \"G3LWlpTjLnb\", \"milliseconds\": \"1000\", \"workspace-uri\": \"m4://users-workspace/00d2e735-7ab5-4c21-ae86-15aabf04a9dc/projects/G3LWlpTjLnb/scenarios/timeout.test/INCM_vNA8bN\"}, \"datastore_public_uri\": \"https://datastore.risis.localhost\"}, \"context\": {\"datastore\": {\"token\": \"132e8fc1-8be4-4c8f-9e21-91144e2e5d7a\"}, \"callback_url\": \"http://rcf-riba:7778/api/jobs/INCM_vNA8bN\"}, \"options\": {\"followRedirect\": true}}',100,'2022-08-23 11:28:29.411','2022-08-23 11:28:26.203','00d2e735-7ab5-4c21-ae86-15aabf04a9dc',NULL,NULL),('lkjOqHJ9_6i',NULL,'Project-Admin-timeout.test-1-0-0-0',8,'LIceRVp5HxP','timeout.test','{\"self\": {\"uid\": \"HB5VtBVj7\", \"hash\": \"9d2bd6d925112062acfd1a3fde895001\", \"name\": \"timeout.test\", \"response\": {\"status\": 200}}, \"sleep\": {\"message\": \"ZZzzz... slept for 1000 milliseconds\"}, \"params\": {\"inputs\": {\"project-id\": \"LIceRVp5HxP\", \"milliseconds\": \"1000\", \"workspace-uri\": \"m4://users-workspace/f898d8d3-e088-4077-a649-18c6d2db9a8b/projects/LIceRVp5HxP/scenarios/timeout.test/lkjOqHJ9_6i\"}, \"datastore_public_uri\": \"https://datastore.risis.localhost\"}, \"context\": {\"datastore\": {\"token\": \"ca879993-182d-40d7-80f1-10e58b4f52fc\"}, \"callback_url\": \"http://rcf-riba:7778/api/jobs/lkjOqHJ9_6i\"}, \"options\": {\"followRedirect\": true}}',100,'2022-08-23 11:20:07.083','2022-08-23 11:17:13.803','f898d8d3-e088-4077-a649-18c6d2db9a8b',NULL,NULL),('oGfYtTN6irA',NULL,'User-Project-2-ipc-analysis-1-0-0-0',1,'G3LWlpTjLnb','ipc-analysis',NULL,-1,NULL,'2022-08-23 11:28:16.531','00d2e735-7ab5-4c21-ae86-15aabf04a9dc',NULL,NULL),('qODY7-maMf0',NULL,'Project-Admin-2-timeout.test-1-0-0-0',8,'KxXWhHqgu0B','timeout.test','{\"self\": {\"uid\": \"_cfK-RJRG\", \"hash\": \"2ffae1ff2c173fcdb292413c35b26a60\", \"name\": \"timeout.test\", \"response\": {\"status\": 200}}, \"sleep\": {\"message\": \"ZZzzz... slept for 1000 milliseconds\"}, \"params\": {\"inputs\": {\"project-id\": \"KxXWhHqgu0B\", \"milliseconds\": \"1000\", \"workspace-uri\": \"m4://users-workspace/f898d8d3-e088-4077-a649-18c6d2db9a8b/projects/KxXWhHqgu0B/scenarios/timeout.test/qODY7-maMf0\"}, \"datastore_public_uri\": \"https://datastore.risis.localhost\"}, \"context\": {\"datastore\": {\"token\": \"ca879993-182d-40d7-80f1-10e58b4f52fc\"}, \"callback_url\": \"http://rcf-riba:7778/api/jobs/qODY7-maMf0\"}, \"options\": {\"followRedirect\": true}}',100,'2022-08-23 11:21:06.720','2022-08-23 11:21:01.852','f898d8d3-e088-4077-a649-18c6d2db9a8b',NULL,NULL),('rFSHBaFyEAW',NULL,'Project-Admin-eupro-1-0-0-0',1,'LIceRVp5HxP','eupro',NULL,-1,NULL,'2022-08-23 11:16:42.279','f898d8d3-e088-4077-a649-18c6d2db9a8b',NULL,NULL),('t5Xqn9jTXfF',NULL,'Project-Admin-timeout.test-1-0-0-0',1,'LIceRVp5HxP','timeout.test',NULL,-1,'2022-08-23 11:17:13.787','2022-08-23 11:15:13.673','f898d8d3-e088-4077-a649-18c6d2db9a8b',NULL,NULL),('tbQhjO8X5im',NULL,'User-Project-2-timeout.test-1-0-0-0',1,'G3LWlpTjLnb','timeout.test',NULL,-1,NULL,'2022-08-23 11:28:29.423','00d2e735-7ab5-4c21-ae86-15aabf04a9dc',NULL,NULL);
/*!40000 ALTER TABLE `job` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `keyword` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyword_value_key` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `keyword` WRITE;
/*!40000 ALTER TABLE `keyword` DISABLE KEYS */;
/*!40000 ALTER TABLE `keyword` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'info',
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation_datetime` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `delete_datetime` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notification_user_id_fkey` (`user_id`),
  CONSTRAINT `notification_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
INSERT INTO `notification` VALUES ('_D3RoueVp89','Scenario execution is finished!','The execution of the scenario in the project Project Admin 2 has been completed.','/projects/KxXWhHqgu0B/view/outputs','success',0,'job_finished','f898d8d3-e088-4077-a649-18c6d2db9a8b','2022-08-23 11:21:07.865',NULL),('I8tYbl96tCT','Scenario execution is finished!','The execution of the scenario in the project User Project 2 has been completed.','/projects/G3LWlpTjLnb/view/outputs','success',0,'job_finished','00d2e735-7ab5-4c21-ae86-15aabf04a9dc','2022-08-23 11:28:30.593',NULL),('jiVwDJ319hk','Scenario execution is finished!','The execution of the scenario in the project Project Admin has been completed.','/projects/LIceRVp5HxP/view/outputs','success',0,'job_finished','f898d8d3-e088-4077-a649-18c6d2db9a8b','2022-08-23 11:20:08.266',NULL),('LcmZLXUpaEK','Scenario execution is finished!','The execution of the scenario in the project User Project has been completed.','/projects/k6m9TvnjtSX/view/outputs','success',0,'job_finished','00d2e735-7ab5-4c21-ae86-15aabf04a9dc','2022-08-23 11:27:58.919',NULL);
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_scenario_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_id` int NOT NULL,
  `creation_datetime` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `delete_datetime` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_author_id_fkey` (`author_id`),
  KEY `project_main_scenario_id_fkey` (`main_scenario_id`),
  KEY `project_state_id_fkey` (`state_id`),
  CONSTRAINT `project_author_id_fkey` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `project_main_scenario_id_fkey` FOREIGN KEY (`main_scenario_id`) REFERENCES `scenario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `project_state_id_fkey` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES ('G3LWlpTjLnb','User Project 2','User Project 2 Description','00d2e735-7ab5-4c21-ae86-15aabf04a9dc',NULL,8,'2022-08-23 11:28:11.316',NULL),('k6m9TvnjtSX','User Project','User Project Description','00d2e735-7ab5-4c21-ae86-15aabf04a9dc',NULL,8,'2022-08-23 11:27:15.247',NULL),('KxXWhHqgu0B','Project Admin 2','Project Admin 2 Description','f898d8d3-e088-4077-a649-18c6d2db9a8b',NULL,8,'2022-08-23 11:20:55.446',NULL),('LIceRVp5HxP','Project Admin','Project Admin Description','f898d8d3-e088-4077-a649-18c6d2db9a8b',NULL,8,'2022-08-23 11:11:19.385',NULL);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `scenario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scenario` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `scenario` WRITE;
/*!40000 ALTER TABLE `scenario` DISABLE KEYS */;
INSERT INTO `scenario` VALUES ('eupro'),('ipc-analysis'),('timeout.test');
/*!40000 ALTER TABLE `scenario` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `service_provider_credentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_provider_credentials` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idService` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation_datetime` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `delete_datetime` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_provider_credentials_user_id_fkey` (`user_id`),
  CONSTRAINT `service_provider_credentials_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `service_provider_credentials` WRITE;
/*!40000 ALTER TABLE `service_provider_credentials` DISABLE KEYS */;
INSERT INTO `service_provider_credentials` VALUES ('H1JHEPZDcoO','datastore','RCF Datastore','132e8fc1-8be4-4c8f-9e21-91144e2e5d7a','00d2e735-7ab5-4c21-ae86-15aabf04a9dc','2022-08-23 11:26:50.967',NULL),('IJ5QZycnNmw','datastore','RCF Datastore','ca879993-182d-40d7-80f1-10e58b4f52fc','f898d8d3-e088-4077-a649-18c6d2db9a8b','2022-08-23 11:07:03.748',NULL);
/*!40000 ALTER TABLE `service_provider_credentials` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `state` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `state_slug_key` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES (1,'Not ready','not-ready'),(2,'Ready','ready'),(3,'Pending','pending'),(4,'Running','running'),(5,'Stopping','stopping'),(6,'Stopped','stopped'),(7,'Shutting down','shutting-down'),(8,'Finished','finished');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jti` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creation_datetime` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `delete_datetime` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('00d2e735-7ab5-4c21-ae86-15aabf04a9dc','user@risis.io','user@risis.io','Normal User','RCF',NULL,'3062e4e0-b4f5-490a-ac71-a497ad70bcaf','2022-08-23 11:26:50.845',NULL),('f898d8d3-e088-4077-a649-18c6d2db9a8b','admin@risis.io','admin@risis.io','Admin','RCF',NULL,'25be2ed5-fb42-4833-83fe-1eb346bc7035','2022-08-23 11:07:03.579',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

-- /*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
-- /*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
-- /*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
-- /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
-- /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
-- /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- /*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
DROP TABLE IF EXISTS `ar_dataset`;
-- riba_app.ar_dataset definition

CREATE TABLE `ar_dataset` (
  `id` int NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` mediumtext COLLATE utf8mb4_unicode_ci,
  `use_case` mediumtext COLLATE utf8mb4_unicode_ci,
  `dataset_owner` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dataset_access_manager` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `documentation` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `am_email` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- -- The insert is necessary because this is static information
INSERT INTO `ar_dataset` (`id`, `label`, `description`, `keywords`, `use_case`, `dataset_owner`, `dataset_access_manager`, `short_description`, `documentation`) VALUES
(1, 'CHEETAH', 'Cheetah contains geographical, industry and accounting information on three cohorts of mid-sized firms that experienced fast growth (i.e., average annual sales or employment growth greater than 20% for at least three years) during three growth periods: 2008-2011, 2009-2012 and 2010-2013. Mid-sized firms are defined by combining the EUROSTAT definition of medium-sized firm and the French definition of Entreprise de Taille Intermédiaire (ETI). The uniqueness of this database lies in the focus on this category of firms and its coverage in terms of countries. Cheetah currently includes 42,369 mid-sized firms, which are located in 30 European countries and Israel. Services currently offered by the infrastructure: Cheetah is a brand new output of the first RISIS project. It is a unique source of data that offers to researchers new opportunities to study the growth dynamics of a relevant category of firms (i.e. mid-sized firms), which however has been almost neglected by the current academic literature on firm growth. The European dimension of Cheetah makes the data infrastructure well suited to address research questions concerning the institutional determinants of firm growth and geographical agglomeration effects.', 'company; fast-growing; medium-sized; firm; growth', 'https://sti2017.paris/wp-content/uploads/2017/11/id1-colombo-guerini.pdf', 'Politecnico di Milano; www.dig.polimi.it', 'Massimiliano Guerini; Massimiliano.Guerini@polimi.it', 'Cheetah is a database featuring geographical, industry and accounting information on three cohorts of mid-sized firms that experienced fast growth during the periods 2008-2011, 2009-2012 and 2010-2013', 'https://zenodo.org/record/3337963/files/Documentation_Cheetah_Final_edited.pdf?download=1'),
(2, 'CIB / CinnoB', 'CinnoB (former CIB) provides data and indicators at the firm level for the worldwide largest R&D corporate performers (about 4 000 parent companies and their 350 000 consolidated subsidiaries) that represent over 90% of world corporate industrial R&D. It includes a database of indicators related to R&D and innovation corporate activities at the firm level and, and a register to track back the links between the different sources used to produce the data. The indicators will originate from different resources available in the RISIS consortium: IFRIS-PASTAT, the CWTS Publication Database, EUPRO, the Industrial R&D Scoreboard and Orbis (aggregated data at the parent parent company level). This large set of resources will allow producing data and indicators related to: the corporate knowledge production (Patents/Trademarks/Publications/FP projects), R&D resources in firms and the general activities in firms (financial, sectoral data). Data and indicators will be produced for a period of time of 10 years (2005 -2015) (before and after the crisis). For patents the data will cover the period of time from 1995 to 2015 to provide data for panel analysis. In this sense, CinnoB is used to study many aspects of the dynamics of scientific and technological knowledge creation in large worldwide companies. Services currently offered by the infrastructure: CinnoB is designed as a robust and user-friendly repository of indicators providing standardised indicators at the firm level (including the parent company and consolidated subsidiaries activities). It will cover many dimensions, i.e. industrial sector, technologies, scientific domains, geography, collaborations or financial information. It will benefit from specific recent developments such as the geocoding of addresses, the identification of key actors, and the tagging of thematic priorities. CinnoB will provide a diverse set of quantitative information to study the dynamics of knowledge production at fine scale in large firms in a given sector, technology or geographical space.', 'corporation; invention; patent', 'https://core.ac.uk/download/pdf/48330918.pdf', 'Université Paris-Est Marne-la-Vallée (UPEM); http://www.u-pem.fr/', 'Patricia Laurens; patricia.laurens@esiee.fr', 'The CIB / CinnoB - Corporate Invention and Innovation Boards is a database about largest R&D performers and their subsidiaries worldwide, providing patenting and other indicators', 'https://zenodo.org/record/3338122/files/Documentation_CIB_Final_RISIS1_version.pdf?download=1'),
(3, 'CWTS Publication Database', 'The CWTS publication database is a full copy of Web of Science (WoS) dedicated to bibliometric analyses. The enhancements and improvement to the original version of WoS involve: harmonized main organisation names, matching cited references to source publications and some other data quality improvements. In this database a permanent link is made between author affiliations and OrgReg identifiers. The database enables output and (scientific) impact analyses of any set of publications covered by WoS, using state of the art methods. Services currently offered by the infrastructure: A public dataset to demonstrate the potential of the database, together with a complete documentation of data and methods used is available at leidenranking.com. In this dataset a set of around 800 universities worldwide is included with their research performance statistics. Through RISIS, a much larger list of organisations will be provided (including PRO’s) with their research performance statistics. More details studies can be executed on-site at CWTS using the underlying database via a research visit.', 'academic publishing; journals; articles; web of science', 'http://www.leidenranking.com/downloads', 'Leiden University; www.cwts.nl', 'Clara Calero Medina; clara@cwts.leidenuniv.nl', 'The CWTS publication database is a full copy of Web of Science (WoS) dedicated to bibliometric analyses, with additional information e.g. on standardised organisation names and other enhancements', 'https://zenodo.org/record/3381509/files/Documentation_CWTS_Final_edited.pdf?download=1'),
(4, 'EUPRO', 'EUPRO is a unique dataset providing systematic and standardized information on R&D projects, participants and resulting networks of the EU FP, starting from FP1, and recently integrating H2020 (until 2019), and other European funding instruments, such as EUREKA, COST and selected Joint Technology Initiatives (JTIs). It has been recently used intensively as a core facility in research activities that investigate structure, dynamics and impacts of project-based R&D collaboration, in particular to grasp and understand the development of the European Research Area (ERA). Basically, EUPRO covers cleaned and standardised information on R&D projects (such as project objectives and achievements, project costs, total funding, start and end date, contract type, information on the call), and their participants (standardized name of the participating organisation, organisation type, and geographical location). Services currently offered by the infrastructure: EUPRO offers the access to high quality data on R&D projects funded by different European funders. The robustness of the dataset and its relevance for actual research issues of the field has been underlined by the number of transnational access requests and visits conducted in the first RISIS project. EUPRO offers substantial possibilities to address questions on structure and dynamics of knowledge creation, networking patterns of organisations in R&D projects, or impacts of publicly funded R&D, among others. Further, with its huge standardised set of organisational names it offers a great possibility to connect with other organisational level datasets, both within and external to RISIS.', 'R&D projects; European Framework Programme; networks; collaborative knowledge production', 'https://onlinelibrary.wiley.com/doi/abs/10.1111/j.1435-5957.2012.00419.x; https://academic.oup.com/joeg/article-abstract/13/1/23/891199?redirectedFrom=fulltext;', 'AIT Austrian Institute of Technology; www.ait.ac.at', 'Thomas Scherngell; thomas.scherngell@ait.ac.at', 'EUPRO is a unique dataset providing systematic and standardized information on R&D projects of different European R&D policy programmes', 'https://zenodo.org/record/4428394#.X_hjPBYxlPY'),
(5, 'RISIS Patent', 'RISIS Patent offers an enriched and cleaned version of the PATSTAT database, a database of all patent offices produced by the European patent office (EPO). In RISIS1, three major developments have been made to improve its relevance. The first one deals with geocoding addresses so as to follow the dynamics of invention. A second important development has been taking into account so-called artificial patents (13% of total priority patents). A third development – still at the pilot phase – deals with the recognition of individuals being legal persons in patent owners. Services currently offered by the infrastructure: All these developments will be open and accessible. Users can query it along technological dimensions, geographical dimensions (at country and metropolitan levels), along actors and their agglomeration in sectors. Users can also use the sector-technology concordance map developed by Fraunhofer-ISI as a complementary resource to conduct sectoral studies. In addition, firms (and their subsidiaries) from CinnoB (see above) have been identified and agglomerated data at the firm level is integrated into the CinnoB DB. Identification of public actors (from ORGREG) and of firms from the other datasets (VICO and CHEETAH) has been initiated to insure a greater integration potential for RISIS users.', 'patent; invention;', 'http://www.sciences-technologies.eu/images/stories/cv/RISIS_Patents_DB.pdf;', 'Université Paris-Est Marne-la-Vallée (UPEM); http://www.u-pem.fr/', 'Patricia Laurens; patricia.laurens@esiee.fr', 'RISIS Patent offers an enriched and cleaned version of the PATSTAT database, with a focus on standardised organisation names and geolocalisation', 'https://zenodo.org/record/3342454/files/Documentation_RISIS%20Patstat_Final.pdf?download=1'),
(6, 'JOREP 2.0', 'JoREP is a unique database on European trans-national joint R&D programmes. JOREP stores raw panel data on joint R&D programmes and a basic set of descriptors of agencies participating to the programmes. The actual list of variables was subject to utility and feasibility controls. The current version 2.0, opened in June 2016 (on-site access), covers data for the period 2000-2014, with a specific focus on 2013 and 2014 - assumed as reference years. JOREP 2.0 has a geographical coverage of 32 countries for European-level programmes for the period 2010-2014 and a table of indicators at the country level to allow spatial analysis. Services currently offered by the infrastructure: The facility provides a quantitative basis for the monitoring of investments in joint R&D programmes, pointing out the policy rationales behind them and their impact. Each programme is described by a set of attributes (organisational, financial, etc.). The design of the database has been planned to store data on the amount spent for joint R&D programmes and on their organisational characteristics (managing research funding organisations), according to the analyses of the modes of the ERA dynamics. Combined analyses with EUPRO are currently allowed for JTIs and EUREKA thank to the integration of the two datasets.', 'Funding of science; European Research Area; transnational programmes', 'https://zenodo.org/record/2560373/files/JOREP.pdf?download=1; https://academic.oup.com/rev/article-abstract/23/4/312/2889058?redirectedFrom=fulltext; https://www.sciencedirect.com/science/article/abs/pii/S0048733313001911', 'IRCrES-CNR; http://www.ircres.cnr.it', 'Andrea Orazio Spinello; andrea.spinello@ircres.cnr.it', 'JoREP is a database on European trans-national joint R&D programmes, storing a basic set of descriptors on the programmes and  agencies participating to the programmes', 'https://zenodo.org/record/3530218#.XcKIe9Uxm70'),
(7, 'MORE', 'MORE (Mobility Survey of the Higher Education Sector) is arguably the most comprehensive empirical study of researcher mobility available. The MORE dataset (\'facility\') is composed of three independent waves of extensive, Europe-wide surveys in this family (MORE1, MORE2 and MORE3). They were carried out on behalf of the EU Commission among large samples of researchers at European universities, respectively. MORE provides a unique lens on mobility patterns and career paths in Europe, including measures of flows of international (and sector) mobility, of factors that influence mobility, and of effects that can be linked to researcher mobility. Services currently offered by the infrastructure: The MORE facility includes harmonization (e.g. of field of science, career stages) and links the data to the European Tertiary Education Register (RISIS-ETER). The linked information includes variables such as the distribution of staff by field of science, overall funding, and share of foreign students or staff, and provides the scope for in-depth analysis as it allows the research community to control for important institutional-level effects.', 'researcher mobility; research careers; survey; career stages', 'https://brage.bibsys.no/xmlui/bitstream/handle/11250/2358894/NIFUarbeidsnotat2014-11.pdf?sequence=1', 'NIFU; http://www.nifu.no/en/', 'Eric Iversen; eric@nifu.no', 'MORE (Mobility Survey of the Higher Education Sector) is a comprehensive empirical study of researcher mobility in Europe', 'https://zenodo.org/record/3337987/files/Documentation_More_Final_edited.pdf?download=1'),
(8, 'NANO', 'The Nano S&T dynamics database (Nano) collects publications and patents between 1991 and 2011 about Nano S&T. One central characteristics of emerging S&T is that they do not correspond to pre-existing categorisations and require the elaboration of semantic based queries. IFRIS has developed a dynamic query gathering 1.18 million Nano-related publications and 735000 priority patents. Services currently offered by the infrastructure: The facility offers harmonised and integrative analysis of Nano S&T dynamics globally, due to four types of enrichments that have been conducted: (i) categorisation and harmonisation of institutional affiliations, (ii) geolocalisation of all authors and inventors; (iii) geographical clustering of S&T activities; and (iv) thematic clustering of S&T activities.', 'nanoscience; nanotechnology; publications; patent; invention;', 'https://sti2017.paris/wp-content/uploads/2017/11/geo-villard-on-nano.pdf', 'Université Paris-Est Marne-la-Vallée (UPEM); http://www.u-pem.fr/', 'Lionel Villard ; lionel.villard@esiee.fr', 'The Nano S&T dynamics database (Nano) collects publications and patents between 1991 and 2011 about Nano S&T', 'https://zenodo.org/record/3338124/files/Documentation_Nano_Final_RISIS1_version.pdf?download=1'),
(9, 'PROFILE', 'ProFile is a longitudinal study focusing on the situation of doctoral candidates and their postdoctoral professional careers. The study started in 2009 and continued until 2017 when it was dissolved in the successor study Nacaps. Up to date, ProFile and its successor study Nacaps are the only longitudinal studies in Europe containing doctoral students from all scientific disciplines and regular information on the conditions of their doctoral training. ProFile is based on a systematic sample of doctoral candidates at universities and funding organisations in Germany. ProFile has seen visits during RISIS partly from researchers active in collecting data on careers themselves. ProFile and Nacaps thus serve as a beacon for similar data collection endeavours in other countries. The visits advance research on careers of PhDs and help build a network of data collectors on PhD careers in Europe.', 'researcher mobility; research careers; survey; career stages', 'https://doi.org/10.1093/reseval/rvx024; https://www.degruyter.com/view/j/jbnst.2017.237.issue-4/jbnst-2015-1037/jbnst-2015-1037.xml', 'DZHW; https://www.dzhw.eu/', 'Jakob Tesch ; tesch@dzhw.eu', 'ProFile is a longitudinal study focusing on the situation of doctoral candidates and their postdoctoral professional careers at German universities and funding organisations', 'https://zenodo.org/record/3337989/files/Documentation_ProFile_Final_edited.pdf?download=1'),
(10, 'RISIS-ETER', 'RISIS-ETER represents an extension of the Tertiary Education Register (ETER; https://www.eter-project.com/) database that is supported by the European Commission through a service contract. RISIS-ETER provides an environment for enriching ETER with additional data in three respects: integrating additional variables from other RISIS datasets, particularly concerning research output (EU-FP participations from EUPRO, scientific publications from CWTS publications databse, patents from IFRIS-PATSTAT), extending the time coverage of ETER, and extending the geographical scope of ETER beyond Europe. RISIS-ETER is closely integrated and hosted by the same technical infrastructure as the register of public-sector research and higher education institutions OrgReg, but is a distinct database providing statistical data.', 'higher education; university; register', 'https://link.springer.com/article/10.1007/s11192-015-1768-2', 'Università della Svizzera italiana; https://www.usi.ch/en', 'Benedetto Lepori ; blepori@usi.ch', 'RISIS-ETER represents an extension by additional indicators in terms of research activities of the European Tertiary Education Register database', 'https://www.eter-project.com/wp-content/uploads/2022/02/ETERIV_Handbook.pdf'),
(11, 'SIPER', 'Science and Innovation Policy Evaluations Repository (SIPER) is a rich and unique database and knowledge source of science and innovation policy evaluations worldwide. Effects and efficiency of science, technology and innovation (STI) policies are typically assessed through a process of evaluation and illustrated in the evaluation reports. SIPER provides on-line access to those STI evaluation reports as well as data on the underlying policy measures at a single location. The database allows targeted searches and access to specific instruments and evaluation design for policy practitioners to learn from. It enables policy learning and academic research by providing an informed analysis of the database contents which allows users to pursue a range of research questions on evaluation approaches (methods, designs, objectives etc.), evaluation uses, impact, connection between characteristics of policy design and evaluation design, etc. Services currently offered by the infrastructure: SIPER provides completely new repository of evaluations, with a unique characterisation system, offering an accessible database with a public portal to function as a service to the policy makers and academic communities. SIPER has developed a framework to characterise each evaluation report as well as the corresponding policy measure. SIPER focuses on evaluations after the year 2000. It has a global ambition, but starts with a focus on the Organisation for Economic Co-operation and Development (OECD) member countries/regions. SIPER is open to all interested parties, and is being widely used for policy learning and research purposes. Via RISIS, SIPER offers access to specific data no open in the public version.', 'policy evaluations; repository; science policy', 'http://si-per.eu/Home/Publications', 'Fraunhofer ISI; https://www.isi.fraunhofer.de/en.html', 'Susanne Bührer-Topçu; Susanne.Buehrer-Topcu@isi.fraunhofer.de', 'Science and Innovation Policy Evaluations Repository (SIPER) is a rich and unique database and knowledge source of science and innovation policy evaluations worldwide', 'https://zenodo.org/record/3338001/files/RISIS_datasets_documentation_SIPER_v2_rev.pdf?download=1'),
(12, 'VICO', 'VICO contains geographical, industry and accounting information on start-ups that received at least one venture capital investment in the period 1998-2014. Start-ups have been incorporated from 1988 onwards in seven European countries (Belgium, Finland, France, Germany, Italy, Spain, and the United Kingdom) and Israel. VICO also provides information on venture capital investment deals (e.g. deal date, total amount invested) and investors (e.g. investor type, investor experience, and its geographical location). Its uniqueness lies in the overall number of companies (17,863), the country coverage, and the extent of information gathered, thanks to the combination of multiple secondary data sources, namely Thompson One Private Equity, Zephyr, Crunchbase and Orbis. Services currently offered by the infrastructure: VICO allows addressing a wide range of research questions concerning the characteristics, the evolution and the role of venture capital in Europe. It has been widely used by a large academic community to study the peculiarities of the European venture capital market and effectiveness of (different forms of) venture capital in supporting the performance of European start-ups. Results from academic research based on VICO data also attracted the attention of policy makers interested in assessing the impact of policy measures aimed at facilitating access to finance to start-ups. Data are currently available on site (at the Department of Management Engineering of POLIMI) for visits. During the first RISIS project, several scholars had the opportunity to access the data and to interact with researchers of POLIMI, which provided advice and customized support.', 'start-ups; venture capital', 'https://www.sciencedirect.com/science/article/pii/S0929119915001030', 'Politecnico di Milano; www.dig.polimi.it', 'Francesca Tenca; francesca.tenca@polimi.it', 'VICO is a database comprising geographical, industry and accounting information on start-ups that received at least one venture capital investment in the period 1998-2014', 'https://zenodo.org/record/3337991/files/Documentation_Vico_Final_edited.pdf?download=1'),
(13, 'ESID ', 'ESID is a comprehensive and authoritative source of information on social innovation projects and actors in Europe and beyond. Initially developed as part of the EU Funded KNOWMAK project, ESID is now being developed as part of the EU funded RISIS 2 Project. ESID utilizes advanced machine learning and natural language processing techniques to collect information about social innovation projects and actors from the publicly available information on the web. ESID also uses some limited human annotation to train its machine learning models and to ensure the quality and the integrity of the data. The ESID database contains two sets of datasets, one being the subset of the other. The full dataset comprises of 9577 social innovation projects in total. The curated dataset which is part of the 9577 projects comprises of data that is high quality as it has been manually checked and annotated by different annotators. For these projects, ESID contains a title, type of social innovation with scores, summary, location and topic.', 'Social Innovation, new forms of innovation, social innovation projects, machine learning, Natural Language processing', 'https://pureportal.strath.ac.uk/en/publications/using-machine-learning-and-text-mining-to-classify-fuzzy-social-s; https://link.springer.com/chapter/10.1007%2F978-3-030-23281-8_13; https://link.springer.com/chapter/10.1007%2F978-3-319-91947-8_42', 'University of Strathclyde; https://www.strath.ac.uk/', 'Roseline Antai; roseline.antai@strath.ac.uk', 'ESID is a comprehensive and authoritative source of information on social innovation projects and actors in Europe and beyond. ', 'https://zenodo.org/record/4605814#.YFBrO537Q2w'),
(14, 'ISI-Trademark Data Collection (ISI-TM)', 'The ISI-Trademark Data Collection (ISI-TM) provides detailed information on trademarks filed at the EUIPO and at the USPTO. It includes information on all trademarks filed at the EUIPO since 1996 and at the USPTO since the early 19th century onwards. The data provided are the trademark number, as a unique identifier, applicant and representative information, several relevant dates application, registration,...), trademark type (word, figurative, sound,…), the language of the trademark, information on opposition and information on relevant classifications. The trademark information is processed and set-up into an Oracle relational database system (RDBMS). The ISI-Trademark Data Collection (ISI-TM) offers using trademarks as a complementary and relatively \"close to the market\" indicator for new products and innovation activities, especially in the service sector. It offers the possibility to be linked to other datasets at the organizational level, within the RCF as well as externally. ', 'Trademarks, IPR, EUIPO, USPTO', NULL, 'Fraunhofer Institute for Systems and Innovation Research ISI; https://www.isi.fraunhofer.de/en.html', 'Peter Neuhäusler; peter.neuhaeusler@isi.fraunhofer.de', 'The ISI-Trademark Data Collection (ISI-TM) provides detailed information on trademarks filed at the EUIPO and at the USPTO.', 'https://zenodo.org/record/4633061#.YFsiRNwxmUk'),
(15, 'EFIL', 'EFIL – European dataset of public R&D funding instruments aims at enabling users to investigate public R&D funding in Europe at the level of project funding instruments and Research Funding Organizations (RFO), addressing questions related to policy design and policy implementation. Main objectives of EFIL are: - re-composing and characterizing the portfolios of funding instruments of relevant RFOs from selected European countries (AT; CH; CZ; DK; EE; DE; IT; NO; UK, for the time being); - producing evidence of the structural, procedural, and allocational aspects of funding instruments, as well as organizational profiles. EFIL provides a set of descriptors with an emphasis on a general characterization of the instrument (orientation, delegation mode of funding, composition of decision-making body, etc.); funding allocation criteria and eligible beneficiaries; and funding amounts allocated through each funding instrument. The temporal coverage assumes 2017 and 2018 as reference years for the re-composition of portfolios, but funding data are covered for the period 2018-backwards to 2010. A peculiar feature related to the facility is the possibility to characterize the instruments through text analysis, key words, and vocabularies. Indeed, the database is complemented by a repository of official documents hosted on a cloud – composed of instrument calls, guidelines for participants, descriptions on official webpages, etc. – that is accessible to the database user.', 'R&D Policy Instruments, Project Funding, Research Funding Organizations', 'https://www.frontiersin.org/articles/10.3389/frma.2021.712839/full', 'IRCrES-CNR; http://www.ircres.cnr.it', 'Andrea Orazio Spinello; andrea.spinello@ircres.cnr.it', 'EFIL provides data useful for characterizing research funding instruments managed by selected European Research Funding Organizations.', 'https://zenodo.org/record/6367802#.YjSN5TUo-Mq');
-- riba_app.ar_access_request_dataset definition

-- riba_app.ar_dataset_access_managers definition

CREATE TABLE `ar_dataset_access_managers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `dataset_id` int NOT NULL,
  `user_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ar_dataset_access_managers_dataset_id_fkey` (`dataset_id`),
  CONSTRAINT `ar_dataset_access_managers_dataset_id_fkey` FOREIGN KEY (`dataset_id`) REFERENCES `ar_dataset` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- populate ar_dataset_access_managers table
INSERT INTO `ar_dataset_access_managers` (`dataset_id`,`user_email`) VALUES
	 (1,'Massimiliano.Guerini@polimi.it'),
	 (2,'patricia.laurens@esiee.fr'),
	 (3,'clara@cwts.leidenuniv.nl'),
	 (4,'thomas.scherngell@ait.ac.at'),
	 (5,'patricia.laurens@esiee.fr'),
	 (6,'andrea.spinello@ircres.cnr.it'),
	 (7,'eric@nifu.no'),
	 (8,'lionel.villard@esiee.fr'),
	 (9,'tesch@dzhw.eu'),
	 (10,'blepori@usi.ch'),
	 (12,'francesca.tenca@polimi.it'),
	 (13,'roseline.antai@strath.ac.uk'),
	 (14,'peter.neuhaeusler@isi.fraunhofer.de'),
	 (15,'andrea.spinello@ircres.cnr.it'),
	 (1,'admin@risis.io'),
	 (2,'admin@risis.io'),
	 (3,'admin@risis.io'),
	 (4,'admin@risis.io'),
	 (5,'admin@risis.io'),
	 (6,'admin@risis.io'),
	 (7,'admin@risis.io'),
	 (8,'admin@risis.io'),
	 (9,'admin@risis.io'),
	 (10,'admin@risis.io'),
	 (11,'admin@risis.io'),
	 (12,'admin@risis.io'),
	 (13,'admin@risis.io'),
	 (14,'admin@risis.io'),
	 (15,'admin@risis.io');

-- riba_app.ar_access_request_dataset definition

CREATE TABLE `ar_access_request_dataset` (
  `id` int NOT NULL AUTO_INCREMENT,
  `access_request_id` int NOT NULL,
  `dataset_id` int NOT NULL,
  `access_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `host_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferred_visit_dates` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `re_imbursement` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_main` int NOT NULL DEFAULT '0',
  `decision` int DEFAULT NULL,
  `decision_date` datetime(3) DEFAULT NULL,
  `am_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `is_cancelled` int NOT NULL DEFAULT '0',
  `email_sent` int NOT NULL DEFAULT '0',
  `email_sent_at` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ar_access_request_dataset_dataset_id_fkey` (`dataset_id`),
  KEY `ar_access_request_dataset_access_request_id_fkey` (`access_request_id`),
  CONSTRAINT `ar_access_request_dataset_access_request_id_fkey` FOREIGN KEY (`access_request_id`) REFERENCES `ar_access_request` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `ar_access_request_dataset_dataset_id_fkey` FOREIGN KEY (`dataset_id`) REFERENCES `ar_dataset` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `ar_access_request`;
-- riba_app.ar_access_request definition

CREATE TABLE `ar_access_request` (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_title` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_acronym` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_objectives` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_summary` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_needed` mediumtext COLLATE utf8mb4_unicode_ci,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `step` int NOT NULL DEFAULT '0',
  `first_question_am` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_question_am` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `third_question_am` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forth_question_am` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `decision_am` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reasoning_am` mediumtext COLLATE utf8mb4_unicode_ci,
  `first_question_prb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_question_prb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `third_question_prb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forth_question_prb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `decision_prb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reasoning_prb` mediumtext COLLATE utf8mb4_unicode_ci,
  `decision_pc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updated_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `cv_url` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cancelled` int NOT NULL DEFAULT '0',
  `is_submitted` int NOT NULL DEFAULT '0',
  `should_be_revised` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ar_access_request_user_id_fkey` (`user_id`),
  CONSTRAINT `ar_access_request_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/**
 * Dataset Controller
 * @namespace Dataset_Controller
 */

const datastoreService = require('../services/datastore');
const errors = require('../utils/errors');

/**
 * Function to request list of datasets
 * @method
 * @async
 * @memberof Dataset_Controller
 * @param {object} req - Request by User
 * @returns {array} List of datasets
 */
async function listAllDatasets (req) {
  const { user, context: { logger } } = req;
  logger.debug(`[Dataset Controller] Getting all datasets for user ${user.username}`);
  const datasets = await datastoreService.getDatasets(req);
  return datasets;
}

/**
 * Function to request datasets by id
 * @method
 * @async
 * @memberof Dataset_Controller
 * @param {object} req - Request by User
 * @returns {object} Dataset found
 */

async function getSingleDataset (req) {
  const { params: { datasetId }, context: { logger } } = req;
  logger.debug(`[Dataset Controller] Getting dataset ${datasetId}`);
  const dataset = await datastoreService.getDataset(req);
  if (!dataset) {
    logger.debug(`[Dataset Controller] There is no dataset ${datasetId}`);
    throw new errors.NotFoundError('There is no dataset');
  }
  return dataset;
}

async function getFileMetadata (req) {
  const { params: { fileId }, context: { logger } } = req;
  logger.debug(`[Dataset Controller] metadata for file ${fileId} requested`);
  const metadata = await datastoreService.getFileMetadata(req);
  logger.debug('[Dataset Controller] metadata', metadata);
  return metadata;
}

/**
 * Function to delete a dataset by id
 * @method
 * @async
 * @memberof Dataset_Controller
 * @param {object} req - Request by User
 * @returns {object} Empty object with status code 200
 */
async function deleteDataset (req) {
  const { params: { datasetId }, context: { logger } } = req;
  const dataset = await getSingleDataset(req);
  logger.debug(`[Dataset Controller] Deleting a dataset ${datasetId}`);
  try {
    await datastoreService.deleteDataset(req);
    return dataset;
  } catch (error) {
    logger.error(`[Dataset Controller] Error deleting dataset ${datasetId}`);
    logger.error(error);
    throw new errors.RuntimeError('Error deleting dataset');
  }
}

/**
 * Function to create a dataset
 * @method
 * @async
 * @memberof Dataset_Controller
 * @param {object} req - Request by User
 * @returns {object} Dataset created
 */
async function createDataset (req) {
  const { context: { logger } } = req;

  logger.debug('[Dataset Controller] Creating Dataset');
  const newDataset = await datastoreService.createDataset(req);
  return newDataset;
}

/**
 * Function to update a dataset by id
 * @method
 * @async
 * @memberof Dataset_Controller
 * @param {object} req - Request by User
 * @returns {object} Dataset updated
 */
async function updateDataset (req) {
  const { params: { datasetId }, context: { logger } } = req;
  await getSingleDataset(req);
  logger.debug(`[Dataset Controller] Updating Dataset ${datasetId}`);
  const updatedDataset = await datastoreService.updateDataset(req);
  if (!updatedDataset || updatedDataset.length === 0) {
    throw new errors.RuntimeError('Could not update dataset');
  }
  return await getSingleDataset(req);
}

/**
 * Function to upload files to a dataset
 * @method
 * @async
 * @memberof Dataset_Controller
 * @param {object} req - Request by User
 * @returns {array} List of the files
 */
async function uploadFiles (req) {
  const { params: { datasetId }, context: { logger } } = req;
  await getSingleDataset(req);
  logger.debug(`[Dataset Controller] Uploading file into a Dataset ${datasetId}`);
  const uploadFilesDataset = await datastoreService.uploadFiles(req);
  if (!uploadFilesDataset || uploadFilesDataset.length === 0) {
    throw new errors.RuntimeError('Unable to upload dataset files');
  }
  return uploadFilesDataset;
}

/**
 * Function to get files of a dataset
 * @method
 * @async
 * @memberof Dataset_Controller
 * @param {object} req - Request by User
 * @returns {array} List of the files
 */
async function getFiles (req) {
  const { params: { datasetId }, context: { logger } } = req;
  logger.debug(`[Dataset Controller] Getting files of a Dataset ${datasetId}`);
  req.dataset = await getSingleDataset(req);
  const files = await datastoreService.downloadDataset(req);
  return files;
}

module.exports = {
  listAllDatasets,
  getSingleDataset,
  getFileMetadata,
  createDataset,
  deleteDataset,
  updateDataset,
  uploadFiles,
  getFiles
};

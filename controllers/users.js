/**
 * Users Controller
 * @namespace Users_Controller
 */

const datasetController = require('./dataset');
const projectController = require('./projects');
const scenarioController = require('./scenario');
const jobController = require('./job');
const userService = require('../services/db/user.js');
const risorService = require('../services/risor.js');
const datastoreService = require('../services/datastore.js');
const permissionsService = require('../services/permissions');
const errors = require('../utils/errors');

/**
 * Function to get a user information.
 * @method
 * @async
 * @memberof Users_Controller
 * @param {object} req - Request by User
 * @returns {object} User information
 */
async function getUserInformation (req, keyToSearch = 'id') {
  const { params: { userId }, context: { logger } } = req;
  logger.debug(`[Users Controller] Getting user information. User ${userId}`);
  await permissionsService.validateAdminPermissions(req);
  const userInformation = await userService.getUser(req, userId, keyToSearch);
  if (!userInformation) {
    logger.error(`[Users Controller] The user ${userId} does not exist`);
    throw new errors.ArgumentError(`The user ${userId} does not exist`);
  }
  return userInformation;
}

/**
 * Function to create service credentials for a user.
 * @method
 * @async
 * @memberof Users_Controller
 * @param {object} req - Request by User
 * @returns {object} User information
 */
async function createServiceCredentials (req) {
  const { user, body: { providerId }, context: { logger } } = req;
  if (user.service_providers.find(provider => provider.idService === providerId)) {
    logger.debug(`The user ${user.username} has already the provider ${providerId}`);
    throw new errors.ArgumentError(`The user has already the provider ${providerId}`);
  }
  logger.debug(`[Users Controller] Creating a service credentials for the user ${user.id}`);
  req.provider = await getProvider(req);
  try {
    const response = await userService.createService(req);
    user.service_providers.push(response);
  } catch (error) {
    logger.error('Error creating service credential');
    logger.debug(error);
    throw new errors.RuntimeError();
  }
  return user;
}

/**
 * Function to update service credentials for a user.
 * @method
 * @async
 * @memberof Users_Controller
 * @param {object} req - Request by User
 * @returns {object} User information
 */
async function updateServiceCredentials (req) {
  const { user, params: { serviceId }, context: { logger } } = req;
  const userProvider = user.service_providers.find(provider => provider.idService === serviceId);
  if (!userProvider) {
    logger.debug(`The user ${user.username} doesn't have the provider ${serviceId}`);
    throw new errors.ArgumentError(`The user doesn't have the provider ${serviceId}`);
  }
  logger.debug(`[Users Controller] Updating a service credentials for the user ${user.id}`);
  try {
    req.providerId = userProvider.id;
    await userService.updateService(req);
  } catch (error) {
    logger.error(`Error updating service ${serviceId} credential for the user ${user.id}`, error);
    throw new errors.RuntimeError();
  }
  return user;
}

/**
 * Function to find the provider
 * @method
 * @async
 * @memberof Users_Controller
 * @param {object} req - Request by User
 * @returns {object} Provider information
 */
async function getProvider (req) {
  const { body: { providerId, token }, context: { logger } } = req;
  let providers = await risorService.getProviders(req);
  if (providers.statusCode !== 200) {
    logger.debug(`Error getting providers in risor. Status code ${providers.statusCode}`, providers.body);
    throw new errors.RuntimeError();
  }
  providers = JSON.parse(providers.body);
  const providerFound = providers.find(provider => provider.id === providerId);
  if (!providerFound || !token) {
    logger.debug(`Wrong information to create a service credential for provider ${providerId} and token ${token}`);
    throw new errors.ArgumentError('Wrong information to create a service credential');
  }
  return providerFound;
}

/**
 * Function to get a stats of the user.
 * @method
 * @async
 * @memberof Users_Controller
 * @param {object} req - Request by User
 * @returns {object} Stats of the user
 */
async function stats (req) {
  const { user, context: { logger } } = req;
  logger.debug(`[Users Controller] Getting stats of the user ${user.username}`);
  let projects; let datasets; let scenarios; let jobs; let scenariosImported; let datasetsImported = [];
  const statsResult = {};
  try {
    projects = await projectController.listAllUserProjects(req);
    scenariosImported = getScenariosImported(projects);
    datasetsImported = getDatasetsImported(projects);
    statsResult.projects = {
      count: projects.length,
      owned: projects.filter(project => project.author.id === user.id).length
    };
  } catch (error) {
    projects = { error: true };
    statsResult.projects = projects;
  }
  try {
    datasets = await datasetController.listAllDatasets(req);
    statsResult.datasets = {
      count: datasets.length,
      imported: datasetsImported.length
    };
  } catch (error) {
    datasets = { error: true };
    statsResult.datasets = datasets;
  }
  try {
    scenarios = await scenarioController.listAllScenarios(req);
    statsResult.scenarios = {
      count: scenarios.length,
      imported: scenariosImported.length
    };
  } catch (error) {
    scenarios = { error: true };
    statsResult.scenarios = scenarios;
  }
  try {
    jobs = await jobController.getJobs(req);
    statsResult.jobs = {
      executing: jobs.filter(job => job.state.slug === 'running').length,
      waiting: jobs.filter(job => job.state.slug === 'not-ready').length
    };
  } catch (error) {
    jobs = { error: true };
    statsResult.jobs = jobs;
  }

  if (!projects.error && !jobs.error && projects.length) {
    // The last project is the project with the most recently executed/created job
    const lastJob = jobs.sort((d1, d2) => { return new Date(d2.execution_datetime) - new Date(d1.execution_datetime); })[0];
    let lastProject;
    if (!lastJob) {
      lastProject = projects.sort((d1, d2) => { return new Date(d2.creation_datetime) - new Date(d1.creation_datetime); })[0];
      statsResult.project_most_used = lastProject;
    } else {
      lastProject = projects.find(project => project.id === lastJob.project.id);
      // The most used projects is the project with the most jobs
      statsResult.project_most_used = projects.reduce((prev, current) => (prev.jobs.length > current.jobs.length) ? prev : current);
    }
    statsResult.last_project = lastProject;
  }

  return statsResult;
}

function getScenariosImported (projects) {
  let scenariosImported = [];
  projects.forEach(project => {
    scenariosImported = [...scenariosImported, ...project.scenarios.map(scenario => scenario.id)];
  });
  return Array.from(new Set(scenariosImported));
}

function getDatasetsImported (projects) {
  let datasetsImported = [];
  projects.forEach(project => {
    datasetsImported = [...datasetsImported, ...project.datasets.map(dataset => dataset.id)];
  });
  return Array.from(new Set(datasetsImported));
}

/**
 * Function to get information about user in session
 * @method
 * @async
 * @param {object} req - Request by User
 * @returns {object} Information of user in session
 */
async function getOrCreateUser (req) {
  req.user = await userService.getUser(req, req.tokenDecoded.id);
  req.user = await syncUser(req);
  return req.user;
}

/**
 * Function to create an user
 * @method
 * @async
 * @param {object} req - Request by User
 * @returns {object} Information of user
 */
async function syncUser (req) {
  const { user, tokenDecoded, context: { logger } } = req;
  logger.info(`Checking ${tokenDecoded.id} existence in RCF database`);
  let dbUser;

  // Step 1. User in RCF database
  if (!user) {
    logger.info(`${tokenDecoded.id} not in RCF database. Creating`);
    const dbUserInput = {
      id: tokenDecoded.id,
      email: tokenDecoded.email,
      username: tokenDecoded.preferred_username,
      first_name: tokenDecoded.given_name,
      last_name: tokenDecoded.family_name,
      image: tokenDecoded.image,
      jti: tokenDecoded.jti
    };
    logger.debug('Creating RCF database user with dbUserInput', dbUserInput);
    try {
      req.user = dbUserInput;
      dbUser = await userService.createUser(req);
      logger.info(`User ${tokenDecoded.id} added to database`);
    } catch (error) {
      logger.error('Error creating user in RCF database', error);
      throw new errors.RuntimeError('Error creating user in RCF database', error);
    }
  } else {
    logger.info(`${tokenDecoded.id} present in RCF database.`);
    dbUser = user;
  }

  // Ensure user object is in request
  req.user = dbUser;

  // Step 2. Dataverse
  // If user has provider dataverse, all has been done
  const userProvider = dbUser.service_providers.find(provider => provider.idService === 'datastore');
  if (!userProvider) {
    // If user doesnt have provider dataverse, start working

    logger.info(`User ${tokenDecoded.id} does not have datastore provider`);

    // 2.1. User in dataverse
    let dataverseUser = await datastoreService.getDatastoreUser(req);
    logger.info('dataverseUser', dataverseUser);
    if (!dataverseUser) {
      logger.info(`User ${tokenDecoded.id} does not exist in dataverse`);
      logger.info(`Creating user ${tokenDecoded.id} in dataverse`);
      dataverseUser = await datastoreService.createDatastoreUser(req);
    }
    logger.info(`User ${tokenDecoded.id} in dataverse`);
    logger.debug(`User ${tokenDecoded.id} dataverse user`, dataverseUser);

    // 2.2 Dataverse token
    logger.info(`Setting ${tokenDecoded.id} dataverse token`);
    req.datastoreUser = dataverseUser;
    dataverseUser.apiToken = await datastoreService.getUserApiToken(req);

    // 2.3. dataverse token -> RCF service credential in RIBA
    logger.info(`Persisting ${tokenDecoded.id} dataverse token as RCF service credential`);
    // Needed for createServiceCredentials
    if (!req.body) {
      req.body = {};
    }
    req.body.providerId = 'datastore';
    req.body.token = dataverseUser.apiToken;
    req.user = dbUser;
    dbUser = await createServiceCredentials(req);
    logger.info(`Persisted ${tokenDecoded.id} dataverse token successfully`);
  } else {
    logger.info(`User ${tokenDecoded.id} has datastore provider in dataverse`);
  }

  return dbUser;
}

/**
 * Function to update an information of the user
 * @method
 * @async
 * @param {object} req - Request by User
 * @returns {object} Information of user
 */
async function updateUserInfo (req) {
  const { tokenDecoded, user, context: { logger } } = req;
  logger.debug(`[User Service] Preparing object for the user ${tokenDecoded.id} to be updated`);
  req.user = { jti: tokenDecoded.jti };
  if (tokenDecoded.email !== undefined && tokenDecoded.email !== user.email) {
    req.user.email = tokenDecoded.email;
  }
  if (tokenDecoded.given_name !== undefined && tokenDecoded.given_name !== user.first_name) {
    req.user.first_name = tokenDecoded.given_name;
  }
  if (tokenDecoded.family_name !== undefined && tokenDecoded.family_name !== user.last_name) {
    req.user.last_name = tokenDecoded.family_name;
  }
  if (tokenDecoded.image !== undefined && tokenDecoded.image !== user.image) {
    req.user.image = tokenDecoded.image;
  }
  if (tokenDecoded.preferred_username !== undefined && tokenDecoded.preferred_username !== user.username) {
    req.user.username = tokenDecoded.preferred_username;
  }
  try {
    return await userService.updateUser(req, tokenDecoded.id);
  } catch (error) {
    logger.error(`Error updating user ${tokenDecoded.id}`);
    logger.error(error);
    throw new errors.RuntimeError('Error updating user information');
  }
}

module.exports = {
  stats,
  getUserInformation,
  getOrCreateUser,
  updateUserInfo,
  createServiceCredentials,
  updateServiceCredentials
};

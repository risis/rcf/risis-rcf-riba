/**
 * Job Controller
 * @namespace Job_Controller
 */
const url = require('url');
const jobService = require('../services/db/job');
const notificationService = require('../services/db/notification');
const errors = require('../utils/errors');
const minioService = require('../services/minio');
const pathLibrary = require('path');
const projectService = require('../services/db/project');
const appRoot = require('app-root-path').toString();
const path = require('path');
const fs = require('fs-extra');
const writeYamlFile = require('write-yaml-file');
const settings = global.config.settings;
const tmpRootPath = path.join(appRoot, settings.files.tmpDir);
const slugify = require('slugify');

const yaml = require('js-yaml');

const { storage: storageConfig } = global.config.services;

/**
 * Function to create a job
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @param {string} slugJob - slug of new job
 * @returns {object} - Job created
 */
async function createJob (req, slugJob) {
  const { context: { logger } } = req;
  logger.debug(`[Scenario  Controller] Creating a job with slug ${slugJob}`);

  const creationJob = await jobService.createJob(req, slugJob);
  if (!creationJob) {
    logger.error(`[Job Controller] Error creating a Job with slug ${slugJob}`);
    throw new errors.RuntimeError('Error creating a Job');
  }
  return creationJob;
}

/**
 * Function to get a list of jobs
 * @method
 * @async
 * @memberof Job_Controller
 * @param {object} req - Request by User
 * @returns {array} List of jobs
 */
async function getJobs (req) {
  const { user, context: { logger } } = req;
  logger.debug(`[Job Controller] Getting all list of the jobs by user ${user.username}`);
  const listJobs = await jobService.getJobs(req);
  for (let i = 0; i < listJobs.length; i++) {
    const job = listJobs[i];
    req.job = job;
    req.project = job.project;
    if (process.env.NODE_ENV !== 'test') {
      job.files = await getFilesStorage(req);
    }
  }
  return listJobs;
}

/**
 * Function to get a configuration of a specific job
 * @method
 * @async
 * @memberof Job_Controller
 * @param {object} req - Request by User
 * @returns {object} Scenario configuration
 */
async function getSpecificJobConfig (req) {
  const { context: { logger } } = req;
  req.job = await getSingleJob(req);
  verifyAuthorization(req, req.job.project);
  logger.debug(`[Job Controller] Getting configuration for project ${req.job.project.id}, scenario ${req.job.scenario.id}, and job ${req.job.id}`);
  return await getConfigurationJob(req);
}

/**
 * Function to restore a configuration of a specific job
 * @method
 * @async
 * @memberof Job_Controller
 * @param {object} req - Request by User
 * @returns {object} Scenario configuration
 */
async function restoreJobConfig (req) {
  const { user, context: { logger } } = req;
  req.job = await getSingleJob(req);
  verifyAuthorization(req, req.job.project);
  const commonPath = `${user.id}/projects/${req.job.project.id}/scenarios/${req.job.scenario.id}`;
  const nameFile = 'input/input.yaml';
  const origenPath = `${commonPath}/${req.job.id}/${nameFile}`;
  req.project.params = { projectId: req.job.project.id };
  const projectFull = await projectService.getProject(req);
  const lastJob = getLastJob(req, req.job.scenario.id, projectFull.jobs);
  const destinationPath = `${commonPath}/${lastJob.id}/${nameFile}`;
  logger.debug(`[Job Controller] Coping configuration from ${origenPath} to ${destinationPath}`);
  await minioService.copyFile(origenPath, destinationPath, req);
  return {
    scenarioId: req.job.scenario.id
  };
}

/**
 * Function to return the last job of a project
 * @method
 * @async
 * @memberof Job_Controller
 * @param {object} req - Request by User
 * @returns {object} Scenario configuration
 */
function getLastJob (req, scenarioId, projectJobs = []) {
  const { context: { logger } } = req;
  logger.debug('[Job Controller] Finding the last Job');
  let jobs = projectJobs.filter(job => job.scenario.id === scenarioId);
  jobs = jobs.sort((d1, d2) => { return new Date(d2.creation_datetime) - new Date(d1.creation_datetime); });
  if (!jobs || jobs.length === 0) {
    return null;
  }
  logger.debug(`[Job Controller] Last job = ${jobs[0].id}`);
  return jobs[0];
}

/**
 * Function to get a configuration of a job
 * @method
 * @async
 * @memberof Job_Controller
 * @param {object} req - Request by User
 * @returns {object} configuration of project
 */
async function getConfigurationJob (req) {
  const { context: { logger } } = req;
  const stream = await minioService.getFile(req);
  logger.debug('[Job Controller] Getting input yaml');
  return new Promise((resolve, reject) => {
    stream.on('data', async (minioFile) => {
      resolve(yaml.load(minioFile.toString()));
    });
    stream.on('error', (error) => {
      logger.error(`[Job Controller] Error getting file ${error}`);
      resolve({});
    });
  });
}

/**
 * Function to create a configuration of a job
 * @method
 * @async
 * @memberof Job_Controller
 * @param {object} req - Request by User
 * @param {object} lastJobConfiguration - Last configuration for the last job
 * @returns {object} configuration of project
 */
async function createConfigurationJob (req, lastJobConfiguration) {
  const { project, user, scenario, context: { logger } } = req;
  const slugJob = `${slugify(project.title)}-${slugify(scenario.id)}-1-0-0-0`;
  logger.debug(`[Job Controller] Creating a configuration for a new job with slug ${slugJob}`);
  const newJob = await createJob(req, slugJob);
  req.job = newJob;
  const path = `${user.id}/projects/${project.id}/scenarios/${scenario.id}`;
  const createFileCompleted = await createParamsFileMinio(req, `${path}/${newJob.id}`, lastJobConfiguration);
  const createOutputFolder = await createOutputFolderMinio(req, `${path}/${newJob.id}`);
  await fs.remove(`${tmpRootPath}/${user.id}`);
  if (!createFileCompleted || !createOutputFolder) {
    logger.error(`[Job Controller] Error: The files could not be created in path ${path}`);
    throw new errors.RuntimeError('The files could not be created');
  }
  return lastJobConfiguration;
}

/**
 * Function create a file params.yml into minio
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @param {string} path - Path of the folder to create a params file.
 * @param {object} lastJobConfiguration - Last configuration for the last job
 * @param {array} inputs - List of inputs needs to run a scenario
 */
async function createParamsFileMinio (req, path, lastJobConfiguration) {
  const { context: { logger } } = req;
  logger.debug(`[Scenario  Controller] Creating input.yaml and put it in ${path}/input`);
  const nameFile = 'input.yaml';
  const localInputFolderPath = `${tmpRootPath}/${path}`;
  await fs.mkdir(localInputFolderPath, { recursive: true });
  const localInputFilePath = `${localInputFolderPath}/${nameFile}`;
  const minioInputFilePath = `${path}/input/${nameFile}`;
  fs.createWriteStream(localInputFilePath);
  await writeYamlFile(localInputFilePath, lastJobConfiguration);
  const uploadFileToMinio = await minioService.putFile(
    minioInputFilePath,
    localInputFilePath,
    { 'Content-Type': 'text/yaml' },
    req
  );
  return uploadFileToMinio;
}

/**
 * Function create a output into minio
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @param {string} path - Path of the folder to create a params file.
 * @param {array} inputs - List of inputs needs to run a scenario
 */
async function createOutputFolderMinio (req, path) {
  const { context: { logger } } = req;
  logger.debug(`[Scenario  Controller] Creating .keep file and put it in ${path}/outputs`);
  const nameFile = '.keep';
  const localInputFolderPath = `${tmpRootPath}/${path}`;
  await fs.mkdir(localInputFolderPath, { recursive: true });
  const localInputFilePath = `${localInputFolderPath}/${nameFile}`;
  const minioInputFilePath = `${path}/outputs/${nameFile}`;
  fs.createWriteStream(localInputFilePath);
  await writeYamlFile(localInputFilePath, {});
  await minioService.makePathPublic(`${path}/outputs`);
  const uploadFileToMinio = await minioService.putFile(
    minioInputFilePath,
    localInputFilePath,
    { 'Content-Type': 'text/yaml' },
    req
  );
  return uploadFileToMinio;
}

/**
 * Function to get a list of files into output folder from minio
 * @method
 * @async
 * @memberof Job_Controller
 * @param {object} req - Request by User
 * @returns {array} List of files.
 */
async function getFilesStorage (req) {
  const { user, project, job, context: { logger } } = req;

  const path = `${user.id}/projects/${project.id}/scenarios/${job.scenario.id}/${job.id}/outputs`;
  const stream = minioService.listPathContent(
    path,
    req
  );
  const filesStorage = [];
  logger.debug(`[Job Controller] Getting files on path ${path}`);
  return new Promise((resolve, reject) => {
    stream.on('data', async (minioFile) => {
      const name = pathLibrary.basename(minioFile.name);
      const bucket = `${storageConfig.buckets.workspaces.key}`;
      const urlMinio = url.format(storageConfig.uri);
      filesStorage.push(
        {
          type: minioFile.metadata['content-type'],
          lastmodified_datetime: minioFile.lastModified,
          name,
          etag: minioFile.etag,
          size: minioFile.size,
          uri: `${urlMinio}/${bucket}/${minioFile.name}`
        }
      );
    });
    stream.on('error', (error) => {
      logger.error(`[Job Controller] Error getting files ${error}`);
      resolve([]);
    });
    stream.on('end', () => {
      resolve(filesStorage);
    });
  });
}

/**
 * Function to get a job from service
 * @method
 * @async
 * @memberof Job_Controller
 * @param {object} req - Request by User
 * @returns {object} Job founded
 */
async function getJobFromService (req) {
  const { params: { jobId }, context: { logger } } = req;
  logger.debug(`[Job Controller] Getting job ${jobId}`);
  const job = await jobService.getJob(req);
  if (!job) {
    logger.error(`[Job Controller] Error: There is no job created with id ${jobId}`);
    throw new errors.NotFoundError('There is no job created');
  }
  if (job.delete_datetime) {
    logger.error(`[Job Controller] Error: The job with id ${jobId} is eliminated, so is impossible get the job`);
    throw new errors.NotFoundError('There job is eliminated');
  }
  return job;
}

/**
 * Function to get a job by id
 * @method
 * @async
 * @memberof Job_Controller
 * @param {object} req - Request by User
 * @returns {object} Job founded
 */
async function getSingleJob (req) {
  const { params: { jobId }, context: { logger } } = req;
  logger.debug(`[Job Controller] Getting job ${jobId}`);
  req.job = await getJobFromService(req);
  const project = req.job.project;
  await verifyAuthorization(req, project);
  req.project = project;
  if (process.env.NODE_ENV !== 'test') {
    req.job.files = await getFilesStorage(req);
  }
  return req.job;
}

/**
 * Function to delete a job by id
 * @method
 * @async
 * @memberof Job_Controller
 * @param {object} req - Request by User
 * @returns {object} empty
 */
async function deleteJob (req) {
  const { params: { jobId }, context: { logger } } = req;

  const job = await getJobFromService(req);
  await verifyAuthorization(req, job.project);

  logger.debug(`[Job Controller] Deleting job ${jobId}`);
  try {
    await jobService.deleteJob(req);
    return {};
  } catch (error) {
    logger.error(`Error deleting job ${jobId}`);
    logger.error(error);
    throw new errors.RuntimeError('Error deleting job');
  }
}

/**
 * Function callback execute to change data
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {object} Project details
 */
async function updateJob (req) {
  const { params: { jobId }, body, context: { logger } } = req;

  const job = await getJobFromService(req);
  await verifyAuthorization(req, job.project);

  const { context, ...newData } = body;

  const dataJob = {};
  if (newData.percentage) {
    dataJob.percentage = newData.percentage;
  }

  if (newData.state) {
    dataJob.state = {
      connect: { slug: newData.state }
    };
    if (newData.state === 'finished') {
      dataJob.percentage = 100;
    }
  }

  if (newData.response) {
    dataJob.response = newData.response;
  }

  if (context && context.instanceId) {
    dataJob.engine_id = context.instanceId;
  }

  logger.debug(`[Job Controller] Updating Job ${jobId}`);
  if (Object.keys(dataJob).length === 0) {
    logger.error('[Job Controller] No information to be updated');
    throw new errors.ArgumentError('No information to be updated');
  }
  try {
    const resultJobChangeStatus = await jobService.updateJob(req, dataJob);
    await updateProjectState(req, job.project, newData.state);
    if (req.isService) {
      await createNotification(req, resultJobChangeStatus, newData.state);
    }
    return resultJobChangeStatus;
  } catch (error) {
    logger.error('Unable to update status');
    logger.error(error);
    throw new errors.RuntimeError('Unable to update status');
  }
}

async function createNotification (req, job, state) {
  if (state === 'finished' || state === 'stopped') {
    const { context: { logger } } = req;
    if (state === 'finished') {
      const error = job.response && job.response.error !== undefined;
      req.body = {
        url: `/projects/${job.project.id}/view/outputs?job=${job.id}`,
        reason: 'job_finished'
      };
      if (error) {
        req.body.title = 'Oops... Scenario execution failed';
        req.body.details = `The execution of the scenario in the project ${job.project.title} finished with errors.`;
        req.body.type = 'error';
      } else {
        req.body.title = 'Scenario execution is finished!';
        req.body.details = `The execution of the scenario in the project ${job.project.title} has been completed.`;
        req.body.type = 'success';
      }
    } else { // state stopped
      req.body = {
        url: `/projects/${job.project.id}/view`,
        title: 'Scenario execution stopped',
        details: `The scenario ${job.scenario.id} on project ${job.project.title} has been stopped`,
        reason: 'job_stopped',
        type: 'info'
      };
    }
    try {
      const originalId = req.user.id;
      req.user.id = job.author.id;
      const notification = await notificationService.createNotification(req);
      req.user.id = originalId;
      const io = req.app.get('io');
      notification.projectId = job.project.id;
      io.to(`/notifications/live/${job.author.id}`).emit('job_finished', notification);
    } catch (error) {
      logger.error('Error creating a notification');
      logger.debug(error);
    }
  }
}

async function updateProjectState (req, project, state) {
  if (state === 'finished' || state === 'running' || state === 'stopped') {
    req.params.projectId = project.id;
    req.body = {
      state
    };
    return await projectService.patchProject(req);
  }
  return true;
}

async function verifyAuthorization (req, project) {
  if (!req.isService) {
    const { user, context: { logger } } = req;
    logger.debug(`[Job Controller] Verifying if the user ${user.username} has permissions`);
    req.params.projectId = project.id;
    const projectFull = await projectService.getProject(req);
    if (projectFull.delete_datetime) {
      logger.error(`[Job Controller] Error: The project ${projectFull.id} is already eliminated`);
      throw new errors.NotFoundError('The project is eliminated');
    }
    const isAuthor = user.id === projectFull.author.id;
    const isOwner = projectFull.owners.find(owner => owner.id === user.id);
    const isMember = projectFull.members.find(members => members.id === user.id);
    if (!isAuthor && !isOwner && !isMember) {
      logger.error('[Job Controller] The user can not get the resource');
      throw new errors.UnauthorizedError('The user can not get the resource');
    }
  }
}

/**
 *
 * Set all running jobs as failed.
 * Provisional orchestrator-only endpoint to be called at orchestrator startup.
 * @param { } req Request object with a json body with a response attribute
 */
async function resetRunning (req) {
  // Only orchestrator is allowed to perform this operation
  const { context: { logger }, body: { response } } = req;
  if (req.isService) {
    // Retrieve all running jobs
    const jobs = await jobService.getRunningJobs(req);
    logger.debug(jobs);
    // Update each one to "failed"
    return await Promise.all(
      jobs.map(job => {
        const jobReq = {
          ...req,
          app: req.app, //
          params: { jobId: job.id },
          body: {
            state: 'finished',
            response
          }
        };
        return updateJob(jobReq);
      })
    );
  } else {
    logger.error('Error: Not authorized to reset job status');
    throw new errors.UnauthorizedError('The user can not get the resource');
  }
}

module.exports = {
  getJobs,
  getFilesStorage,
  createJob,
  deleteJob,
  getSingleJob,
  updateJob,
  getConfigurationJob,
  createConfigurationJob,
  createParamsFileMinio,
  resetRunning,
  getSpecificJobConfig,
  restoreJobConfig,
  getLastJob
};

/**
 * Scenario Controller
 * @namespace Scenario_Controller
 */
const url = require('url');
const risorService = require('../services/risor');
const projectService = require('../services/db/project');
const jobService = require('../services/db/job');
const presetService = require('../services/db/preset');
const jobController = require('../controllers/job');
const errors = require('../utils/errors');
const slugify = require('slugify');
const minioService = require('../services/minio');

const { riba: ribaConfig, storage: storageConfig } = global.config.services;

/**
 * Function to get a list of scenarios of the user in a project selected
 * @method
 * @async
 * @memberof Scenario_Controller
 * @param {object} req - Request by User
 * @returns {array} List of scenarios found
 */
async function getScenariosByProject (req) {
  const { context: { logger } } = req;

  logger.debug(`[Scenario Controller] Getting Scenarios of project ${req.project.id}`);
  const scenariosIds = req.project.scenarios;
  const scenarios = [];
  if (!scenariosIds) {
    throw new errors.NotFoundError('The project has no scenarios');
  }
  for (let i = 0; i < scenariosIds.length; i++) {
    const scenarioProject = scenariosIds[i];
    req.params.scenarioId = scenarioProject.id;
    const scenario = await getSingleScenario(req);
    req.scenario = scenario;
    scenario.config = await getConfiguration(req);
    scenario.state = req.job.state.slug;
    scenario.project = req.project;
    scenarios[i] = scenario;
  }
  return scenarios;
}

async function getConfiguration (req) {
  req.job = jobController.getLastJob(req, req.scenario.id, req.project.jobs);
  return await jobController.getConfigurationJob(req);
}

async function getOrCreateConfig (req) {
  const lastJob = jobController.getLastJob(req, req.scenario.id, req.project.jobs);
  if (!lastJob) {
    return await jobController.createConfigurationJob(req, {});
  } else if (lastJob.state.id > 1) {
    req.job = lastJob;
    const jobConfiguration = await jobController.getConfigurationJob(req);
    return await jobController.createConfigurationJob(req, jobConfiguration);
  } else {
    req.job = lastJob;
    return await jobController.getConfigurationJob(req);
  }
}

async function createConfiguration (req) {
  const jobConfiguration = await getConfiguration(req);
  return await jobController.createConfigurationJob(req, jobConfiguration);
}

/**
 * Function to request list of scenarios
 * @method
 * @async
 * @memberof Scenario_Controller
 * @param {object} req - Request by User
 * @returns {array} List of scenarios
 */
async function listAllScenarios (req) {
  const { user, context: { logger } } = req;
  logger.debug(`[Scenario Controller] Getting all scenarios for user ${user.username}`);
  let scenarios = await risorService.getScenarios(req);
  scenarios = JSON.parse(scenarios.body);
  scenarios = scenarios.map(scenario => { scenario.user = user; return scenario; });
  return scenarios;
}

/**
 * Function to request a scenarios by id
 * @method
 * @async
 * @memberof Scenario_Controller
 * @param {object} req - Request by User
 * @returns {object} Scenarios found
 */
async function getSingleScenario (req) {
  const { params: { scenarioId }, context: { logger } } = req;
  logger.debug(`[Scenario Controller] Getting scenario ${scenarioId}`);

  const slug = slugify(scenarioId, { lower: true });
  let scenario = await risorService.getScenario(req, slug);
  scenario = JSON.parse(scenario.body);
  if (!scenario) {
    throw new errors.NotFoundError('There is no scenario');
  }
  return scenario;
}

/**
 * Function to run a scenario
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {object} Scenario details
 */
async function runScenario (req) {
  const { body, params: { projectId }, project, user, context: { logger } } = req;
  verifyAuthorization(req, project);
  const scenario = await getSingleScenario(req);
  req.scenario = scenario;
  req.params.scenarioId = scenario.id;
  const jobs = project.jobs.filter(job => job.scenario.id === req.params.scenarioId);
  const job = jobs.sort((d1, d2) => { return new Date(d2.creation_datetime) - new Date(d1.creation_datetime); })[0];
  if (!job) {
    logger.error('There is not a job created to run a scenario');
    throw new errors.ArgumentError('There is not a job created to run a scenario');
  }
  logger.debug(`[Scenario Controller] Running scenario ${scenario.id}`);
  const path = `${user.id}/projects/${projectId}/scenarios/${scenario.id}`;
  const jobId = job.id;
  req.params.jobId = jobId;
  const params = { ...body.params };
  if (!params.inputs) {
    params.inputs = {};
  }
  params.inputs['workspace-uri'] = `m4://${storageConfig.buckets.workspaces.key}/${path}/${jobId}`;
  params.inputs['project-id'] = projectId;
  params.inputs.token = req.token;
  ribaConfig.uri.pathname = `/api/jobs/${jobId}`;
  const userDatastoreToken = user.service_providers.find(service => service.idService.toLowerCase() === 'datastore');
  const data = {
    context: {
      callback_url: url.format(ribaConfig.uri),
      datastore: {
        token: userDatastoreToken ? userDatastoreToken.token : null
      }
    },
    params: params
  };
  await copyFilesMinio(req, path, jobId);
  req.body = {
    state: 'ready'
  };
  await projectService.patchProject(req);
  risorService.runScenario(req, scenario.id, scenario.scenario_version, data);
  await jobService.updateJob(req, { execution_datetime: new Date() });
  await createConfiguration(req);
  return scenario;
}

/**
 * Function to copy files in minio
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @param {string} path - Path the scenario folder.
 * @param {string} jobHash - Hash of the job execution.
 */
async function copyFilesMinio (req, path, jobHash) {
  const { context: { logger } } = req;
  logger.debug(`[Scenario  Controller] Coping files ${path}/model to ${path}/${jobHash}`);
  const steam = minioService.listPathContent(
    `${path}/model`,
    req
  );
  return new Promise((resolve, reject) => {
    steam.on('data', async (minioFile) => {
      minioService.copyFile(minioFile.name, `${path}/${jobHash}/${minioFile.name.split(`${path}/model/`)[1]}`, req);
    });
    steam.on('end', () => resolve());
    steam.on('error', () => resolve());
  });
}

function verifyAuthorization (req, project) {
  if (!req.isService) {
    const { user, context: { logger } } = req;
    logger.debug(`[Project Controller] Verifying if the user ${user.username} has permissions`);
    const isAuthor = user.id === project.author.id;
    const isOwner = project.owners.find(owner => owner.id === user.id);
    const isMember = project.members.find(members => members.id === user.id);
    if (!isAuthor && !isOwner && !isMember) {
      throw new errors.UnauthorizedError('The user can not get the resource');
    }
  }
}

/**
 * Function to get a query factory presets of a scenarios by id
 * @method
 * @async
 * @memberof Scenario_Controller
 * @param {object} req - Request by User
 * @returns {object} Presets
 */
async function getPresetsScenario (req) {
  const { context: { logger }, params: { scenarioId }, user } = req;
  await getSingleScenario(req);
  logger.debug(`[Scenario Controller] Getting preset for user ${user.id} and scenario ${scenarioId}`);
  return await presetService.getUserPresets(req);
}

/**
 * Function to create a query factory presets of a scenario
 * @method
 * @async
 * @memberof Scenario_Controller
 * @param {object} req - Request by User
 * @returns {object} Presets
 */
async function createPresetsScenario (req) {
  const { context: { logger }, params: { scenarioId }, user } = req;
  await getSingleScenario(req);
  logger.debug(`[Scenario Controller] Creating preset for user ${user.id} and scenario ${scenarioId}`);
  return await presetService.createPreset(req);
}

/**
 * Function to delete a query factory presets of a scenarios by id
 * @method
 * @async
 * @memberof Scenario_Controller
 * @param {object} req - Request by User
 * @returns {object} Presets
 */
async function deletePresetsScenario (req) {
  const { context: { logger }, params: { presetId } } = req;
  await getSingleScenario(req);
  logger.debug(`[Scenario Controller] Deleting preset ${presetId}`);
  return await presetService.deletePreset(req);
}

/**
 * Function to update a query factory presets of a scenarios by id
 * @method
 * @async
 * @memberof Scenario_Controller
 * @param {object} req - Request by User
 * @returns {object} Presets
 */
async function updatePresetsScenario (req) {
  const { context: { logger }, params: { presetId } } = req;
  await getSingleScenario(req);
  logger.debug(`[Scenario Controller] Updating preset ${presetId}`);
  return await presetService.updatePreset(req);
}

module.exports = {
  listAllScenarios,
  getSingleScenario,
  getScenariosByProject,
  runScenario,
  getConfiguration,
  getOrCreateConfig,
  getPresetsScenario,
  createPresetsScenario,
  deletePresetsScenario,
  updatePresetsScenario
};

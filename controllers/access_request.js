/**
 * Access Request Controller
 * @namespace AccessRequest_Controller
 */

const ARDatasetsService = require('../services/db/ar_dataset');
/**
 * Function to request list of datasets
 * @method
 * @async
 * @memberof AccessRequest_Controller
 * @param {object} req - Request by User
 * @returns {array} List of datasets
 */
async function listAllDatasets (req) {
  return await ARDatasetsService.getAllDatasets(req);
}

/**
 * Function to get access request data
 * @method
 * @async
 * @memberof AccessRequest_Controller
 * @param {object} req - Request by User
 * @returns {object} Access request data
 */
async function getAccessRequestData (req) {
  const { context: { logger } } = req;
  logger.debug('[Access Request Controller] Getting access request data');
  return await ARDatasetsService.getAccessRequestData(req);
}

/**
 * Function to get access request evaluation data
 * @method
 * @async
 * @memberof AccessRequest_Controller
 * @param {object} req - Request by User
 * @returns {Array} Access request data
 */
async function getEvaluationData (req) {
  return await ARDatasetsService.getEvaluationData(req);
}

/**
 * Function to get access request data
 * @method
 * @async
 * @memberof AccessRequest_Controller
 * @param {object} req - Request by User
 * @returns {object} Access request data
 */
async function getAllAccessRequests (req) {
  return await ARDatasetsService.getAllAccessRequests(req);
}

/**
 * Function to get access request data
 * @method
 * @async
 * @memberof AccessRequest_Controller
 * @param {object} req - Request by User
 * @returns {object} Access request data
 */
async function getMyAccessRequests (req) {
  return await ARDatasetsService.getMyAccessRequests(req);
}

/**
 * Function to get access requests that are pending evaluation from user
 * @method
 * @async
 * @memberof AccessRequest_Controller
 * @param {object} req - Request by User
 * @returns {array} Access request data
 */
async function listEvaluations (req) {
  return await ARDatasetsService.listEvaluations(req);
}

/**
 * Function to request list of datasets
 * @method
 * @async
 * @memberof AccessRequest_Controller
 * @param {object} req - Request by User
 * @returns {array} List of datasets
 */
async function getDataset (req) {
  return await ARDatasetsService.getDataset(req);
}

/**
 * Function to request list of datasets
 * @method
 * @async
 * @memberof AccessRequest_Controller
 * @param {object} req - Request by User
 * @returns {object} Newly created access request
 */
async function createAccessRequest (req) {
  return await ARDatasetsService.createAccessRequest(req);
}

/**
 * Function to upload the cv for an access request
 * @method
 * @async
 * @memberof AccessRequest_Controller
 * @param {object} req - Request by User
 * @returns {object} Newly created access request
 */
async function uploadCv (req) {
  return await ARDatasetsService.uploadCv(req);
}

/**
 * Function to upload the cv for an access request
 * @method
 * @async
 * @memberof AccessRequest_Controller
 * @param {object} req - Request by User
 * @returns {object} Newly created access request
 */
async function downloadCv (req) {
  return await ARDatasetsService.downloadCv(req);
}

/**
 * Function to Submit Access Request Evaluation
 * @method
 * @async
 * @memberof AccessRequest_Controller
 * @param {object} req - Request by User
 * @returns {object} Newly created access request
 */
async function submitEvaluation (req) {
  if (req.body.type === 'sam') {
    return await ARDatasetsService.submitSAMEvaluation(req);
  } else {
    return await ARDatasetsService.submitEvaluation(req);
  }
}

/**
 * Function to Submit Access Request Evaluation
 * @method
 * @async
 * @memberof AccessRequest_Controller
 * @param {object} req - Request by User
 * @returns {object} the updated access request
 */
async function submitRevision (req) {
  return await ARDatasetsService.submitRevision(req);
}

/**
 * Function to Cancel Access Request
 * @method
 * @async
 * @memberof AccessRequest_Controller
 * @returns {object} the updated access request
 */
async function cancelAccessRequest (req) {
  return await ARDatasetsService.cancelAccessRequest(req);
}

module.exports = {
  listAllDatasets,
  getDataset,
  getAccessRequestData,
  getAllAccessRequests,
  createAccessRequest,
  listEvaluations,
  getMyAccessRequests,
  submitEvaluation,
  getEvaluationData,
  uploadCv,
  downloadCv,
  submitRevision,
  cancelAccessRequest
};

/**
 * Notification Controller
 * @namespace Notification_Controller
 */

const notificationService = require('../services/db/notification');
const errors = require('../utils/errors');

/**
 * Function to request list of notification
 * @method
 * @async
 * @memberof Notification_Controller
 * @param {object} req - Request by User
 * @returns {array} List of notifications
 */
async function listAllUserNotifications (req) {
  const { user, context: { logger } } = req;
  logger.debug(`[Notification Controller] Getting all notifications for user ${user.username}`);
  const notifications = await notificationService.getUserNotifications(req);
  return notifications;
}

/**
 * Function to get a notification from service
 * @method
 * @async
 * @memberof Notification_Controller
 * @param {object} req - Request by User
 * @returns {object} Notification founded
 */
async function getNotificationFromService (req) {
  const { params: { notificationId }, context: { logger } } = req;
  logger.debug(`[Notification Controller] Getting notification ${notificationId}`);
  const notification = await notificationService.getNotification(req);
  if (!notification) {
    logger.error(`[Notification Controller] Error: There is no a notification created with id ${notificationId}`);
    throw new errors.NotFoundError('There is no a notification created');
  }
  return notification;
}

/**
 * Function to mark as seen all notifications
 * @method
 * @async
 * @memberof Notification_Controller
 * @param {object} req - Request by User
 * @returns {array} List of notifications
 */
async function notificationSeen (req) {
  const { user, context: { logger } } = req;
  logger.debug(`[Notification Controller] Mark as seen all notifications of the user ${user.id}`);
  const notifications = await notificationService.markAsSeen(req);
  if (!notifications) {
    logger.error(`[Notification Controller] Error changing the status as seen of the user ${user.id}`);
    throw new errors.NotFoundError('Error changing the status as seen');
  }
  return notifications;
}

/**
 * Function to delete notification
 * @method
 * @async
 * @memberof Notification_Controller
 * @param {object} req - Request by User
 * @returns {object} Empty object
 */
async function deleteNotification (req) {
  const { params: { notificationId }, context: { logger } } = req;

  const notification = await getNotificationFromService(req);
  await verifyAuthorization(req, notification);

  logger.debug(`[Notification Controller] Deleting notification ${notificationId}`);
  try {
    await notificationService.deleteNotification(req);
    return {};
  } catch (error) {
    logger.error(`Error deleting notification ${notificationId}`, error);
    throw new errors.RuntimeError('Error deleting notification');
  }
}

/**
 * Function to create a notification
 * @method
 * @async
 * @memberof Notification_Controller
 * @param {object} req - Request by User
 * @returns {object} - Notification created
 */
async function createNotification (req) {
  const { body: { title }, context: { logger } } = req;

  if (!title) {
    logger.error('[Notification Controller] Error: Title is null');
    throw new errors.ArgumentNullError('Title is required');
  }

  logger.debug(`[Notification Controller] Creating a notification with title ${title}`);

  const creationNotification = await notificationService.createNotification(req);
  if (!creationNotification) {
    logger.error(`[Notification Controller] Error creating a notification with title ${title}`);
    throw new errors.RuntimeError('Error creating a Notification');
  }
  return creationNotification;
}

async function verifyAuthorization (req, notification) {
  if (!req.isService) {
    const { user, context: { logger } } = req;
    logger.debug(`[Notification Controller] Verifying if the user ${user.username} has permissions`);
    return notification.user_id === user.id;
  }
}

module.exports = {
  listAllUserNotifications,
  deleteNotification,
  createNotification,
  notificationSeen
};

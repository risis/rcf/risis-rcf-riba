/**
 * Project Controller
 * @namespace Project_Controller
 */

/**
 * Add list of keywords if dataset had.
 * @method
 * @memberof Dataset_Controller
 * @param {object} object - response with data to user
 */

const fs = require('fs-extra');
const extract = require('extract-zip');
const util = require('util');
const path = require('path');
const appRoot = require('app-root-path').toString();
const log4js = require('log4js');
const crypto = require('crypto');
const stream = require('stream');
const walkdir = require('walkdir');
const mime = require('mime-types');

const randomBytes = util.promisify(crypto.randomBytes);
const pipeline = util.promisify(stream.pipeline);

const settings = global.config.settings;

const projectService = require('../services/db/project');
const datasetService = require('../services/db/dataset');
const scenarioService = require('../services/db/scenario');
const jobService = require('../services/db/job');
const scenarioController = require('./scenario');
const datasetController = require('./dataset');
const jobController = require('./job');
const risorService = require('../services/risor');
const minioService = require('../services/minio');
const errors = require('../utils/errors');
const objectPath = require('object-path');

const tmpRootPath = path.join(appRoot, settings.files.tmpDir);
const defaultLogger = log4js.getLogger('default');

// ensure and empty folder for downloads
function init () {
  try {
    fs.ensureDirSync(tmpRootPath);
    defaultLogger.info('tmp path ' + tmpRootPath + ' exists');
  } catch (e) {
    throw new errors.RuntimeError('Error ensuring tmp path ' + tmpRootPath, e);
  }
  try {
    fs.emptyDirSync(tmpRootPath);
    defaultLogger.info('tmp path ' + tmpRootPath + ' emptied');
  } catch (e) {
    throw new errors.RuntimeError('Error emptying tmp path ' + tmpRootPath, e);
  }
}

function flattenKeywords (object) {
  if (object && object.keywords && object.keywords.length > 0 && object.keywords[0].value) {
    object.keywords = object.keywords.map(keyword => keyword.value);
  };
}

/**
 * Function to request list of all users projects
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {array} List of projects
 */
async function listAllUserProjects (req) {
  const { user, context: { logger } } = req;
  logger.debug(`[Project Controller] Getting all projects for user ${user.username}`);
  const projects = await projectService.getUserProjects(req);
  return projects;
}

/**
 * Function to request a project by id
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {object} Project found
 */
async function getSingleProject (req) {
  const { params: { projectId }, query: { view }, context: { logger } } = req;
  logger.debug(`[Project Controller] Getting project ${projectId}`);
  logger.debug(`[Project Controller] View ${view}`);
  const project = await projectService.getProject(req, view);
  if (!project) {
    logger.error(`[Project Controller] Project ${projectId} does not exist`);
    throw new errors.NotFoundError('The project does not exist');
  }
  verifyAuthorization(req, project);
  if (project.delete_datetime) {
    logger.error(`[Job Controller] The project ${project.id} is already eliminated`);
    throw new errors.NotFoundError('The project is eliminated');
  }
  req.project = project;
  if (project.jobs) {
    const jobs = [];
    for (let i = 0; i < req.project.jobs.length; i++) {
      const job = req.project.jobs[i];
      req.job = job;
      job.files = await jobController.getFilesStorage(req);
      restructureErrorResponse(job);
      jobs.push(job);
    }
    project.jobs = jobs;
  }
  return project;
}

/**
 * Function to create a project
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {object} Project created
 */
async function createProject (req) {
  const { body: { title, description }, context: { logger } } = req;

  if (!title || !description) {
    throw new errors.ArgumentNullError('Not enough information to create the project');
  }

  logger.debug('[Project Controller] Creating project');
  const newProject = await projectService.createProject(req);
  req.params.projectId = newProject.id;
  return await getSingleProject(req);
}

/**
 * Function to find a Dataset before importing it in another function.
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {object} Project with full information
 */
async function importDataset (req) {
  const { params: { projectId, datasetId }, context: { logger } } = req;
  const project = await getSingleProject(req);
  verifyAuthorization(req, project);
  await datasetController.getSingleDataset(req);
  logger.debug(`[Project Controller] Importing dataset ${datasetId} into project ${projectId}`);
  const projectData = await datasetService.importDatasetInProject(req);
  flattenKeywords(projectData);
  return projectData;
}

/**
 * Function to unlink a Dataset in a project.
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {object} Project information
 */
async function unlinkDataset (req) {
  const { params: { projectId, datasetId }, context: { logger } } = req;
  logger.info(`Unlink dataset ${datasetId} from project ${projectId}`);

  const project = await getSingleProject(req);
  verifyAuthorization(req, project);
  await datasetController.getSingleDataset(req);

  const datasetImported = project.datasets.find(ids => ids.id === datasetId);
  if (!datasetImported) {
    throw new errors.ArgumentError('The dataset is not linked to the project.');
  }
  return await datasetService.disconnectDatasetProject(req);
}

/**
 * Function to request list of the datasets into a project
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {array} List of datasets
 */
async function getDatasetsByProject (req) {
  const { context: { logger } } = req;
  const project = await getSingleProject(req);
  verifyAuthorization(req, project);
  logger.debug(`[Project Controller] Getting a datasets of a project ${project.id}`);
  const datasets = project.datasets;
  const datasetsWithInfo = [];
  for (let i = 0; i < datasets.length; i++) {
    const dataset = datasets[i];
    req.params.datasetId = dataset.id;
    const datasetInfo = await datasetController.getSingleDataset(req);
    datasetsWithInfo.push(datasetInfo);
  }
  return datasetsWithInfo;
}

/**
 * Function to get a list of jobs depending of the project
 * @method
 * @async
 * @memberof Job_Controller
 * @param {object} req - Request by User
 * @returns {array} List of jobs
 */
async function getJobsByProject (req) {
  const { params: { projectId }, context: { logger } } = req;
  logger.debug(`[Job Controller] Getting Jobs by project ${projectId}`);
  const project = await getSingleProject(req);
  verifyAuthorization(req, project);
  return project.jobs;
}

/**
 * Function to transform the error json to the next structure
 * {
 *    title: string
 *    message: string
 * }
 * @method
 * @memberof Job_Controller
 * @param {object} job - Job object
 * @returns {object} Job object
 */
function restructureErrorResponse (job) {
  if (job.response) {
    const error = job.response.error;
    if (error && Object.prototype.hasOwnProperty.call(error, 'includes')) {
      job.response = {
        error: {
          title: ''
        }
      };
      if (error.includes('500')) {
        job.response.error.title = 'Internal Server Error';
        job.response.error.message = 'Try running the scenario again or feel free to contact us if the problem persist.';
      } else if (error.includes('-')) {
        job.response.error.message = error.split('-')[1].trim() || '';
      } else {
        job.response.error.message = '';
      }
    }
  }
}

/**
 * Function to unlink a scenario into a project
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {array} project information
 */
async function unlinkScenario (req) {
  const { params: { projectId, scenarioId }, context: { logger } } = req;
  logger.info(`Unlink scenario ${scenarioId} from project ${projectId}`);

  const project = await getSingleProject(req);
  verifyAuthorization(req, project);
  await scenarioController.getSingleScenario(req);

  // Is it the main scenario?
  const mainScenario = project.main_scenario;
  let disconnectedScenario;
  if (mainScenario && mainScenario.id === scenarioId) {
    logger.debug('[Project Controller] Linked as main scenario');
    req.params.scenarioId = mainScenario.id;
    disconnectedScenario = await scenarioService.disconnectMainScenarioProject(req);
  }

  // Is it an ordinary one?
  if (project.scenarios) {
    const coincidingScenario = project.scenarios.find(sc => sc.id === scenarioId);
    if (coincidingScenario) {
      logger.debug('[Project Controller] Linked as ordinary scenario');
      req.params.scenarioId = coincidingScenario.id;
      disconnectedScenario = await scenarioService.disconnectScenarioProject(req);
    }
  }

  if (!disconnectedScenario) {
    logger.error('[Project Controller] Scenario not linked to project');
    throw new errors.NotFoundError('Scenario not linked to project');
  }

  return disconnectedScenario;
}

/**
 * Function to get a list of scenarios of the user in a project selected
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {array} List of scenarios found
 */
async function getScenariosByProject (req) {
  const { context: { logger } } = req;
  const project = await getSingleProject(req);
  verifyAuthorization(req, project);
  req.project = project;
  logger.debug(`[Project Controller] Requesting to Scenario controllers a list of scenarios of this project (${req.project.id})`);
  return await scenarioController.getScenariosByProject(req);
}

/**
 * Function to import a scenario into a project
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {array} project information
 */
async function importScenario (req) {
  const { user, context: { logger } } = req;

  // For further use
  let project = null;
  [req.scenario, project] = await Promise.all([
    scenarioController.getSingleScenario(req),
    getSingleProject(req)
  ]);

  verifyAuthorization(req, project);
  req.project = project;

  if (!req.scenario || !req.project) {
    throw new errors.NotFoundError();
  }

  logger.info(`import scenario ${req.scenario.id} into project ${req.project.id}`);

  const { main_scenario: mainScenario, scenarios } = req.project;
  if (
    (mainScenario && mainScenario.id === req.scenario.id) ||
    scenarios.filter(sc => sc.id === req.scenario.id).length > 0
  ) {
    logger.info('already imported');
    return req.project;
  }

  // A random suffix for the temporary work folder
  const randomName = (await randomBytes(16)).toString('hex');

  // Temporary work folder
  let tmpScenarioPath = path.join(tmpRootPath, `${req.scenario.id}${randomName}`);
  try {
    await fs.ensureDir(tmpScenarioPath);
  } catch (e) {
    const msg = 'Error with scenario tmp folder ' + tmpScenarioPath;
    logger.error(msg);
    throw new errors.RuntimeError(msg, e);
  }

  try {
    // Download scenario to tmp folder
    const zipFilePath = path.join(tmpScenarioPath, `${req.scenario.id}.zip`);
    await pipeline(
      risorService.downloadScenarioBundle(req),
      fs.createWriteStream(zipFilePath)
    );

    // Extract files
    await extract(zipFilePath, { dir: tmpScenarioPath });
    const nestedPath = path.join(tmpScenarioPath, req.scenario.id);
    if (
      await fs.pathExists(nestedPath) &&
      (await fs.lstat(nestedPath)).isDirectory()
    ) {
      tmpScenarioPath = nestedPath;
    }

    // Delete zip
    await fs.unlink(zipFilePath);
  } catch (e) {
    const msg = 'Error getting scenario bundle';
    logger.error(msg);
    throw new errors.RuntimeError(msg, e);
  }

  // Scenario base path
  const minioBasePath = `${user.id}/projects/${req.project.id}/scenarios/${req.scenario.id}/model/`;
  logger.debug(`[Project Controller] Minio folder: ${minioBasePath}`);

  // Get all file paths
  let files = await walkdir.async(tmpScenarioPath);
  logger.debug('[Project Controller] Directory walk');
  logger.debug(files);

  const stats = await Promise.all(
    files.map((path) => {
      return fs.lstat(path);
    })
  );

  // pair up sub-scenario path in minio and local path for each file. filter out directories in same loop
  files = files.reduce((filtered, currentPath, index) => {
    if (!stats[index].isDirectory()) {
      const minioPath = path.join(minioBasePath, path.relative(tmpScenarioPath, currentPath));
      filtered.push({
        minioPath,
        localPath: currentPath,
        mimetype: mime.lookup(currentPath) || 'application/octet-stream'
      });
    }
    return filtered;
  }, []);
  logger.debug('[Project Controller] Filtered files');
  logger.debug(files);
  logger.info(`got ${files.length} scenario files`);

  // upload to storage
  try {
    await Promise.all(
      files.map((file) => {
        return minioService.putFile(
          file.minioPath,
          file.localPath,
          { 'Content-Type': file.mimetype },
          req
        );
      })
    );
    logger.info('all uploads succeeded');
  } catch (e) {
    logger.error('error uploading files');
    // Try to do some cleaning
    await minioService.clearPath(minioBasePath, req);
    // Still have to return something on response
    throw e;
  }

  // Clear folder
  logger.info(`Removing tmp work folder: ${tmpRootPath}/${req.scenario.id}${randomName}`);
  await fs.remove(`${tmpRootPath}/${req.scenario.id}${randomName}`);

  // Persist to db
  logger.info('saving to db');
  let projectInfo;
  try {
    projectInfo = await scenarioService.importScenarioInProject(req);
    await scenarioController.getOrCreateConfig(req);
  } catch (e) {
    const msg = 'problem persisting scenario info to db';
    logger.error(msg);
    throw new errors.RuntimeError(msg, e);
  }

  flattenKeywords(projectInfo);

  logger.info('Project', projectInfo);

  return projectInfo;
}

/**
 * Function to create an object with the data to update.
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {object} Project updated
 */
async function patchProject (req) {
  const { params: { projectId }, body: projectData, context: { logger } } = req;
  logger.debug(`[Project Controller] Getting project ${projectId}`);

  const project = await getSingleProject(req);
  verifyAuthorization(req, project);

  if (projectData.main_scenario) {
    req.params.scenarioId = projectData.main_scenario;
    await scenarioController.getSingleScenario(req);
  }

  logger.debug(`[Project Controller] Updating Project ${projectId}`);
  return await projectService.patchProject(req);
}

/**
 * Function to delete a project.
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {object} Empty object
 */
async function deleteProject (req) {
  const { params: { projectId }, context: { logger } } = req;
  logger.debug(`[Project Controller] Getting project ${projectId}`);

  const project = await getSingleProject(req);
  verifyAuthorization(req, project);

  if (project.state.slug === 'running') {
    logger.error(`[Project Controller] The project ${projectId} is running, so it's impossible eliminate`);
    throw new errors.ArgumentError('The project is running');
  }

  if (project.jobs.length > 0) {
    for (let i = 0; i < project.jobs.length; i++) {
      const job = project.jobs[i];
      req.params.jobId = job.id;
      await jobController.deleteJob(req);
    }
  }

  logger.debug(`[Project Controller] Eliminating Project ${projectId}`);
  return await projectService.deleteProject(req);
}

/**
 * Function to find projects by scenario id
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {array} List of projects
 */
async function getProjectsByScenario (req) {
  const { user, params: { scenarioId }, context: { logger } } = req;
  logger.debug(`[Project Controller] Getting projects by scenario ${scenarioId}`);
  const projects = await projectService.getProjectsByScenario(req);
  return projects.filter(project => project.author.id === user.id || project.members.find(m => m.id === user.id) || project.owners.find(o => o.id === user.id));
}

/**
 * Function to get a scenario instance from a project
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {object} Scenario instance
 */
async function getScenarioFromProject (req) {
  const { context: { logger } } = req;
  const project = await getSingleProject(req);
  verifyAuthorization(req, project);
  const scenario = await scenarioController.getSingleScenario(req);
  logger.debug(`[Project Controller] Getting configuration for project ${project.id} and scenario ${scenario.id}`);
  req.project = project;
  req.scenario = scenario;
  scenario.config = await scenarioController.getConfiguration(req);
  scenario.state = req.job.state.slug;
  return scenario;
}

/**
 * Function to get a configuration last job
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {object} Scenario configuration
 */
async function getConfigScenario (req) {
  const { context: { logger } } = req;
  const project = await getSingleProject(req);
  verifyAuthorization(req, project);
  const scenario = await scenarioController.getSingleScenario(req);
  logger.debug(`[Project Controller] Getting configuration for project ${project.id} and scenario ${scenario.id}`);
  req.project = project;
  req.scenario = scenario;
  logger.debug('[Project Controller] job state', req.job.state);
  const config = await scenarioController.getConfiguration(req);
  if (req.job.state.slug === 'not-ready') {
    await jobService.updateJob(
      { params: { jobId: req.job.id }, context: req.context },
      { state: { connect: { slug: 'ready' } } }
    );
  };
  return config;
}

/**
 * Function to set a configuration of a scenarios by id
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {object} Scenario configuration
 */
async function updateConfigScenario (req) {
  const { context: { logger }, user, body } = req;
  const project = await getSingleProject(req);
  verifyAuthorization(req, project);
  const scenario = await scenarioController.getSingleScenario(req);
  const jobsScenario = project.jobs.filter(job => job.scenario.id === scenario.id);
  const job = jobsScenario.sort((d1, d2) => { return new Date(d2.creation_datetime) - new Date(d1.creation_datetime); })[0];
  if (!job || jobsScenario.find(job => job.state.slug === 'running')) {
    throw new errors.ArgumentError('No jobs to configure');
  }
  req.project = project;
  req.scenario = scenario;
  req.job = job;
  const jobConfiguration = await jobController.getConfigurationJob(req);
  logger.debug(`[Project Controller] Updating configuration for job ${job.id}`);
  const data = unwrap(body);
  Object.entries(data).forEach(([key, value]) => {
    const objKey = key.split('.')[0];
    objectPath.set(jobConfiguration, objKey, body[objKey]);
  });
  const path = `${user.id}/projects/${project.id}/scenarios/${scenario.id}/${job.id}`;
  await jobController.createParamsFileMinio(req, path, jobConfiguration);
  return jobConfiguration;
}

/**
 * Function to change the title of the last job scenario
 * @method
 * @async
 * @memberof Project_Controller
 * @param {object} req - Request by User
 * @returns {object} job configuration
 */
async function changeTitleLastScenarioJob (req) {
  const { context: { logger }, body } = req;
  const project = await getSingleProject(req);
  verifyAuthorization(req, project);
  const scenario = await scenarioController.getSingleScenario(req);
  const jobsScenario = project.jobs.filter(job => job.scenario.id === scenario.id);
  const job = jobsScenario.sort((d1, d2) => { return new Date(d2.creation_datetime) - new Date(d1.creation_datetime); })[0];
  if (!job || jobsScenario.find(job => job.state.slug === 'running')) {
    throw new errors.ArgumentError('No jobs to configure');
  }
  logger.debug(`[Project Controller] Change title for job ${job.id}`);
  return await jobService.updateJob(
    { params: { jobId: job.id }, context: req.context },
    { title: body.title }
  );
}

function unwrap (obj, prefix) {
  const res = {};
  for (const k of Object.keys(obj)) {
    const val = obj[k];
    const key = prefix ? prefix + '.' + k : k;
    if (typeof val === 'object') { Object.assign(res, unwrap(val, key)); } else { res[key] = val; }
  }
  return res;
}

async function downloadOutputs (req) {
  const { user, context: { logger } } = req;
  const project = await getSingleProject(req);
  verifyAuthorization(req, project);
  req.project = project;
  const job = await jobController.getSingleJob(req);
  const minioPath = `${user.id}/projects/${project.id}/scenarios/${job.scenario.id}/${job.id}/outputs`;
  try {
    return await minioService.getPathAsZip(req, minioPath, 'outputs');
  } catch (error) {
    logger.error(`Error downloading output in ${minioPath}.`);
    logger.error(error);
    throw new errors.RuntimeError('Error downloading outputs');
  }
}

async function processScenarioAction (req) {
  const { params: { scenarioId }, context: { logger } } = req;

  const project = await getSingleProject(req);
  verifyAuthorization(req, project);

  if (req.body.command === 'STOP') {
    const runningScenarioJob = project.jobs.find(job => job.state.id === 4 && job.scenario.id === scenarioId);
    if (!runningScenarioJob) {
      throw new errors.ArgumentError('scenario');
    }
    const fullRunningJob = await jobService.getJob({ ...req, params: { jobId: runningScenarioJob.id } }, 'full');
    logger.info(`job runing ${runningScenarioJob} engine id ${fullRunningJob.engine_id}`);
    req.job = fullRunningJob;
    const risorResponse = await risorService.stopScenarioInstance(req);
    return { ...risorResponse, ...req.job };
  } else {
    throw new errors.ArgumentError('command');
  }
}

function verifyAuthorization (req, project) {
  if (!req.isService) {
    const { user, context: { logger } } = req;
    logger.debug(`[Project Controller] Verifying if the user ${user.username} has permissions`);
    const isAuthor = user.id === project.author.id;
    if (isAuthor) return;
    const isOwner = project.owners.find(owner => owner.id === user.id);
    if (isOwner) return;
    const isMember = project.members.find(members => members.id === user.id);
    if (isMember) return;
    throw new errors.UnauthorizedError('The user can not get the resource');
  }
}

init();

module.exports = {
  listAllUserProjects,
  getProjectsByScenario,
  importDataset,
  getSingleProject,
  createProject,
  patchProject,
  getDatasetsByProject,
  importScenario,
  processScenarioAction,
  unlinkScenario,
  unlinkDataset,
  getScenariosByProject,
  getScenarioFromProject,
  getJobsByProject,
  getConfigScenario,
  updateConfigScenario,
  deleteProject,
  downloadOutputs,
  changeTitleLastScenarioJob
};

# RISIS Backend

## Running locally using docker

Clone the repository:

```
git clone git@gitlab.com:risis/rcf/risis-rcf-riba.git
```

Checkout the dev branch

```
git checkout dev
```

Create `data` and `log` directories and change permissions to write data

```
mkdir data log && sudo chmod 777 -R data log prisma
```

Create a `.env` file with the environment variables to prima.

```
cp prisma/.env.dist prisma/.env
```

**Note:** Is important to change the information about url connection.

Create a `.env` file with the environment variables [docker-compose.yml](../docker-compose.yml) grabs from it. See [.env.dist](../.env.dist).

```
cp .env.dist .env
```

**Note:** You need add the NPM_TOKEN in .env file

Configuration values will try to be loaded from a file in `./config`. The file used will depend on the value of the `NODE_ENV` environment variable. i.e. if `NODE_ENV=development`, `config/development.yaml` will try to be loaded.

Values in `config/default.yaml` are there for reference only. Configuration values are environment-depent.

Create a config file for your environment.

```
cp config/default.yaml config/development.yaml
```

You need to change some information before tu run RIBA, that changes depends of the environment. 

Values to change depending of the environmet:

### **Development**
```
1. HOST_URI_RISOR: YOUR IP ADDRESS
2. PORT_URI_RISOR: 7777
3. HOST_URI_RIBA: http://localhost
4. PORT_URI_RIBA: 7778
5. HOST_URI_STORAGE: m4
6. PORT_URI_STORAGE: 9000
7. ACCESS_KEY_STORAGE: ACCESS_KEY
8. SECRET_KEY_STORAGE: SECRET_KEY
9. HOST_URI_DATASTORE: http://datastore.risis.io
10. API_TOKEN_DATASTORE: API_TOKEN
```
### **Production**
```
1. HOST_URI_RISOR: https://risor.risis.io
2. PORT_URI_RISOR: 80
3. HOST_URI_RIBA: https://riba.risis.io
4. PORT_URI_RIBA: 80
5. HOST_URI_STORAGE: m4
6. PORT_URI_STORAGE: 9000
7. ACCESS_KEY_STORAGE: ACCESS_KEY
8. SECRET_KEY_STORAGE: SECRET_KEY
9. HOST_URI_DATASTORE: http://datastore.risis.io
10. API_TOKEN_DATASTORE: API_TOKEN
```

RIBA is ready to run for development or production. Use the next commands to run.

```
1. docker network create frontend && docker network create backend
2. npm run data:clear
3. npm run run:project

In other terminal run the next command after run project.
4. npm run data:migration
```

## Folder structure

```
\root
  \api         - routing and common middleware
  \config      - config files
  \controllers - build responses from services
  \docs
  \fixtures    - stub data (for prototyping and maybe testing)
  \openapi     - api documentation
  \prisma      - db schema (for prototyping)
  \services    - external service interfaces (db, minio...)
  \utils       - common utilities (custom error classes...)
```

# During development

Source files are mounted as a volume in the `riba` container. The `live` command (ran by default in docker-compose.dev.yml) starts the server with [nodemon](https://nodemon.io/). This enables live reloading of changes in code.

## Debugging

The `live` command runs with `--inspect=0.0.0.0:9228`, enabling a debugger to attach from outside the container. If you use VS Code, there's a [debug configuration](../.vscode/launch.json) in place to attach to the 9228 port.

## Adding dependencies

If you install a new dependency you will have to rebuild the `riba` image

```
docker-compose -f docker-compose.yml -f docker-compose.dev.yml down
docker-compose -f docker-compose.yml -f docker-compose.dev.yml build
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up
```

You may want to build ignoring the cached docker layers:

```
docker-compose -f docker-compose.yml -f docker-compose.dev.yml build --no-cache
```

You may want to force creating a new container instead of using one in place:

```
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --force-recreate
```

## Managing the prototype db

To launch [prisma studio](https://github.com/prisma/studio)

```
docker exec riba npx prisma studio
```

Port 7775 is mapped to prisma studio running inside the container.

## Prisma migrations

To create customized SQL migrations:
```sh
docker-compose exec rcf-riba npx prisma migrate dev --create-only --name <NAME>
```

Edit the SQL file created and apply the migration:
```sh
docker-compose exec rcf-riba npx prisma migrate dev
```

## Testing

Test suites are run with `mocha`.

A different configuration for testing might be needed (e.g. to have logging write to a different file...), in that case, override the environment variables prepending them to the command and create a `config/test.yml` file.

### API testing

API test files are under `test/api`.

When testing the API endpoints, the server needs to be running. It will be launched at the start of the API test suite. Make sure the port it is set to listen to (`RIBA_PORT` environment variable) is available. Run:

Without environment variables:
```
./node_modules/.bin/mocha --exit --recursive test/api
```

With environment variables:
```
RIBA_PORT=7779 NODE_ENV=test ./node_modules/.bin/mocha --exit --recursive test/api
```

### Unit testing

Unit test files are under `test/unit`.

Without environment variables:
```
./node_modules/.bin/mocha --exit --recursive test/unit
```

With environment variables:
```
RIBA_PORT=7779 NODE_ENV=test ./node_modules/.bin/mocha --exit --recursive test/unit
```

### Debugging tests

## Scripts

There's a bunch of convenience scripts in `package.json`:

Usual `node index.js`
```json
"start": "node index.js"
```

Live reload + debug launch with nodemon
```json
"live": "nodemon --inspect=0.0.0.0:9228 index.js"
```

Stop docker containers
```json
"docker:down": "docker-compose -f docker-compose.yml -f docker-compose.${BUILD_ENV-dev}.yml down"
```

Docker build
```json
"docker:build": "docker-compose -f docker-compose.yml -f docker-compose.${BUILD_ENV-dev}.yml build"
```

Docker build with no cache
```json
"docker:build-clean": "docker-compose -f docker-compose.yml -f docker-compose.${BUILD_ENV-dev}.yml build --no-cache"
```

Docker run
```json
"docker:up": "docker-compose -f docker-compose.yml -f docker-compose.${BUILD_ENV-dev}.yml up"
```

Docker run new containers
```json
"docker:up-recreate": "docker-compose -f docker-compose.yml -f docker-compose.${BUILD_ENV-dev}.yml up --force-recreate"
```

Run before npm run test:api
```json
"pretest:api": "npx swagger-cli bundle openapi/openapi.yml -r -t yaml -o openapi/bundle.yml"
```

Runs api test suite (in running docker with name `riba`)
```json
"test:api": "docker exec riba sh -c 'RIBA_PORT=7779 NODE_ENV=test ./node_modules/.bin/mocha --exit --recursive test/api'"
```

Run before npm run test:api-debug
```json
"pretest:api-debug": "npx swagger-cli bundle openapi/openapi.yml -r -t yaml -o openapi/bundle.yml"
```

Runs api test suite but waits for a debugger to attach before starting (in running docker with name `riba`)
```json
"test:api-debug": "docker exec riba sh -c 'RIBA_PORT=7779 NODE_ENV=test ./node_modules/.bin/mocha --exit --recursive test/api --inspect-brk=0.0.0.0:5858'"
```

Runs unit test suite (in running docker with name `riba`)
```json
"test:unit": "docker exec riba sh -c 'RIBA_PORT=7779 NODE_ENV=test ./node_modules/.bin/mocha --exit --recursive test/unit'"
```

Runs unit test suite but waits for a debugger to attach before starting (in running docker with name `riba`)
```json
"test:unit-debug": "docker exec riba sh -c 'RIBA_PORT=7779 NODE_ENV=test ./node_modules/.bin/mocha --exit --recursive test/unit --inspect-brk=0.0.0.0:5858'"
```

Run them as usual with `npm run <script_name>`

/*
 * RCF Service - main
 * @author Philippe Breucker
 * @copyright 12/2019
*/

// deps

const appRoot = require('app-root-path').toString();
const express = require('express');
const helmet = require('helmet');
const log4js = require('log4js');

// Enable normal error middleware use with async route handlers
// If we ever want to drop this, we'll have to wrap every async route handler in a function.
// See https://www.npmjs.com/package/express-async-errors
require('express-async-errors');

// load config

const config = require('config-lite')({
  config_basedir: appRoot,
  config_dir: 'config'
});
global.config = config;

// initialize logger

const { config: log4jsConfig } = require('./utils/logging');

log4js.configure(log4jsConfig);

// Start server

const app = express();
const port = process.env.RIBA_PORT || 7777;

const routes = require('./api');

app.use(helmet());

const logger = log4js.getLogger('default');

if (process.env.NODE_ENV === 'development') {
  logger.info('swagger-stats middleware enabled');
  app.use(require('./middleware/swagger-stats'));
}

routes(app);

const server = app.listen(port, () => {
  logger.info('Server started on port: ' + port);
});

const io = require('socket.io')(server);
app.set('io', io);
io.on('connection', socket => {
  socket.on('join_room', (room) => {
    socket.join(room);
  });
  socket.on('leave_room', (room) => {
    console.log('leaving room', room);
    socket.leave(room);
  });
});

function stop () {
  server.close();
}

/* For testing */
module.exports = server;
module.exports.stop = stop;

const log4js = require('log4js');
const fetch = require('node-fetch');
const logger = log4js.getLogger('default');
const url = require('url');

async function getPublicKey () {
  logger.info('getting keycloak public_key');
  const config = global.config.services.keycloak;
  try {
    if (process.env.NODE_ENV !== 'test') {
      if (!global._keycloakPublicKey) {
        const keycloakUri = url.format(Object.assign(config.uri, { pathname: `/auth/realms/${config.realm}` }));
        logger.info('keycloak public_key not found');
        logger.info('retrieving keycloak public_key at', keycloakUri);
        const res = await fetch(keycloakUri, { method: 'get' });
        const json = await res.json();
        global._keycloakPublicKey = json.public_key;
        logger.info('keycloak public_key =', global._keycloakPublicKey);
      }
      return `-----BEGIN PUBLIC KEY-----\n${global._keycloakPublicKey}\n-----END PUBLIC KEY-----`;
    } else {
      return config.publicKey.replace(/\\n/gm, '\n');
    }
  } catch (err) {
    logger.error('Unexpected error getting keycloak public key');
    logger.error(err.message);
    logger.error(err);
  }
}

module.exports = {
  getPublicKey
};

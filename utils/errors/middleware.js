const http = require('http');
const messageMap = Object.assign({}, http.STATUS_CODES); // see https://nodejs.org/docs/latest/api/http.html#http_http_status_codes

/*
 * Inspired by https://github.com/shutterstock/node-common-errors
 */

const codeMap = {
  ArgumentError: 400,
  ArgumentNullError: 400,
  UnauthorizedError: 403,
  UnauthenticatedError: 401,
  NotFoundError: 404
};

class HttpMappedError extends Error {
  constructor (error) {
    super();
    this.name = this.constructor.name;
    this.statusCode = error.status || codeMap[error.name] || 500;
    this.stack = error.stack;
    this.message = messageMap[this.statusCode];
    this.payload = {
      timestamp: new Date(),
      status: this.statusCode,
      error: messageMap[this.statusCode],
      message: this.statusCode < 500 ? error.message : messageMap[this.statusCode]
    };
  }

  static codeMap () { return codeMap; }
  static messageMap () { return messageMap; }
}

function errorHandler (error, req, res, next) {
  if (!error) {
    if (next) {
      return next();
    }
    return res.end();
  }
  const { context: { logger } } = req;
  const errorToSend = new HttpMappedError(error);
  if (errorToSend.statusCode >= 500) {
    logger.error(error);
  } else {
    logger.info(error.name + ': ' + error.message);
  }
  res.status(errorToSend.statusCode).json(errorToSend.payload);
}

module.exports = {
  errorHandler
};

const util = require('util');

const { logging } = global.config;

const loggingConfig = {
  appenders: {
    stdout: {
      type: 'stdout',
      layout: {
        type: 'pattern',
        pattern: '%d level=%p %x{traceid}%x{message} at=%f{3}.%l%x{context}%n',
        tokens: {
          traceid: ({ context }) => context.traceid ? 'traceid=' + context.traceid + ' ' : '',
          message: ({ data }) => `message='${util.format(data[0])}'`,
          context: ({ data }) => {
            return data.length < 2 || !data[1]
              ? ''
              : ` context=${JSON.stringify(data[1]).replace(/\n/g, ' ')}`;
          }
        }
      }
    },
    stderr: {
      type: 'stderr',
      layout: {
        type: 'coloured'
      }
    },
    errors: {
      type: 'logLevelFilter',
      level: 'ERROR',
      appender: 'stderr'
    }
  },
  categories: {
    default: {
      appenders: [
        'stdout',
        'errors'
      ],
      level: logging.level,
      enableCallStack: true
    }
  }
};

module.exports = {
  config: loggingConfig
};

# risis-rcf-riba

RISIS Backend API (RIBA)

## Releasing a new RIBA version

* Go to main branch, merge dev branch into main
* Make sure the new version is declared in package.json and package-lock.json files
  * See the [Semantic Versioning](https://semver.org) specification on how to increment version numbers
* Make sure all automated tests are ok
* Push main branch to Gitlab
* Create a git tag for the new version and push to Gitlab
* Merge main branch back to dev branch and push
* Create a new Release on Gitlab from the new tag
  * https://gitlab.com/risis/rcf/risis-rcf-riba/-/releases

Gitlab CI/CD will push docker images tagging it with the new git tag pushed to
the repo.

## RIBA development guide

For technical info, visit our [dev docs](./docs/dev.md).

### Monitor API statistics with swagger-stats

RIBA development deployment comes with [swagger-stats](https://swaggerstats.io)
available on the URL below:

* https://riba.risis.localhost/swagger-stats

swagger-stats is an API observability tool to trace API calls and monitor API
performance, health and usage statistics in Node.js microservices.

## License

- Copyright (c) 2019 Philippe Breucker
- Copyright (c) 2020-2023 Université Gustave Eiffel
- Copyright (c) 2020-2023 INRAE

Licensed under the EUPL.

The full text of the EUPL licence can be found at
https://opensource.org/licenses/EUPL-1.2.

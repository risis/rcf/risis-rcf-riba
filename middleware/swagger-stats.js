'use strict';

const path = require('path');
const jsyaml = require('js-yaml');
const fs = require('fs');
const swStats = require('swagger-stats');

const spec = fs.readFileSync(path.join(__dirname, '..', 'openapi', 'bundle.yml'), 'utf8');
const apiSpec = jsyaml.load(spec);

const { name, version } = require(path.join(__dirname, '..', 'package.json'));

module.exports = swStats.getMiddleware({
  name,
  metricsPrefix: 'riba_',
  version,
  swaggerSpec: apiSpec
});

/**
 * Service of Project to save data in database
 * @namespace Project_Service
 */

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const schemas = require('./schemas');
const { nanoid } = require('../nanoid');
const { getFilterParams } = require('./parameters');

const viewSchemaMap = {
  summary: 'projectSummary',
  full: 'project'
};

/**
 * Function to get a project by id
 * @method
 * @async
 * @memberof Project_Service
 * @param {object} req - Request by User
 * @returns {object} project information
 */
async function getProject (req, view = 'full') {
  const { params: { projectId }, context: { logger } } = req;
  logger.debug(`[Project Service] view ${view}`);
  const schemaKey = viewSchemaMap[view];
  logger.debug(`[Project Service] Getting info of project ${projectId} in prisma service`);
  const project = await prisma.project.findUnique({
    where: {
      id: projectId
    },
    select: schemas[schemaKey]
  });
  return project;
}

/**
 * Function to update project by id
 * @method
 * @async
 * @memberof Project_Service
 * @param {object} req - Request by User
 * @param {object} projectData - Data with data to change
 * @returns {object} Project with full information
 */
async function patchProject (req) {
  const { params: { projectId }, body: projectData, context: { logger } } = req;
  logger.debug(`[Project Service] Updating info in project ${projectId} in prisma service`);
  if (projectData.keywords && projectData.keywords.length > 0) {
    projectData.keywords = {
      connectOrCreate: projectData.keywords.map((value) => {
        return {
          where: { value },
          create: { id: nanoid(), value }
        };
      })
    };
  }
  if (projectData.state) {
    projectData.state = {
      connect: {
        slug: projectData.state
      }
    };
  }
  if (projectData.main_scenario) {
    projectData.main_scenario = {
      connect: {
        id: projectData.main_scenario
      }
    };
  }
  if (projectData.job) {
    projectData.jobs = {
      connect: {
        id: projectData.job
      }
    };
  }
  const project = await prisma.project.update({
    where: {
      id: projectId
    },
    data: projectData,
    select: schemas.project
  });
  return project;
}

/**
 * Function to get project where user is author, owner or member.
 * @method
 * @async
 * @memberof Project_Service
 * @param {object} req - Request by User
 * @returns {array} List of project information
 */
async function getUserProjects (req) {
  const { user: { id: userId }, context: { logger } } = req;

  logger.debug(`[DB Service] Getting a projects of the user or member ${userId}`);

  const filterUser = {
    AND: [
      { delete_datetime: null },
      {
        OR: [
          { author_id: userId },
          { owners: { some: { id: userId } } },
          { members: { some: { id: userId } } }
        ]
      }
    ]
  };

  const query = getFilterParams(req);

  query.where = {
    ...query.where,
    ...filterUser
  };

  query.select = schemas.project;

  const projects = await prisma.project.findMany(query);

  return projects;
}

/**
 * Function to create a project
 * @method
 * @async
 * @memberof Project_Service
 * @param {object} req - Request by User
 * @returns {object} Project created
 */

async function createProject (req) {
  const { body: { title, description, keywords }, user, context: { logger } } = req;

  logger.debug('[Project Service] Creating a new project');

  const newProjectData = {
    id: nanoid(),
    title,
    short_description: description,
    author: {
      connect: {
        id: user.id
      }
    },
    state: {
      connect: { slug: 'ready' }
    }
  };
  if (keywords && keywords.length > 0) {
    newProjectData.keywords = {
      connectOrCreate: keywords.map((value) => {
        return {
          where: { value },
          create: { id: nanoid(), value }
        };
      })
    };
  }
  const result = await prisma.project.create(
    {
      data: newProjectData,
      include: {
        keywords: {
          select: {
            value: true
          }
        }
      }
    }
  );
  return result;
}

/**
 * Function to get all projects of one scenario
 * @method
 * @async
 * @memberof Project_Service
 * @param {object} req - Request by User
 * @returns {array} List of projects
 */
async function getProjectsByScenario (req) {
  const { params: { scenarioId }, context: { logger } } = req;
  logger.debug(`[Project Service] Getting a project by scenario ${scenarioId}`);

  const projects = await prisma.scenario.findUnique({
    where: {
      id: scenarioId
    }
  }).projects({
    select: schemas.project,
    where: {
      delete_datetime: null
    }
  });

  return projects;
}

/**
 * Function to eliminate a project
 * @method
 * @async
 * @memberof Project_Service
 * @param {object} req - Request by User
 * @returns {object} Project eliminated
 */
async function deleteProject (req) {
  const { params: { projectId }, context: { logger } } = req;
  logger.debug(`[Project Service] eliminating a project ${projectId}`);

  const result = await prisma.project.update({
    where: {
      id: projectId
    },
    data: {
      delete_datetime: new Date()
    }
  });
  return result;
}

module.exports = {
  getProject,
  getUserProjects,
  getProjectsByScenario,
  createProject,
  patchProject,
  deleteProject
};

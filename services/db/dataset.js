/**
 * Service of Dataset to save data in database
 * @namespace Dataset_Service
 */

const { PrismaClient } = require('@prisma/client');
const schemas = require('./schemas');
const prisma = new PrismaClient();

/**
 * Function to import a Dataset to a project
 * @method
 * @async
 * @memberof Dataset_Service
 * @param {object} req - Request by User
 * @returns {object} Project information
 */
async function importDatasetInProject (req) {
  const { params: { projectId, datasetId }, context: { logger } } = req;
  logger.debug(`[Dataset Service] Importing dataset into a project ${projectId}`);
  const data = {
    connectOrCreate: {
      where: { id: datasetId },
      create: { id: datasetId }
    }
  };
  return await manageConnection(req, data);
}

/**
 * Function to disconnect a dataset into a project
 * @method
 * @async
 * @memberof Dataset_Service
 * @param {object} req - Request by User
 * @returns {object} Project information
 */
async function disconnectDatasetProject (req) {
  const { params: { projectId, datasetId }, context: { logger } } = req;
  logger.debug(`[Dataset Service] Disconnecting dataset from project ${projectId}`);
  const data = {
    disconnect: {
      id: datasetId
    }
  };
  return await manageConnection(req, data);
}

/**
 * Function to manage (connect or disconnect) a dataset into a project
 * @method
 * @async
 * @memberof Dataset_Service
 * @param {object} req - Request by User
 * @param {object} data - Object with information to desconnect or connect.
 * @returns {object} Project information
 */
async function manageConnection (req, data) {
  const { params: { projectId }, context: { logger } } = req;
  logger.debug(`[Dataset Service] Managing dataset into a project ${projectId}`);
  const query = {
    where: {
      id: projectId
    },
    data: {
      datasets: data
    },
    select: schemas.project
  };
  return await prisma.project.update(query);
}

module.exports = {
  importDatasetInProject,
  disconnectDatasetProject
};

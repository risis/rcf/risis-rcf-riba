/**
 * Service of Notification to save data in database
 * @namespace Notification_Service
 */

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const schemas = require('./schemas');
const { nanoid } = require('../nanoid');
const { getFilterParams } = require('./parameters');

/**
 * Function to get notifications by user
 * @method
 * @async
 * @memberof Notification_Service
 * @param {object} req - Request by User
 * @returns {object} list of notifications
 */
async function getUserNotifications (req) {
  const { user: { id: userId }, context: { logger } } = req;
  logger.debug(`[Notification Service] Getting list of notifications by user ${userId}`);

  const filterUser = {
    AND: [
      { user_id: userId },
      { delete_datetime: null }
    ]
  };
  const query = getFilterParams(req);

  query.where = {
    ...query.where,
    ...filterUser
  };

  query.select = schemas.notification;

  const result = await prisma.notification.findMany(query);

  return result;
}

/**
 * Function to get a notification
 * @method
 * @async
 * @memberof Notification_Service
 * @param {object} req - Request by User
 * @returns {object} notification
 */
async function getNotification (req) {
  const { user: { id: userId }, params: { notificationId }, context: { logger } } = req;
  logger.debug(`[Notification Service] Getting a notification ${notificationId} for the user ${userId}`);

  const result = await prisma.notification.findUnique({
    where: {
      id: notificationId
    },
    select: schemas.notification
  });
  return result;
}

/**
 * Function to create a notification
 * @method
 * @async
 * @memberof Notification_Service
 * @param {object} req - Request by User
 * @returns {object} Notification information
 */
async function createNotification (req) {
  const { body: { title, details, url, reason, type }, context: { logger }, user } = req;

  logger.info(`[Notification Service] Creating notification with title ${title} in prisma`);
  const data = {
    id: nanoid(),
    title,
    user: {
      connect: {
        id: user.id
      }
    }
  };
  [{ details }, { url }, { reason }, { type }].forEach(attribute => {
    const key = Object.keys(attribute)[0];
    const value = Object.values(attribute)[0];
    if (value) {
      data[key] = value;
    }
  });
  const result = await prisma.notification.create({
    data
  });
  return result;
}

/**
 * Function to eliminate a notification
 * @method
 * @async
 * @memberof Notification_Service
 * @param {object} req - Request by User
 * @returns {object} Notification eliminated
 */
async function deleteNotification (req) {
  const { params: { notificationId }, context: { logger } } = req;
  logger.debug(`[Notification Service] Eliminating a notification ${notificationId}`);

  const result = await prisma.notification.update({
    where: {
      id: notificationId
    },
    data: {
      delete_datetime: new Date()
    }
  });
  return result;
}

/**
 * Function to mark as seen all notifications
 * @method
 * @async
 * @memberof Notification_Service
 * @param {object} req - Request by User
 * @returns {array} list notification
 */
async function markAsSeen (req) {
  const { user, context: { logger } } = req;
  logger.debug(`[Notification Service] Mark as seen all notifications of the user ${user.id}`);

  const result = await prisma.notification.updateMany({
    where: {
      user: {
        id: user.id
      }
    },
    data: {
      seen: true
    }
  });
  logger.debug(result);
  return result;
}

module.exports = {
  getUserNotifications,
  getNotification,
  createNotification,
  deleteNotification,
  markAsSeen
};

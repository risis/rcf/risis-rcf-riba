/**
 * Service of User to save data in database
 * @namespace User_Service
 */

const { PrismaClient } = require('@prisma/client');
const { nanoid } = require('../nanoid');
const prisma = new PrismaClient();
const schemas = require('./schemas');

/**
 * Function to get an user
 * @method
 * @async
 * @memberof User_Service
 * @param {object} req - Request by User
 * @param {string} idUser - unique id of the user
 * @returns {object} user information
 */
async function getUser (req, idUser, keyToSearch = 'id') {
  const { context: { logger } } = req;
  logger.debug(`[User Service] Getting info of user ${idUser} in prisma service`);
  return await prisma.user.findUnique({
    where: {
      [keyToSearch]: idUser
    },
    select: schemas.users
  });
}

/**
 * Function to create an user
 * @method
 * @async
 * @memberof User_Service
 * @param {object} req - Request by User
 * @returns {object} user information
 */
async function createUser (req) {
  const { user, context: { logger } } = req;
  logger.debug(`[User Service] Creating an user ${user.username} in prisma service`);
  return await prisma.user.create({
    data: user,
    select: schemas.users
  });
}

/**
 * Function to update an user
 * @method
 * @async
 * @memberof User_Service
 * @param {object} req - Request by User
 * @param {string} idUser - unique id of the user
 * @returns {object} user information
 */
async function updateUser (req, idUser) {
  const { user, context: { logger } } = req;
  logger.debug(`[User Service] Updating info of user ${user.username} in prisma service`);
  return await prisma.user.update({
    where: {
      id: idUser
    },
    data: user,
    select: schemas.users
  });
}

/**
 * Function to create service credentials for a user.
 * @method
 * @async
 * @memberof Users_Controller
 * @param {object} req - Request by User
 * @returns {object} User information
 */
async function createService (req) {
  const { user, body: { token }, provider, context: { logger } } = req;
  logger.debug(`[Users Service] Creating a service credentials for the user ${user.username}`);
  const data = {
    id: nanoid(),
    idService: provider.id,
    name: provider.name,
    token,
    user: {
      connect: { id: user.id }
    }
  };
  return await prisma.service_provider_credentials.create({
    data
  });
}

/**
 * Function to update service credentials for a user.
 * @method
 * @async
 * @memberof Users_Controller
 * @param {object} req - Request by User
 * @returns {object} User information
 */
async function updateService (req) {
  const { user, providerId, body: { token }, context: { logger } } = req;
  logger.debug(`[Users Controller] Updating a service credentials for the user ${user.id}`);
  return await prisma.service_provider_credentials.update({
    where: {
      id: providerId
    },
    data: {
      token
    }
  });
}

module.exports = {
  getUser,
  createUser,
  updateUser,
  createService,
  updateService
};

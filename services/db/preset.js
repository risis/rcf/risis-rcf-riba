/**
 * Service of Preset to save data in database
 * @namespace Preset_Service
 */

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const schemas = require('./schemas');
const { nanoid } = require('../nanoid');
const { getFilterParams } = require('./parameters');

/**
 * Function to create a preset
 * @method
 * @async
 * @memberof Preset_Service
 * @param {object} req - Request by User
 * @returns {object} Preset information
 */
async function createPreset (req) {
  const { body: { query, title, description }, params: { scenarioId }, context: { logger }, user } = req;

  logger.info(`[Preset Service] Creating preset with query ${query} in prisma`);
  const data = {
    id: nanoid(),
    title,
    description,
    query,
    user: {
      connect: {
        id: user.id
      }
    },
    scenario: {
      connect: {
        id: scenarioId
      }
    }
  };
  const result = await prisma.presets_query_factory.create({
    data
  });
  return result;
}

/**
 * Function to update a preset
 * @method
 * @async
 * @memberof Preset_Service
 * @param {object} req - Request by User
 * @returns {object} Preset information
 */
async function updatePreset (req) {
  const { body: { query, title, description }, params: { presetId }, context: { logger } } = req;

  logger.info(`[Preset Service] Updating preset ${presetId} with query ${query} in prisma`);
  return await prisma.presets_query_factory.update({
    where: {
      id: presetId
    },
    data: {
      query,
      title,
      description
    }
  });
}

/**
 * Function to get preset by user and scenario
 * @method
 * @async
 * @memberof Preset_Service
 * @param {object} req - Request by User
 * @returns {object} list of presets
 */
async function getUserPresets (req) {
  const { user: { id: userId }, params: { scenarioId }, context: { logger } } = req;
  logger.debug(`[Preset_Service Service] Getting list of presets by user ${userId}`);

  const filterUser = {
    AND: [
      { user_id: userId },
      { scenario_id: scenarioId },
      { delete_datetime: null }
    ]
  };
  const query = getFilterParams(req);

  query.where = {
    ...query.where,
    ...filterUser
  };

  query.select = schemas.preset;

  const result = await prisma.presets_query_factory.findMany(query);

  return result;
}

/**
 * Function to delete a preset by id
 * @method
 * @async
 * @memberof Preset_Service
 * @param {object} req - Request by User
 * @returns {object} Preset information
 */
async function deletePreset (req) {
  const { params: { presetId }, context: { logger } } = req;
  logger.debug(`[Preset Service] Deleting info of preset ${presetId} in prisma service`);
  return await prisma.presets_query_factory.update({
    where: {
      id: presetId
    },
    data: {
      delete_datetime: new Date()
    }
  });
}

module.exports = {
  createPreset,
  getUserPresets,
  updatePreset,
  deletePreset
};

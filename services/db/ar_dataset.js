/**
 * Service of Access Request Datasets
 * @namespace ARDatasets_Service
 */

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const errors = require('../../utils/errors');
const email = require('../accessRequests/email');
const minio = require('../minio.js');

const includeAll = {
  datasets: {
    include: {
      dataset: {
        include: {
          access_managers: true
        }
      }
    }
  },
  user: true
};

/**
 * Function to get a dataset
 * @method
 * @async
 * @memberof ARDatasets_Service
 * @param {object} req - Request by User
 * @returns {object} Dataset Information
 */
async function getDataset (req) {
  const {
    params: { id }
  } = req;
  return await prisma.ar_dataset.findUnique({
    where: {
      id: parseInt(id)
    }
  });
}

/**
 * Function to get an access request
 * @method
 * @async
 * @memberof ARDatasets_Service
 * @param {object} req - Request by User
 * @returns {object} Dataset Information
 */
async function getAccessRequestData (req) {
  const {
    params: { id },
    context: { logger }
  } = req;
  logger.debug('[User Service] Getting info for an access request in prisma');
  const roles = req.tokenDecoded.realm_access.roles;
  const where = { id };
  const allowedRoles = ['Advanced User', 'Local Admin', 'Super Admin', 'Access Request Evaluator', 'Access Request Admin'];

  if (roles.filter(role => allowedRoles.includes(role)).length === 0) {
    where.user_id = req.tokenDecoded.id;
  }

  let ar = await prisma.ar_access_request.findMany({
    where,
    include: includeAll
  });

  ar = accessRequestsWithStatus(ar);

  if (ar.length) {
    return ar[0];
  } else {
    throw new errors.UnauthorizedError('Do not have sufficient permissions to access the resource');
  }
}

/**
 * Function to get an access request evaluation status and data
 * @method
 * @async
 * @memberof ARDatasets_Service
 * @param {object} req - Request by User
 * @returns {object} Dataset Information
 */
async function getEvaluationData (req) {
  const {
    params: { id },
    context: { logger }
  } = req;
  logger.debug('[User Service] Getting info for an access request in prisma');
  const accessRequest = await prisma.ar_access_request.findUnique({
    where: {
      id: parseInt(id)
    },
    include: includeAll
  });

  const step = accessRequest.step;
  const roles = req.tokenDecoded.realm_access.roles;
  const isAccessManager = accessRequest.datasets.filter(d => {
    return (
      d.dataset.access_managers.map(a => a.user_email.toLowerCase()).includes(req.user.email.toLowerCase())
    );
  }).length > 0;

  if (!roles.includes('PC') && !isAccessManager) {
    throw new errors.NotFoundError('This page does not exist');
  }

  const evaluationTypes = ['na', 'am', 'pc', 'finished'];
  let showForm = false;
  let secondaryDatasets = [];
  const hasAnyNegativeDecision = [accessRequest.decision_am, accessRequest.decision_pc].indexOf('Negative') !== -1;
  let message = 'You do not have access to the current evaluation step';
  const mainAccessManagers = accessRequest.datasets.find(dataset => dataset.is_main === 1).dataset.access_managers.map(am => am.user_email.toLowerCase());

  if (step === 1) {
    const isMainAm = mainAccessManagers.includes(req.user.email.toLowerCase());
    if (!accessRequest.decision_am && isMainAm) {
      showForm = true;
    }

    let isSecondaryAm = false;
    accessRequest.datasets.filter(dataset => dataset.is_main === 0).forEach(dataset => {
      if (dataset.dataset.access_managers.map(am => am.user_email.toLowerCase()).includes((req.user.email.toLowerCase()))) {
        isSecondaryAm = true;
      }
    });

    if (accessRequest.decision_am && isSecondaryAm) {
      showForm = true;
      evaluationTypes[1] = 'sam';
      secondaryDatasets = accessRequest.datasets.filter(dataset => {
        return dataset.is_main === 0 &&
          dataset.dataset.access_managers.map(am => am.user_email.toLowerCase()).includes(req.user.email.toLowerCase()) &&
          dataset.decision === null;
      });
    }

    if (accessRequest.should_be_revised === 1) {
      showForm = false;
      message = 'The access request cannot be evaluated at the moment because it is awaiting revision from the applicant.';
    }
  }

  if (step === 2 && roles.indexOf('PC') !== -1) {
    showForm = true;
  }

  if (hasAnyNegativeDecision) {
    showForm = false;
    message = 'This access request has been evaluated negatively by one of the evaluators, therefore the workflow has finished.';
  }

  if (step === 4 && !hasAnyNegativeDecision) {
    showForm = false;
    message = 'The evaluation for this access request has finished, please go to the details page of this access request to see.';
  }

  if (accessRequest.cancelled === 1) {
    showForm = false;
    message = 'This access request has been cancelled by the applicant.';
  }

  if (showForm) {
    message = '';
  }

  return {
    accessRequest,
    showForm,
    evaluationType: evaluationTypes[step],
    secondaryDatasets,
    message
  };
}

/**
 * Function to get all access requests
 * @method
 * @async
 * @memberof ARDatasets_Service
 * @param {object} req - Request by User
 */
async function getAllAccessRequests (req) {
  const { context: { logger } } = req;
  logger.debug('[Access Request Service] Getting access request list');

  const accessRequests = await prisma.ar_access_request.findMany(
    {
      include: {
        datasets: {
          include: {
            dataset: {
              include: {
                access_managers: true
              }
            }
          },
          orderBy: {
            is_main: 'desc'
          }
        },
        user: true
      },
      orderBy: {
        id: 'desc'
      }
    }
  );
  return accessRequestsWithStatus(accessRequests);
}

/**
 * Function to get access requests of the user
 * @method
 * @async
 * @memberof ARDatasets_Service
 * @param {object} req - Request by User
 * @returns {Array} Access request objects
 */
async function getMyAccessRequests (req) {
  const { context: { logger } } = req;
  logger.debug('[Access Request Service] Getting access request list');
  const accessRequests = await prisma.ar_access_request.findMany(
    {
      where: {
        user_id: req.user.id
      },
      include: includeAll,
      orderBy: {
        id: 'desc'
      }
    }
  );
  return accessRequestsWithStatus(accessRequests);
}

/**
 * Function to get a user
 * @method
 * @async
 * @memberof ARDatasets_Service
 * @param {object} req - Request by User
 * @returns {Array} Dataset Information objects
 */
async function listEvaluations (req) {
  const { context: { logger } } = req;
  logger.debug('[Access Request Service] Getting access request list');
  const roles = req.tokenDecoded.realm_access.roles;
  const isEvlauator = roles.includes('PC');
  let where = {
    user_email: req.user.email
  };
  if (isEvlauator) {
    where = {
      dataset_id: {
        gt: 0
      }
    };
  }
  const datasets = await prisma.ar_dataset_access_managers.findMany({
    where
  });
  const accessRequests = await prisma.ar_access_request.findMany(
    {
      where: {
        datasets: {
          some: {
            dataset_id: {
              in: datasets.map(d => d.dataset_id)
            }
          }
        }
      },
      include: includeAll,
      orderBy: {
        id: 'desc'
      }
    }
  );

  return accessRequestsWithStatus(accessRequests);
}

function accessRequestsWithStatus (accessRequests) {
  accessRequests.forEach(element => {
    if (element.cancelled === 1) {
      element.status = 'Cancelled';
      return;
    }

    if (element.should_be_revised === 1) {
      element.status = 'Revision';
      return;
    }

    if (element.step === 3 && element.decision_pc === 'Positive') {
      element.status = 'Positive';
      return;
    }

    if ([element.decision_am, element.decision_pc].indexOf('Negative') !== -1) {
      element.status = 'Negative';
      return;
      // evaluated negatively
    }

    if (element.step < 3) {
      element.status = 'Pending';
    }
  });

  return accessRequests;
}

/**
 * Function to get a user
 * @method
 * @async
 * @memberof ARDatasets_Service
 * @param {object} req - Request by User
 * @returns {Array} Dataset Information objects
 */
async function getAllDatasets (req) {
  return await prisma.ar_dataset.findMany({
    where: {
      show: true
    },
    orderBy: {
      label: 'asc'
    }
  });
}

/**
 * Function to get a user
 * @method
 * @async
 * @memberof ARDatasets_Service
 * @param {object} req - Request by User
 * @returns {Array} Dataset Information objects
 */
async function createAccessRequest (req) {
  const request = req.body;
  const accessRequestData = {
    project_title: request.project_title,
    project_acronym: request.project_acronym,
    project_summary: request.project_summary,
    project_objectives: request.project_objectives,
    data_needed: request.data_needed,
    user_id: req.user.id,
    step: 1,
    is_submitted: 1,
    datasets: null
  };
  const datasets = [];
  req.body.selectedDatasets.forEach(dataset => {
    const append = {
      dataset_id: dataset.id,
      access_type: dataset.access_type ? dataset.access_type : 'distant',
      host_location: dataset.host_location ? dataset.host_location : '',
      preferred_visit_dates: dataset.preferred_visit_dates ? dataset.preferred_visit_dates : '',
      re_imbursement: dataset.re_imbursement ? dataset.re_imbursement : '',
      is_main: dataset.is_main ? 1 : 0
    };
    datasets.push(append);
  });
  accessRequestData.datasets = {
    create: datasets
  };

  const res = await prisma.ar_access_request.create({
    data: accessRequestData,
    include: includeAll
  });

  email.newAccessRequest(res);

  return res;
}

async function uploadCv (req) {
  const {
    params: { id },
    files
  } = req;
  let result = {};
  if (files.length) {
    const minioPath = `${req.user.id}/accessRequests/${id}/cv.pdf`;
    if (!files.length) {
      return {};
    }
    const localPath = files[0].path;
    const uploadedFile = await minio.putFile(minioPath, localPath, {}, req);

    result = await prisma.ar_access_request.update({
      where: {
        id: parseInt(id)
      },
      data: {
        cv_url: uploadedFile.etag
      }
    });
  }
  return result;
}

async function downloadCv (req) {
  const { params: { id } } = req;
  const accessRequest = await prisma.ar_access_request.findUnique({
    where: {
      id: parseInt(id)
    },
    include: {
      user: true
    }
  });
  const path = `${accessRequest.user_id}/accessRequests/${accessRequest.id}/cv.pdf`;
  return minio.getFileByPath(path, accessRequest);
}

async function submitEvaluation (req) {
  const request = req.body;
  request.accessRequest.updated_at = new Date();

  delete request.accessRequest.datasets;
  delete request.accessRequest.user;

  if (request.type === 'am' && request.accessRequest.decision_am === 'Revise') {
    request.accessRequest.should_be_revised = 1;
  }

  const accessRequest = await prisma.ar_access_request.update({
    where: {
      id: request.id
    },
    data: request.accessRequest,
    include: includeAll
  });

  if (request.type === 'am') {
    // TODO: send email to secondary access managers
    const decisionsMappings = {
      Positive: 1,
      Negative: 0,
      Revise: 2
    };

    if (request.accessRequest.decision_am === 'Positive' && accessRequest.datasets.length > 1) {
      email.newAccessRequestSAM(accessRequest);
    }

    if (request.accessRequest.decision_am === 'Revise') {
      email.sendReviseEmail(accessRequest);
    }

    await prisma.ar_access_request_dataset.updateMany({
      where: {
        access_request_id: request.id,
        is_main: 1
      },
      data: {
        decision: decisionsMappings[request.accessRequest.decision_am],
        description: request.accessRequest.reasoning_am,
        decision_date: new Date(),
        am_email: req.user.email
      }
    });
  }

  return updateAccessRequestStep(request.id);
}

async function submitSAMEvaluation (req) {
  const request = req.body;
  for (const arDataset of request.accessRequest) {
    arDataset.decision_date = new Date();
    arDataset.decision = parseInt(arDataset.decision);
    arDataset.am_email = req.user.email;
    delete arDataset.dataset;
    await prisma.ar_access_request_dataset.update({
      where: {
        id: arDataset.id
      },
      data: arDataset
    });
  }
  return updateAccessRequestStep(request.id);
}

async function updateAccessRequestStep (id) {
  const ar = await prisma.ar_access_request.findUnique({
    where: {
      id: id
    },
    include: includeAll
  });

  let newStep;
  if (ar.step === 1 && ar.decision_am === 'Positive' && !ar.datasets.find(dataset => dataset.decision === null || dataset.decision === 2)) {
    await email.newAccessRequestPC(ar);
    newStep = 2;
  }

  if (ar.step === 2 && ar.decision_pc === 'Positive') {
    await email.accessRequestAccepted(ar);
    newStep = 3;
  }

  if ([ar.decision_am, ar.decision_pc].indexOf('Negative') !== -1) {
    // evaluated negatively
    await email.accessRequestDenied(ar);
  }

  if (newStep) {
    await prisma.ar_access_request.update({
      where: {
        id: id
      },
      data: {
        step: newStep
      }
    });
  }

  return { updated: true };
}

async function submitRevision (req) {
  const request = req.body;
  const accessRequestData = {
    project_title: request.project_title,
    project_acronym: request.project_acronym,
    project_summary: request.project_acronym,
    project_objectives: request.project_objectives,
    data_needed: request.data_needed,
    first_question_am: null,
    second_question_am: null,
    third_question_am: null,
    forth_question_am: null,
    reasoning_am: null,
    decision_am: null,
    should_be_revised: 0
  };

  await prisma.ar_access_request.updateMany({
    where: {
      id: request.id,
      user: {
        email: req.user.email
      }
    },
    data: accessRequestData
  });

  for (const dataset of request.datasets) {
    await prisma.ar_access_request_dataset.updateMany({
      where: {
        id: dataset.id,
        access_request: {
          user: {
            email: req.user.email
          }
        }
      },
      data: {
        decision: null,
        description: null,
        access_type: dataset.access_type ? dataset.access_type : 'distant',
        host_location: dataset.host_location ? dataset.host_location : '',
        preferred_visit_dates: dataset.preferred_visit_dates ? dataset.preferred_visit_dates : '',
        re_imbursement: dataset.re_imbursement ? dataset.re_imbursement : ''
      }
    });
  }

  const accessRequest = await prisma.ar_access_request.findUnique({
    where: {
      id: request.id
    },
    include: includeAll
  });
  email.accessRequestRevised(accessRequest);
  return accessRequest;
}

async function cancelAccessRequest (req) {
  const { params: { id } } = req;

  await prisma.ar_access_request.updateMany({
    where: {
      id: parseInt(id),
      user: {
        email: req.user.email
      }
    },
    data: {
      cancelled: 1,
      updated_at: new Date()
    }
  });

  const accessRequest = await prisma.ar_access_request.findUnique({
    where: {
      id: parseInt(id)
    },
    include: includeAll
  });

  email.accessRequestCancelled(accessRequest);

  return accessRequest;
}

module.exports = {
  getDataset,
  getAllDatasets,
  getAccessRequestData,
  getAllAccessRequests,
  createAccessRequest,
  listEvaluations,
  getMyAccessRequests,
  submitEvaluation,
  submitSAMEvaluation,
  getEvaluationData,
  uploadCv,
  downloadCv,
  submitRevision,
  cancelAccessRequest
};

const state = {
  id: true,
  title: true,
  slug: true
};

const job = {
  id: true,
  title: true,
  slug: true,
  state: {
    select: state
  },
  author: true,
  response: true,
  project: true,
  scenario: true,
  percentage: true,
  execution_datetime: true,
  creation_datetime: true,
  delete_datetime: true
};

const fullJob = {
  ...job,
  engine_id: true
};

const baseProject = {
  id: true,
  title: true,
  author: true, // required to verify authorization
  members: true, // required to verify authorization
  owners: true, // required to verify authorization
  delete_datetime: true // required to validate if deleted
};

const project = {
  ...baseProject,
  short_description: true,
  creation_datetime: true,
  state: {
    select: state
  },
  keywords: {
    select: {
      value: true
    }
  },
  jobs: {
    select: job
  },
  datasets: true,
  scenarios: true,
  main_scenario: true
};

const projectSummary = {
  ...baseProject,
  state: {
    select: state
  },
  scenarios: {
    select: {
      id: true,
      jobs: {
        select: {
          id: true,
          title: true,
          state: true,
          creation_datetime: true
        }
      }
    }
  },
  jobs: {
    select: {
      id: true,
      scenario: {
        select: {
          id: true
        }
      },
      state: true
    },
    where: {
      state_id: 4
    }
  },
  datasets: true
};

const scenarios = {
  id: true,
  projects: true,
  jobs: true,
  project_main_scenario: true
};

const users = {
  id: true,
  email: true,
  username: true,
  first_name: true,
  last_name: true,
  image: true,
  jti: true,
  author_project: true,
  author_jobs: true,
  owned_projects: true,
  member_projects: true,
  service_providers: true
};

const notifications = {
  id: true,
  title: true,
  details: true,
  url: true,
  type: true,
  seen: true,
  reason: true,
  user: true,
  creation_datetime: true,
  delete_datetime: true
};

const presets = {
  id: true,
  query: true,
  title: true,
  description: true,
  creation_datetime: true,
  delete_datetime: true
};

module.exports = {
  project,
  projectSummary,
  job,
  fullJob,
  scenarios,
  users,
  notifications,
  presets
};

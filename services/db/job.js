/**
 * Service of Job to save data in database
 * @namespace Job_Service
 */

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const schemas = require('./schemas');
const { nanoid } = require('../nanoid');
const { getFilterParams } = require('./parameters');

const viewSchemaMap = {
  display: 'job',
  full: 'fullJob'
};

/**
 * Function to delete a job by id
 * @method
 * @async
 * @memberof Job_Service
 * @param {object} req - Request by User
 * @returns {object} Job information
 */
async function deleteJob (req) {
  const { params: { jobId }, context: { logger } } = req;
  logger.debug(`[Job Service] Deleting info of job ${jobId} in prisma service`);
  return await prisma.job.delete({
    where: {
      id: jobId
    }
  });
}

/**
 * Function to create a job
 * @method
 * @async
 * @memberof Job_Service
 * @param {object} req - Request by User
 * @param {string} slug - slug for the Job
 * @returns {object} Job information
 */
async function createJob (req, slug) {
  const { params: { scenarioId }, context: { logger }, project, user } = req;

  logger.info(`Creating job with slug in prisma ${slug}`);
  const result = await prisma.job.create({
    data: {
      id: nanoid(),
      slug,
      state: {
        connect: { slug: 'not-ready' }
      },
      project: {
        connect: { id: project.id }
      },
      scenario: {
        connect: { id: scenarioId }
      },
      author: {
        connect: { id: user.id }
      }
    }
  });
  return result;
}

/**
 * Function to update a job by id
 * @method
 * @async
 * @memberof Job_Service
 * @param {object} req - Request by User
 * @param {object} data - Data to be changed
 * @returns {object} Job information
 */
async function updateJob (req, data) {
  const { params: { jobId }, context: { logger } } = req;
  logger.debug(`[Job Service] Updating data of job ${jobId}`);

  const result = await prisma.job.update({
    where: {
      id: jobId
    },
    data,
    select: schemas.job
  });
  return result;
}

/**
 * Function to get jobs by user
 * @method
 * @async
 * @memberof Job_Service
 * @param {object} req - Request by User
 * @returns {object} list of jobs
 */
async function getJobs (req) {
  const { user: { id: userId }, context: { logger } } = req;
  logger.debug(`[Job Service] Getting list of jobs by user ${userId}`);

  const filterUser = {
    AND: [
      { delete_datetime: null },
      {
        project: {
          OR: [
            { author_id: userId },
            { owners: { some: { id: userId } } },
            { members: { some: { id: userId } } }
          ]
        }
      }
    ]
  };

  const query = getFilterParams(req);

  query.where = {
    ...query.where,
    ...filterUser
  };

  query.select = schemas.job;

  const result = await prisma.job.findMany(query);

  return result;
}

/**
 * Function to get job
 * @method
 * @async
 * @memberof Job_Service
 * @param {object} req - Request by User
 * @returns {object} job information
 */
async function getJob (req, view = 'display') {
  const { user: { id: userId }, params: { jobId }, context: { logger } } = req;
  const schemaKey = viewSchemaMap[view];
  logger.debug(`[Job Service] Getting a job ${jobId} for the user ${userId}`);
  const result = await prisma.job.findUnique({
    where: {
      id: jobId
    },
    select: schemas[schemaKey]
  });
  return result;
}

/**
 * Retrieve all running jobs
 * @param {*} req request object with context.logger
 * @returns all the jobs with state = 'running'
 */
async function getRunningJobs (req) {
  const { context: { logger } } = req;
  logger.debug('[Job Service] Getting all running jobs');
  const result = await prisma.state.findUnique({
    where: {
      slug: 'running'
    },
    select: {
      job_state: {
        select: {
          id: true
        }
      }
    }
  });
  return result.job_state;
}

module.exports = {
  deleteJob,
  createJob,
  updateJob,
  getJobs,
  getRunningJobs,
  getJob
};

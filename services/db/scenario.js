/**
 * Service of Scenario to save data in database
 * @namespace Scenario_Service
 */

const { PrismaClient } = require('@prisma/client');
const schemas = require('./schemas');
const prisma = new PrismaClient();

function baseUpdateQuery (projectId) {
  return {
    where: {
      id: projectId
    },
    select: schemas.project
  };
}

/**
 * Function to import a Scenario to a project
 * @method
 * @async
 * @memberof Scenario_Service
 * @param {object} req - Request by User
 * @returns {object} Project information
 */
async function importScenarioInProject (req) {
  const { params: { projectId, scenarioId }, context: { logger } } = req;
  logger.debug(`[Scenario Service] Importing scenario into a project ${projectId}`);
  const query = baseUpdateQuery(projectId);
  query.data = {
    scenarios: {
      connectOrCreate: {
        where: { id: scenarioId },
        create: { id: scenarioId }
      }
    }
  };
  return await prisma.project.update(query);
}

/**
 * Function to disconnect a scenario from a project
 * @method
 * @async
 * @memberof Scenario_Controller
 * @param {object} req - Request by User
 * @param {string} nameObject - Name of the object to connect or disconnect
 * @returns {object} Project information
 */
async function disconnectScenarioProject (req) {
  const { params: { projectId, scenarioId }, context: { logger } } = req;
  logger.debug(`[Scenario Service] Disconnecting scenario from project ${scenarioId}`);
  const query = baseUpdateQuery(projectId);
  query.data = {
    scenarios: {
      disconnect: {
        id: scenarioId
      }
    }
  };
  return await prisma.project.update(query);
}

/**
 * Function to disconnect main scenario from a project
 * @method
 * @async
 * @memberof Scenario_Controller
 * @param {object} req - Request by User
 * @param {string} nameObject - Name of the object to connect or disconnect
 * @returns {object} Project information
 */
async function disconnectMainScenarioProject (req) {
  const { params: { projectId, scenarioId }, context: { logger } } = req;
  logger.debug(`[Scenario Service] Disconnecting scenario from project ${scenarioId}`);
  const query = baseUpdateQuery(projectId);
  query.data = {
    main_scenario: {
      disconnect: true
    }
  };
  return await prisma.project.update(query);
}

/**
 * Function to update a scenario
 * @method
 * @async
 * @memberof Scenario_Service
 * @param {object} req - Request by User
 * @param {object} data - Data to be changed
 * @returns {object} Scenario information
 */
async function updateScenario (req, data) {
  const { params: { scenarioId }, context: { logger } } = req;
  logger.debug(`[DB Service] Updating data of scenario ${scenarioId}`);
  const result = await prisma.scenario.update({
    where: {
      id: scenarioId
    },
    data
  });
  return result;
}

module.exports = {
  importScenarioInProject,
  disconnectScenarioProject,
  disconnectMainScenarioProject,
  updateScenario
};

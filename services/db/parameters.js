/**
 * Function to create structure to filter parameters in prisma
 * @method
 * @memberof Prisma_Service
 * @param {object} req - Request by User
 * @returns {object} Filters
 */
function getFilterParams (req) {
  let query = {};

  if (req.pagination) {
    query = {
      ...query,
      ...getPaginationParams(req)
    };
  }

  if (req.searchFilter && req.searchFilter.search) {
    query.where = getSearchParams(req);
  }

  if (req.order) {
    query.orderBy = getOrderByParams(req);
  }

  return query;
}

/**
 * Function to order rows depending of the direction
 * @method
 * @memberof Prisma_Service
 * @param {object} req - Request by User
 * @returns {object} With order structure
 */
function getOrderByParams (req) {
  return {
    [req.order.field]: req.order.dir
  };
}

/**
 * Function to create structure to paginate information
 * @method
 * @memberof Prisma_Service
 * @param {object} req - Request by User
 * @returns {object} Pagination structure
 */
function getPaginationParams (req) {
  const pagination = {};
  if (req.pagination.limit) {
    pagination.take = req.pagination.limit;
  }
  if (req.pagination.offset) {
    pagination.skip = req.pagination.offset;
  }
  return pagination;
}

/**
 * Function to create structure to search parameters
 * @method
 * @memberof Prisma_Service
 * @param {object} req - Request by User
 * @returns {object} Search structure
 */
function getSearchParams (req) {
  let where = {};
  const typeFilter = req.searchFilter.match === 'exact' ? 'equals' : 'contains';
  if (Array.isArray(req.searchFilter.in)) {
    where = {
      OR: []
    };
    req.searchFilter.in.forEach(key => {
      where.OR.push({
        [key]: { [typeFilter]: req.searchFilter.search }
      });
    });
  } else {
    where = {
      [req.searchFilter.in]: {
        [typeFilter]: `${req.searchFilter.search}`
      }
    };
  }
  return where;
}

module.exports = {
  getFilterParams
};

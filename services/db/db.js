/**
 * Service of Prisma to save data in database
 * @namespace Prisma_Service
 */

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

/**
 * Function to get a user by id
 * @method
 * @async
 * @memberof Prisma_Service
 * @param {object} req - Request by User
 * @param {string} userId - Id of the user
 * @returns {object} User information
 */
async function getUser (req, userId) {
  const { context: { logger } } = req;
  logger.debug(`[DB Service] Getting info of user ${userId} in prisma service`);
  return await prisma.user.findUnique({
    where: {
      id: userId
    }
  });
}

module.exports = {
  getUser
};

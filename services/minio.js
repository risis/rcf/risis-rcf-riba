/**
 * Service to upload files to the server
 * @namespace Minio_Service
 */

const Minio = require('minio');

const { storage: serviceConfig } = global.config.services;

const errors = require('../utils/errors');
const archiver = require('archiver');

const minioClient = new Minio.Client({
  endPoint: serviceConfig.uri.hostname,
  port: serviceConfig.uri.port,
  useSSL: serviceConfig.ssl,
  accessKey: serviceConfig.keys.access,
  secretKey: serviceConfig.keys.secret
});

/**
 * Function to get file to the server
 * @method
 * @async
 * @memberof Minio_Service
 * @param {object} req - Request by User
 */
async function getFile (req) {
  const { user, project, job, context: { logger } } = req;
  const path = `${user.id}/projects/${project.id}/scenarios/${job.scenario.id}/${job.id}/input/input.yaml`;
  // const path = 'ybPwEA1DNUP/projects/5LxPE7L40-F/scenarios/ipc-analysis/A4VnuQ0Ws0J/input/input.yaml';
  logger.debug(`Getting input.yaml in path ${path}`);
  return new Promise((resolve, reject) => {
    return minioClient.getObject(
      serviceConfig.buckets.workspaces.key,
      path,
      (err, dataStream) => {
        if (err) {
          console.error(err);
          return reject(new Error('Encountered Error while getting file'));
        }
        return resolve(dataStream);
      }
    );
  });
}

/**
 * Function to get file to the server
 * @method
 * @async
 * @memberof Minio_Service
 * @param {string} path - Path of the file in minio repository
 * @param {object} accessRequest - Access Request
 */
async function getFileByPath (path, accessRequest) {
  return new Promise((resolve, reject) => {
    const filePath = `data/cv${accessRequest.id}.pdf`;
    return minioClient.fGetObject(
      serviceConfig.buckets.workspaces.key,
      path,
      filePath,
      (err, file) => {
        if (err) {
          return reject(new Error('Encountered Error while getting file'));
        }
        return resolve(filePath);
      }
    );
  });
}

/**
 * Function to upload file to the server
 * @method
 * @async
 * @memberof Minio_Service
 * @param {string} fileName - Name of the file
 * @param {string} localFilePath - Path of the local file to save
 * @param {object} req - Request by User
 * @returns {object} id of user in session
 */
async function putFile (fileName, localFilePath, metadata, req) {
  const { context: { logger } } = req;

  try {
    return await minioClient.fPutObject(
      serviceConfig.buckets.workspaces.key,
      fileName,
      localFilePath,
      metadata
    );
  } catch (e) {
    logger.error('Error uploading file to storage');
    logger.error(e);
    throw new errors.RuntimeError('Error uploading file to storage');
  }
}

/**
 * Function to copy file in other folder
 * @method
 * @async
 * @memberof Minio_Service
 * @param {string} fileName - Name of the file
 * @param {string} localFilePath - Path of the local file to save
 * @param {object} req - Request by User
 * @returns {object} minio response
 */
async function copyFile (filePath, destinationPath, req) {
  const { context: { logger } } = req;
  logger.debug(`Copy file ${filePath} in ${destinationPath}`);
  try {
    return await minioClient.copyObject(
      serviceConfig.buckets.workspaces.key,
      destinationPath,
      `${serviceConfig.buckets.workspaces.key}/${filePath}`
    );
  } catch (e) {
    logger.error('Error coping file to storage');
    logger.error(e);
    throw new errors.RuntimeError('Error coping file to storage');
  }
}

/**
 * Function to get files on the server
 * @method
 * @memberof Minio_Service
 * @param {string} path - Path of the folder
 * @param {object} req - Request by User
 * @returns {array} Files
 */
function listPathContent (path, req) {
  try {
    return minioClient.extensions.listObjectsV2WithMetadata(
      serviceConfig.buckets.workspaces.key,
      path,
      true
    );
  } catch (e) {
    console.log(e);
    return 'error';
  }
}

async function makePathPublic (path) {
  try {
    const polityBucket = `{"Statement":[{"Action":"s3:GetObject","Effect":"Allow","Principal":
    "*","Resource":"arn:aws:s3:::${serviceConfig.buckets.workspaces.key}/${path}/*"}],"Version": "2012-10-17"}`;
    return await minioClient.setBucketPolicy(serviceConfig.buckets.workspaces.key, polityBucket);
  } catch (e) {
    throw new errors.RuntimeError('Error making path public', e);
  }
}

function clearPath (path, req) {
  const { context: { logger } } = req;

  return new Promise((resolve, reject) => {
    const objectsStream = listPathContent(path, req);
    const objectsList = [];

    objectsStream.on('data', (obj) => {
      objectsList.push(obj.name);
    });

    objectsStream.on('error', (e) => {
      throw new errors.RuntimeError('Error trying to delete objects', e);
    });

    objectsStream.on('end', async () => {
      logger.debug('Listed objects to delete');
      try {
        const result = await minioClient.removeObjects(
          serviceConfig.buckets.workspaces.key,
          objectsList
        );
        logger.debug('Directory cleared');
        resolve(result);
      } catch (e) {
        reject(e);
      }
    });
  });
}

function getPathAsZip (req, minioPath, objectName) {
  const { context: { logger } } = req;
  logger.info(`get object ${objectName} on minio path ${minioPath} inside bucket ${serviceConfig.buckets.workspaces.key}`);
  return new Promise((resolve, reject) => {
    const infoStream = minioClient.listObjects(
      serviceConfig.buckets.workspaces.key,
      minioPath,
      true
    );
    const archive = archiver('zip');
    archive.on('warning', (e) => {
      reject(new errors.RuntimeError('Archiver warning ', e));
    });
    archive.on('error', (e) => {
      reject(new errors.RuntimeError('Archiver error ', e));
    });
    const objects = [];
    infoStream.on('data', (obj) => {
      objects.push({
        stream: minioClient.getObject(serviceConfig.buckets.workspaces.key, obj.name),
        fileName: obj.name.substring(obj.name.indexOf(objectName)) // remove 'dev/' or 'prod/' from filenames
      });
    });
    infoStream.on('error', (e) => {
      reject(new errors.RuntimeError('Error getting scenario files metadata', e));
    });
    infoStream.on('end', async () => {
      logger.info('objects to zip');
      logger.info(objects.map((obj) => obj.fileName));
      for (const object of objects) {
        archive.append(await object.stream, { name: object.fileName });
      }
      archive.finalize();
      resolve(archive);
    });
  });
}

module.exports = {
  getFile,
  putFile,
  listPathContent,
  makePathPublic,
  clearPath,
  copyFile,
  getPathAsZip,
  getFileByPath
};

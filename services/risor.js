const url = require('url');
const got = require('got');
const errors = require('../utils/errors');
const fetch = require('node-fetch');
const httpUri = url.format(global.config.services.risor.uri);

async function executeRequest (req, endpoint) {
  const { context: { logger } } = req;
  try {
    const scenarioBundleUrl = `${httpUri}${endpoint}`;
    return await got(scenarioBundleUrl, { headers: { Authorization: `Bearer ${req.token}` } });
  } catch (e) {
    logger.error('Error in risor service');
    logger.error(e);
    throw new errors.RuntimeError('Error with external service');
  }
}

function downloadScenarioBundle (req) {
  return got.stream(`${httpUri}/scenarios/${req.params.scenarioId}/bundle`, { headers: { Authorization: `Bearer ${req.token}` } });
}

function runScenario (req, scenarioId, version, data) {
  fetch(`${httpUri}/scenarios/${scenarioId}`, {
    method: 'post',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      Accept: `application/vnd.rsml.v${version}+json`,
      Authorization: `Bearer ${req.token}`
    }
  });
}

async function stopScenarioInstance (req) {
  const instanceId = req.job.engine_id;

  const reqBody = { command: 'STOP' };

  const response = await fetch(`${httpUri}/scenarios/instances/${instanceId}/actions`, {
    method: 'post',
    body: JSON.stringify(reqBody),
    headers: {
      'Content-Type': 'application/json',
      'X-Request-Id': req.context.traceid,
      Authorization: `Bearer ${req.token}`
    }
  });

  const responseBody = await response.json();

  return responseBody;
}

async function getScenario (req, scenarioId) {
  return await executeRequest(req, `/scenarios/${scenarioId}`);
}

async function getScenarios (req) {
  return await executeRequest(req, '/scenarios');
}

async function getProviders (req) {
  return await executeRequest(req, '/providers');
}

module.exports = {
  downloadScenarioBundle,
  runScenario,
  stopScenarioInstance,
  getScenario,
  getScenarios,
  getProviders
};

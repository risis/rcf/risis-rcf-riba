const nodemailer = require('nodemailer');
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const settings = global.config.mailserver;

const transporter = nodemailer.createTransport({
  port: 25,
  host: settings.host,
  secure: false,
  tls: {
    rejectUnauthorized: false
  }
});

const salute = {
  html: '<p>Best regards, </p>The RISIS Core Facility Team',
  text: `
    Best regards,
    The RISIS Core Facility Team
  `
};
const rimowUrl = process.env.RIMOW_URL;

function getSecondaryAccessManagers (accessRequest) {
  const accessManagers = [];
  accessRequest.datasets.filter(d => (d.is_main === 0 && d.decision !== 0)).forEach(dataset => {
    dataset.dataset.access_managers.forEach(am => {
      accessManagers.push(am.user_email);
    });
  });
  return accessManagers;
}

async function accessRequestEmails (accessRequest, types) {
  const specialUsers = await prisma.ar_special_users.findMany();
  const emails = {
    mam: accessRequest.datasets.find(ard => ard.is_main === 1).dataset.access_managers.map(d => d.user_email),
    sam: getSecondaryAccessManagers(accessRequest),
    appl: [accessRequest.user.email],
    pc: specialUsers.filter(user => user.role === 'pc').map(u => u.email)
  };

  if (types === '*') {
    return emails.mam.concat(emails.sam, emails.appl, emails.pc);
  } else {
    let res = [];
    types.forEach(type => {
      res = res.concat(emails[type]);
    });
    return res;
  }
}

function detailsLink (accessRequest) {
  return `${rimowUrl}access-request/${accessRequest.id}/details`;
}

function detailsTag (accessRequest, label = null) {
  if (!label) {
    label = accessRequest.project_title;
  }
  return `<a href="${detailsLink(accessRequest)}">${label}</a>`;
}

function evaluationLink (accessRequest) {
  return `${rimowUrl}access-request/${accessRequest.id}/evaluation`;
}

function evaluationTag (accessRequest, label = 'Evaluation') {
  return `<a href="${evaluationLink(accessRequest)}">${label}</a>`;
}

function reviseLink (accessRequest) {
  return `${rimowUrl}access-request/${accessRequest.id}/revise`;
}

function reviseTag (accessRequest, label = 'here') {
  return `<a href="${reviseLink(accessRequest)}">${label}</a>`;
}

function getSubject (subject, accessRequest) {
  const arTitle = accessRequest.project_title.length > 23 ? accessRequest.project_title.substring(0, 20) + '...' : accessRequest.project_title;
  return `[RCF] ${subject} (#${accessRequest.id}): ${arTitle}`;
}

/*
* Send an email when a new access request is created
* @method
* @param {object} accessRequest - the access request for which the email is sent
* */
async function newAccessRequest (accessRequest) {
  const html = `<p>Dear Access Manager,</p>
    there is a new access request for your dataset that you need to evaluate. <br>
    The evaluation page for the request can be found ${evaluationTag(accessRequest, 'here')}.
    ${salute.html}
    `;

  const text = `
    Dear Access Manager,
    
    there is a new access request for your dataset that you need to evaluate.
    The evaluation page for the request can be found at ${evaluationLink(accessRequest)}.
    ${salute.text}
    `;

  return sendEmail({ html, text }, 'New Access Request',
    await accessRequestEmails(accessRequest, ['mam']), accessRequest);
}

/*
* Send an email when a new access request is created
* @method
* @param {object} accessRequest - the access request for which the email is sent
* */
async function sendReviseEmail (accessRequest) {
  const html = `<p>Dear ${accessRequest.user.first_name} ${accessRequest.user.last_name},</p>
    the dataset access manager has stated that your access request needs some revision.<br>
    Please see the details ${detailsTag(accessRequest, ' at the details page')}, 
    and then proceed ${reviseTag(accessRequest, 'to the revise page')} to revise or cancel your access request.
    ${salute.html}
    `;

  const text = `
    Dear ${accessRequest.user.first_name} ${accessRequest.user.last_name},
    
    the dataset access manager has stated that your access request needs some revision.
    Please see the details at ${detailsLink(accessRequest)}, 
    and then proceed at ${reviseLink(accessRequest)} to revise or cancel your access request.
    ${salute.text}
    `;

  return sendEmail({ html, text }, 'Please revise your Access Request', await accessRequestEmails(accessRequest, ['app']), accessRequest);
}

/*
* Send an email after main access manager evaluates
* @method
* @param {object} accessRequest - the access request for which the email is sent
* */
async function newAccessRequestSAM (accessRequest) {
  const html = `<p>Dear Access Manager,</p>
    there is a new access request for your dataset that you need to evaluate. <br>
    The evaluation page for the request can be found ${evaluationTag(accessRequest, 'here')}.
    ${salute.html}
    `;
  const text = `
    Dear Access Manager,
    
    there is a new access request for your dataset that you need to evaluate.
    The evaluation page for the request can be found at ${evaluationLink(accessRequest)}.
    ${salute.text}
    `;

  return sendEmail({ html, text }, 'New Access Request', await accessRequestEmails(accessRequest, ['sam']), accessRequest);
}

/*
* Send an email after AMs evaluate
* @method
* @param {object} accessRequest - the access request for which the email is sent
* */
async function newAccessRequestPC (accessRequest) {
  const html = `<p>Dear Project Coordinator,</p>
    there is a new access request that you need to evaluate. <br>
    The evaluation page for the request can be found ${evaluationTag(accessRequest, 'here')}.
    ${salute.html}
    `;

  const text = `
    Dear Project Coordinator,
    
    there is a new access request that you need to evaluate. 
    The evaluation page for the request can be found at ${evaluationLink(accessRequest)}.
    ${salute.text}
    `;
  return sendEmail({ html, text }, 'New Access Request', await accessRequestEmails(accessRequest,
    ['pc']), accessRequest);
}

/*
* Send an email when a new access request has been revised
* @method
* @param {object} accessRequest - the access request for which the email is sent
* */
async function accessRequestRevised (accessRequest) {
  const html = `<p>Dear Access Manager,</p>
    The access request ${detailsTag(accessRequest)} was revised by the applicant.<br>
    Please go to the ${evaluationTag(accessRequest, 'evaluation page')} to evaluate the revised version.
    ${salute.html}
    `;

  const text = `
    Dear Access Manager,
    
    the access request ${detailsLink(accessRequest)} was revised by the applicant.
    Please go at ${evaluationLink(accessRequest, 'evaluation page')} to evaluate the revised version.
    ${salute.text}
    `;

  return sendEmail({ html, text }, 'Access Request Revised', await accessRequestEmails(accessRequest, ['mam']), accessRequest);
}

/*
* Send an email when an access request has been cancelled
* @method
* @param {object} accessRequest - the access request for which the email is sent
* */
async function accessRequestCancelled (accessRequest) {
  const html = `<p>Dear Evaluator,</p>
    the access request ${detailsTag(accessRequest)} has been cancelled by the applicant.<br>
    ${salute.html}
    `;

  const text = `
    Dear Evaluator,
    
    the access request ${detailsLink(accessRequest)} has been cancelled by the applicant.
    ${salute.text}
    `;

  return sendEmail({ html, text }, 'Access Request Cancelled',
    await accessRequestEmails(accessRequest, ['mam', 'sam', 'pc']), accessRequest);
}

/*
* Send an email when an access request has been denied
* @method
* @param {object} accessRequest - the access request for which the email is sent
* */
async function accessRequestDenied (accessRequest) {
  const html = `<p>Dear ${accessRequest.user.first_name} ${accessRequest.user.last_name},</p>
    your access request  at RISIS platform has been declined in the evaluation process.<br>
    You can view the evaluation details ${detailsTag(accessRequest, 'here')}
    ${salute.html}
    `;
  const text = `
    Dear ${accessRequest.user.first_name} ${accessRequest.user.last_name},
    
    your access request at RISIS platform has been declined in the evaluation process.
    You can view the evaluation details at ${detailsLink(accessRequest)}.
    ${salute.text}
    `;
  return sendEmail({ html, text }, 'Access Request Declined', await accessRequestEmails(accessRequest, '*'), accessRequest);
}

/*
* Send an email when an access request has been accepted
* @method
* @param {object} accessRequest - the access request for which the email is sent
* */
async function accessRequestAccepted (accessRequest) {
  const html = `<p>Dear ${accessRequest.user.first_name} ${accessRequest.user.last_name},</p>
    your access request at the RISIS platform has been evaluated positively.<br/>
    For more information on the evaluation of your request, you can go to ${detailsTag(accessRequest, detailsLink(accessRequest))} .<br/>
    The dataset access manager(s) will start with the data preparation and will contact you soon.
    ${salute.html}. 
    `;
  const text = `
    Dear ${accessRequest.user.first_name} ${accessRequest.user.last_name},
    
    your access request at the RISIS platform has been evaluated positively.
    For more information on the evaluation of your request, you can go to ${detailsLink(accessRequest)}.
    The dataset access manager(s) will start with the data preparation and will contact you soon.
    ${salute.text}. 
    `;
  return sendEmail({ html, text }, 'Access Request Accepted', await accessRequestEmails(accessRequest, '*'), accessRequest);
}

/*
* Send an email method
* @method
* @param {string} template - The HTML string (content) of the email
* @param {string} subject - the subject of the email
* @param {array<string>} receivers - the list of the email receivers
* */
async function sendEmail (template, subject, recipients, accessRequest) {
  subject = getSubject(subject, accessRequest);

  for (const email of [...new Set(recipients)]) {
    await transporter.sendMail({
      from: settings.from,
      to: email,
      bcc: settings.bcc,
      subject,
      html: template.html,
      alternatives: [
        {
          contentType: 'text/plain',
          content: template.text
        }
      ]
    });
  }
}

module.exports = {
  newAccessRequest,
  accessRequestRevised,
  accessRequestCancelled,
  accessRequestDenied,
  accessRequestAccepted,
  newAccessRequestSAM,
  newAccessRequestPC,
  sendReviseEmail
};

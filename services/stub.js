const path = require('path');
const appRoot = require('app-root-path').toString();
const fs = require('fs');

const errors = require('../utils/errors');

const STUB_FILE_PATH = path.join(appRoot, 'fixtures/stubs.json');
let stubFile = {};

function saveChange () {
  writeStubs();
  readStubs();
}

function writeStubs () {
  fs.writeFileSync(STUB_FILE_PATH, null, 2);
}

function readStubs () {
  stubFile = JSON.parse(fs.readFileSync(STUB_FILE_PATH));
}

async function getUser (req) {
  const { user: { id: userId } } = req;

  const user = stubFile.users.find((u) => user.id === userId);
  if (!user) {
    throw new errors.NotFoundError('The user does not exist');
  }
  return user;
}

async function getProject (req) {
  const { params: { projectId } } = req;

  const project = stubFile.projects.find((p) => p.id === projectId);
  if (!project) {
    throw new errors.NotFoundError('The project does not exist');
  }
}

async function getUserProjects (req) {
  const { user: { id: userId }, pagination, searchFilter } = req;

  let projects = stubFile.projects.filter((p) => {
    if (p.owner_id === userId) {
      return true;
    }
    if (p.members && p.members.includes(userId)) {
      return true;
    }
    return false;
  });

  if (searchFilter) {
    projects = projects.filter((p) => {
      if (p.name && p.name.includes(searchFilter.searchTerm)) {
        return true;
      }
      if (p.description && p.description.includes(searchFilter.searchTerm)) {
        return true;
      }
      return false;
    });
  }

  if (pagination) {
    if (pagination.skip) {
      projects = projects.slice(pagination.skip);
    }
    if (pagination.take) {
      projects = projects.slice(0, pagination.take);
    }
  }

  return projects;
}

async function createProject (req) {
  const { newProject: { ownerId, name, description, keywords } } = req;

  const maxIdProject = stubFile.projects.reduce((max, current) => {
    if (current.id > max.id) {
      max = current;
    }
    return max;
  }, { id: Number.MIN_SAFE_INTEGER });

  const nextId = maxIdProject.id + 1;

  stubFile.projects.push({
    id: nextId,
    name,
    description,
    keywords,
    owner_id: ownerId
  });

  saveChange();
}

readStubs();

module.exports = {
  getUser,
  getProject,
  getUserProjects,
  createProject
};

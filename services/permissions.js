/**
 * Permission Service
 * @namespace Permission_Service
 */

const errors = require('../utils/errors');

/**
 * Function to verify if the current user has Admin permission
 * @method
 * @async
 * @memberof Users_Controller
 * @param {object} req - Request by User
 * @returns {object} User information
 */
async function validateAdminPermissions (req) {
  const { user, tokenDecoded, context: { logger } } = req;
  logger.debug(`[Users Controller] checking permission of the user ${user}`);
  if (tokenDecoded.realm_access && tokenDecoded.realm_access.roles && !req.isService) {
    const roles = tokenDecoded.realm_access.roles;
    const scopesLower = ['Local Admin', 'Super Admin'].map(s => s.toLowerCase());
    const rolesLower = roles.map(r => r.toLowerCase());
    const hasRol = rolesLower.some(role => scopesLower.includes(role));
    if (!hasRol) {
      throw new errors.UnauthorizedError('Do not have sufficient permissions to access the resource');
    }
  }
}

module.exports = {
  validateAdminPermissions
};

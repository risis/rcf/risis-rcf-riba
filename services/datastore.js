/**
 * Datastore Service
 * @namespace Datastore_Service
 */

const { datastore: datastoreConfig } = global.config.services;
const datastoreApiUrl = datastoreConfig.apiUrl;

const slugify = require('slugify');
const postgres = require('postgres');
const RcfDatastoreClient = require('@cortext/rcf-datastore-client');
const { v4: uuid } = require('uuid');
const AbortController = require('abort-controller');
const { StringDecoder } = require('string_decoder');
const { parse } = require('csv-parse/sync');
const path = require('path');

const errors = require('../utils/errors');

const UTF8decoder = new StringDecoder('utf8');

const adminDatastoreClient = new RcfDatastoreClient(datastoreApiUrl, datastoreConfig.apiToken, datastoreConfig.unblockKey);

function createUserClient (req) {
  const { user, context: { logger } } = req;
  if (req.isService) {
    logger.debug(`[Datastore Service] The user ${user.id} is a client with Service role, setting datastore client as admin.`);
    return adminDatastoreClient;
  } else {
    const service = user.service_providers.find(service => service.idService.toLowerCase() === 'datastore');
    if (service && service.token) {
      return new RcfDatastoreClient(datastoreApiUrl, service.token);
    } else {
      logger.error(`[Datastore Service] The user ${user.id} does not have the datastore token to manage datasets.`);
      throw new errors.ArgumentError('The user does not have the token to manage datasets.');
    }
  }
}

/**
 * Function to request to datastore-client to get all datasets
 * @method
 * @async
 * @memberof Datastore_Service
 * @param {object} req - Request by User
 * @returns {array} List of the datasets with full information.
 */
async function getDatasets (req) {
  const { context: { logger } } = req;
  logger.debug('[Datastore Service] Requesting list of datasets to datastore client');
  const dataversIds = [datastoreConfig.privateDataverseID, datastoreConfig.userDataverseID, datastoreConfig.openDataverseID];
  const datasets = await req.context.datastoreClient.listDatasets(dataversIds);
  const newDatasets = await Promise.all(
    datasets.map((dataset) => {
      req.params = {
        datasetId: `${dataset.protocol}:${dataset.authority}/${dataset.identifier}`
      };
      return getDataset(req);
    })
  );
  return newDatasets.filter(e => { return e; });
}

/**
 * Function to request to datastore-client to get a dataset by doi
 * @method
 * @async
 * @memberof Datastore_Service
 * @param {object} req - Request by User
 * @returns {object} Dataset with full information.
 */
async function getDataset (req) {
  const { params: { datasetId }, context: { logger } } = req;
  logger.debug(`[Datastore Service] Asking the datastore client to get a dataset ${datasetId}`);
  const datasetResponse = await req.context.datastoreClient.getDatasetInformation(datasetId).catch((e) => {
    logger.error('Error getting dataset information in rcf-datastore-client', e);
    return null;
  });
  if (!datasetResponse) {
    return null;
  }
  const data = datasetResponse.data.data;
  const dataDatafield = data.latestVersion.metadataBlocks.citation.fields || null;
  if (!dataDatafield) {
    return null;
  }
  const title = dataDatafield.find(data => data.typeName === 'title');
  const description = dataDatafield.find(data => data.typeName === 'dsDescription');
  const keywords = dataDatafield.find(data => data.typeName === 'keyword');
  const subject = dataDatafield.find(data => data.typeName === 'subject');
  const authors = dataDatafield.find(data => data.typeName === 'author');
  const dataset = {
    _id: `${data.protocol}:${data.authority}/${data.identifier}`,
    title: title ? title.value : '',
    short_description: description ? description.value[0].dsDescriptionValue.value : '',
    slug: title ? slugify(title.value) : '',
    letters: title ? title.value.replace(/[^a-z]/gi, '').charAt(0).toUpperCase() : '',
    versions: {
      '1.0.0': {
        authors_ids: authors ? authors.value.map(author => author.authorName.value) : [],
        owners_ids: authors ? authors.value.map(author => author.authorName.value) : [],
        tags: keywords ? keywords.value.map(keyword => keyword.keywordValue.value) : [],
        subject: subject || '',
        short_description: description ? description.value[0].dsDescriptionValue.value : '',
        version_type: slugify(data.latestVersion.versionState),
        status: 'available',
        date: data.latestVersion.createTime,
        files: formatFiles(data.latestVersion.files)
      }
    }
  };
  return dataset;
}

function formatFiles (files) {
  const objFiles = {};
  if (files) {
    files.forEach(file => {
      objFiles[file.label] = {
        _id: file.dataFile.id,
        slug: slugify(file.label),
        title: file.label,
        type: file.dataFile.contentType,
        name: file.label,
        status: 'available',
        letters: file.label.replace(/[^a-z]/gi, '').charAt(0).toUpperCase(),
        md5sum: file.dataFile.md5,
        size: file.dataFile.filesize
      };
    });
  }
  return objFiles;
}

async function getFileMetadata (req) {
  const { params: { fileId }, context: { datastoreClient, logger } } = req;
  logger.debug(`[Datastore Service] Retrieve file ${fileId}`);
  const response = await datastoreClient.client.getFileMetadata(fileId, true);
  const result = response.data;
  if (result.label) {
    if (['.csv', '.tsv'].includes(path.extname(result.label))) {
      try {
        result.fields = await fileColumnNames(req);
      } catch (e) {
        logger.info('Error fetching file fields', e);
      }
    }
  }
  return result;
}

function findColumnNames (lines) {
  // find delimiter with minimum ratio between
  // found separator and values in first row and max colNames
  function bestParseCandidate (first, second) {
    const EPSILON = 0.03;
    const nFieldsA = first.colNames.length;
    const nFieldsB = second.colNames.length;
    if (nFieldsA === 0) return second;
    if (nFieldsB === 0) return first;
    const ratioA = first.nLineValues / nFieldsA;
    const ratioB = second.nLineValues / nFieldsB;
    const diff = ratioA - ratioB;
    // if difference is minimal, the option with
    // the most fields is better
    if (Math.abs(diff) < EPSILON) {
      return nFieldsA > nFieldsB ? first : second;
    }
    return ratioA > ratioB ? first : second;
  }

  // find the most likely separator by trying them
  // all with the two lines and finding the best
  // possible one.
  const delimiterCandidates = ['\t', ',', ';'];
  const best = delimiterCandidates.reduce(
    (currentBest, candidate) => {
      const parseOptions = { delimiter: candidate };
      let best = currentBest;
      try {
        // csv-parser returns an array where each element
        // is an array containing the values in a row
        const [currentHeaders, currentLineValues] = parse(lines, parseOptions);
        best = bestParseCandidate(currentBest, {
          colNames: currentHeaders,
          nLineValues: currentLineValues.length
        });
      } catch (e) {} // don't compare delimiter if parse error
      return best;
    },
    { colNames: [], nLineValues: 0 }
  );

  return best.colNames;
}

function columnsFromHeader (header) {
  const delimiterCandidates = ['\t', ',', ';'];
  const best = delimiterCandidates.reduce(
    (currentBest, candidate) => {
      const parseOptions = { delimiter: candidate };
      let best = currentBest;
      try {
        // csv-parser returns an array where each element
        // is an array containing the values in a row
        const [currentHeaders] = parse(header, parseOptions);
        if (currentHeaders.length > best.length) {
          best = currentHeaders;
        }
      } catch (e) {} // don't compare delimiter if parse error
      return best;
    },
    []
  );
  return best;
}

async function fileColumnNames (req) {
  const { params: { fileId }, context: { datastoreClient, logger } } = req;
  let decoded = '';
  let firstLinebreak = -1;
  let secondLinebreak = -1;
  let bytesDownloaded = 0;
  const maxBytes = 1024 * 256; // 256kb
  const controller = new AbortController();
  const res = await datastoreClient.streamDownloadFile(fileId, {
    signal: controller.signal
  });
  return new Promise((resolve, reject) => {
    // fetch the first two lines of the file or the first 256kb
    // then stop downloading it with abort-controller
    // https://www.npmjs.com/package/abort-controller
    res.body.on('data', (chunk) => {
      // keep track of linebreak positions to stop downloading file
      // when two lines have been retrieved
      bytesDownloaded += chunk.byteLength;
      if (bytesDownloaded > maxBytes) controller.abort();
      // file is assumed to be utf8 encoded.
      decoded += UTF8decoder.write(chunk);
      firstLinebreak = decoded.indexOf('\n');
      if (firstLinebreak > -1) {
        secondLinebreak = decoded.lastIndexOf('\n');
        if (firstLinebreak !== secondLinebreak) {
          controller.abort();
        }
      }
    });
    res.body.on('error', (error) => {
      if (error.name === 'AbortError') {
        if (firstLinebreak === -1 || secondLinebreak === -1) {
          logger.info('Could not fetch two lines');
          reject(new Error('Could not fetch two lines'));
        }
        logger.debug('Two lines fetched.');
        decoded = decoded.slice(0, secondLinebreak);
        const columnNames = findColumnNames(decoded);
        resolve(columnNames);
      } else {
        reject(error);
      }
    });
    res.body.on('end', () => {
      if (res.ok) {
        if (firstLinebreak !== -1) {
          decoded = decoded.slice(0, firstLinebreak);
        }
        const columnNames = columnsFromHeader(decoded);
        resolve(columnNames);
      } else {
        reject(new Error(res.status + decoded));
      }
    });
  });
}

/**
 * Function to request to datastore-client create a new dataset
 * @method
 * @async
 * @memberof Datastore_Service
 * @param {object} req - Request by User
 * @returns {object} Dataset created with full information.
 */
async function createDataset (req) {
  const { body: newDatasetData, user, context: { logger } } = req;
  let fullName = 'N/A';
  if (user.first_name || user.last_name) {
    fullName = `${user.first_name} ${user.last_name}`;
  }
  const data = {
    title: newDatasetData.title,
    descriptions: [{
      text: newDatasetData.short_description || 'No description',
      date: new Date().toJSON().slice(0, 10)
    }],
    authors: [
      {
        fullname: fullName
      }
    ],
    contact: [
      {
        email: user.email || 'N/A',
        fullname: fullName
      }
    ],
    subject: newDatasetData.subject ? [newDatasetData.subject] : ['Other'],
    keyword: newDatasetData.keywords ? newDatasetData.keywords.split(',') : []
  };
  logger.debug('[Datastore Service] Asking the datastore client to create a dataset');
  const result = await req.context.datastoreClient.createDataset(datastoreConfig.userDataverseID, data);
  req.params.datasetId = result.data.data.persistentId;
  await uploadFiles(req);
  return getDataset(req);
}

/**
 * Function to request to datastore-client delete a dataset by doi
 * @method
 * @async
 * @memberof Datastore_Service
 * @param {object} req - Request by User
 * @returns {object} Dataset delited with full information.
 */
async function deleteDataset (req) {
  const { params: { datasetId }, context: { logger } } = req;
  logger.debug(`[Datastore Service] Asking the datastore client to delete a dataset with id ${datasetId}`);
  return await req.context.datastoreClient.deleteDataset(datasetId);
}

/**
 * Function to request to datastore-client update a dataset by doi
 * @method
 * @async
 * @memberof Datastore_Service
 * @param {object} req - Request by User
 * @returns {object} Dataset updated with full information.
 */
async function updateDataset (req) {
  const { params: { datasetId }, body: newDatasetData, context: { logger } } = req;
  logger.debug(`[Datastore Service] Asking the datastore client to update a dataset with id ${datasetId}`);
  const data = {};
  if (newDatasetData.title) {
    data.title = newDatasetData.title;
  }
  if (newDatasetData.short_description) {
    data.dsDescription = [
      {
        dsDescriptionValue: {
          typeName: 'dsDescriptionValue',
          value: newDatasetData.short_description
        },
        dsDescriptionDate: {
          typeName: 'dsDescriptionDate',
          value: new Date().toJSON().slice(0, 10)
        }
      }
    ];
  }
  if (newDatasetData.keywords) {
    data.keywords = newDatasetData.keywords;
  }
  if (newDatasetData.subject) {
    data.subject = [newDatasetData.subject];
  }
  const keys = Object.keys(data);
  for (let i = 0; i < keys.length; i++) {
    const response = await req.context.datastoreClient.editDataset(datasetId, keys[i], { value: data[keys[i]] });
    if (!response.ok) {
      logger.error(`Error in risor updating dataset ${datasetId}. Status: ${response.status}, Status Text: ${response.statusText}`);
      throw new errors.RuntimeError('Error updating dataset');
    }
  }
  return getDataset(req);
}

/**
 * Function to request to datastore-client upload files into a dataset by doi
 * @method
 * @async
 * @memberof Datastore_Service
 * @param {object} req - Request by User
 * @returns {array} List of files
 */
async function uploadFiles (req) {
  const { files, params: { datasetId }, context: { logger } } = req;
  const results = [];
  for (let i = 0; i < files.length; i++) {
    const response = await req.context.datastoreClient.uploadFile(datasetId, files[i].path, files[i].originalname);
    if (!response.ok) {
      logger.debug(`Error in risor uploading files in dataset ${datasetId}. Status: ${response.status}, Status Text: ${response.statusText}`);
      throw new errors.RuntimeError('Error uploading files in dataset');
    }
    results.push(response);
  }
  return results;
}

async function downloadDataset (req) {
  const { params: { datasetId }, context: { logger } } = req;
  try {
    const response = await req.context.datastoreClient.downloadDatasetFiles(datasetId);
    return response.data;
  } catch (error) {
    logger.error(`Error downloading dataset ${datasetId}.`);
    logger.error(error);
    throw new errors.RuntimeError('Error downloading files in dataset');
  }
}

async function searchDatastoreUserByEmail (req) {
  const { user, context: { logger } } = req;

  logger.info(`Trying to find a user by email in postgres ${user.email}`);
  const sql = postgres({ ...datastoreConfig.database });
  const [existingUserId] = await sql`
    select useridentifier from public.authenticateduser
      where
    email = ${user.email}
  `;
  sql.end();
  return existingUserId;
}

async function getDatastoreUser (req) {
  const { user, context: { logger } } = req;

  try {
    logger.info(`getting user ${user.username} - id: ${user.id} from dataverse`);
    let response = await adminDatastoreClient.getUser(user.id);
    logger.info('Datastore user response', response);
    let json = await response.json();
    logger.info('Datastore user response in json format', json);
    if (json.status === 'OK') {
      logger.info('Datastore user found');
      logger.debug(json.data);
      return json.data;
    } else if (json.status === 'ERROR' && json.message.match(/not found/)) {
      const existingUserId = await searchDatastoreUserByEmail(req);
      if (existingUserId && existingUserId.useridentifier) {
        logger.info('User found with id ' + existingUserId.useridentifier);
        response = await adminDatastoreClient.getUser(existingUserId.useridentifier);
        json = await response.json();
        return json.data;
      }
      logger.info('Datastore user not found');
      return;
    } else {
      throw new errors.RuntimeError('unexpected dataverse response', { response, json });
    }
  } catch (e) {
    logger.error('error getting datastore user');
    logger.error(e);
    throw new errors.RuntimeError('unexpected dataverse error', e);
  }
}

async function createDatastoreUser (req) {
  const { user, context: { logger } } = req;

  // Standard user creation with dataverse admin API
  const createAuthenticatedUserInput = {
    authenticationProviderId: 'keycloak',
    persistentUserId: user.id,
    identifier: user.id,
    firstName: user.first_name,
    lastName: user.last_name || user.first_name,
    email: user.email
  };
  try {
    logger.debug('Creating authenticated user with datastore client', createAuthenticatedUserInput);
    let datastoreUserResponse = await adminDatastoreClient.createAuthenticatedUser(createAuthenticatedUserInput);
    logger.debug('Datastore create user response', datastoreUserResponse);
    datastoreUserResponse = await datastoreUserResponse.json();
    logger.debug('Datastore user response json', JSON.stringify(datastoreUserResponse, null, 2));
    const datastoreUser = datastoreUserResponse.data;
    return datastoreUser;
  } catch (e) {
    logger.error('error creating authenticated user', e);
    throw new errors.RuntimeError('unexpected error calling dataverse', e, { createAuthenticatedUserInput });
  }
}

async function getDataverseApiTokenRecord (req) {
  const { datastoreUser, context: { logger } } = req;

  logger.info(`Trying to find a token for ${datastoreUser.identifier}`);
  const sql = postgres({ ...datastoreConfig.database });
  const [existingTokenRow] = await sql`
    select * from public.apitoken
      where
    authenticateduser_id = ${datastoreUser.id}
  `;
  sql.end();
  return existingTokenRow;
}

async function getUserApiToken (req) {
  const { datastoreUser, context: { logger } } = req;

  // Unsupported apiToken addition
  // As there is no supported way to programatically create authenticated users with an API key
  // And inserting directly into the dataverse table had equivalent effects
  // We decided to generate and insert the token manually

  // Depending on the status of the apitoken table, we may need to do different things

  // If there is a token, set its validity to ten years from now, get it and return it

  logger.info(`Trying to find a token for ${datastoreUser.identifier}`);

  const existingApiTokenRow = await getDataverseApiTokenRecord(req);

  logger.info('Resulting row', existingApiTokenRow);

  // There is a token
  if (existingApiTokenRow !== undefined) {
    // Update validity and that enabled-ness
    logger.info('Token exists. Checking validity and enabled-ness', existingApiTokenRow.expiretime, existingApiTokenRow.disabled);
    if (existingApiTokenRow.expiretime > new Date() && !existingApiTokenRow.disabled) {
      logger.debug('Token valid and enabled');
      return existingApiTokenRow.tokenstring;
    } else {
      logger.debug('Token invalid or disabled. Updating');
      const sql = postgres({ ...datastoreConfig.database });
      const [updateResponse] = await sql`
        update public.apitoken
          set
        disabled = false, expiretime = now() + interval '10 year'
          where
        id = ${existingApiTokenRow.id}

        returning *
      `;
      sql.end();
      logger.info('New token row', updateResponse);
      return updateResponse.tokenstring;
    }
  } else {
    logger.info('Token doesnt exist. Creating.');
    const insertData = {
      tokenstring: uuid(),
      authenticateduser_id: datastoreUser.id
    };
    logger.debug('insertData', insertData);
    const sql = postgres({ ...datastoreConfig.database });
    const [dataverseApiTokenRow] = await sql`
      insert into public.apitoken (
        createtime, disabled, expiretime, tokenstring, authenticateduser_id
      ) values (
        now(), false, now() + interval '10 year', ${insertData.tokenstring}, ${insertData.authenticateduser_id}
      )
      returning *
    `;
    sql.end();
    logger.debug('dataverseApiTokenRow', dataverseApiTokenRow);
    return dataverseApiTokenRow.tokenstring;
  }
}

module.exports = {
  createUserClient,
  getDatasets,
  getDataset,
  getFileMetadata,
  createDataset,
  deleteDataset,
  updateDataset,
  uploadFiles,
  downloadDataset,
  getDatastoreUser,
  createDatastoreUser,
  getUserApiToken
};

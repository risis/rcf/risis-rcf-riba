const { customRandom, random } = require('nanoid');
const alphabet = 'abcdefghijklmnopqrstuvwxyz';
const numbers = '0123456789';
const safeURLSymbols = '-';
const nanoid = customRandom(`${alphabet}${numbers}${safeURLSymbols}`, 11, random);

module.exports = {
  nanoid
};

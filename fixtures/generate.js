/**
 * Generate Mock Data
 * @namespace Generate_Mock_Data
 */
const url = require('url');
const jsf = require('json-schema-faker');
const faker = require('faker');
const fs = require('fs');
const yaml = require('yaml');
const appRoot = require('app-root-path').toString();
const path = require('path');
const _ = require('lodash');
const { nanoid } = require('../services/nanoid');
const got = require('got');
const utils = require('../test/api/setup/utils');

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const config = require('config-lite')({
  config_basedir: appRoot,
  config_dir: 'config'
});
const { datastore: datastoreConfig } = config.services;
const RcfDatastoreClient = require('@cortext/rcf-datastore-client');
const datastoreApiUrl = datastoreConfig.apiUrl;
const datastoreApiToken = datastoreConfig.apiToken;
const client = new RcfDatastoreClient(datastoreApiUrl, datastoreApiToken, datastoreConfig.unblockKey);

jsf.extend('faker', () => faker);
jsf.option('alwaysFakeOptionals', true);

/**
 * Function to load Schema from file
 * @method
 * @memberof Generate_Mock_Data
 * @param {string} filePath - File Path to read Schema
 * @returns {object} Schema
 */
function loadSchemaFromFile (filePath) {
  const file = fs.readFileSync(filePath, 'utf8');
  return yaml.parse(file);
}

/**
 * Function to add fakers properties
 * @method
 * @memberof Generate_Mock_Data
 * @param {object} schema - Schema
 * @param {object} fakers - Fakers properties
 */
function addPropertyFakers (schema, fakers) {
  Object.keys(fakers).forEach((propertyName) => {
    schema.properties[propertyName].faker = fakers[propertyName];
  });
}

/**
 * Function to create a schema with fakers properties
 * @method
 * @memberof Generate_Mock_Data
 * @param {string} entityName - Name of entity
 * @param {string} modelName - Model of that entity
 * @param {object} propertyFakers - Fakers properties
 * @returns {object} Schema with fakers properties
 */

function makeStubSchema (entityName, modelName, propertyFakers) {
  let schema = loadSchemaFromFile(path.join(appRoot, 'openapi/schemas/' + entityName + '.yml'));
  schema = schema.components.schemas[modelName];
  addPropertyFakers(schema, propertyFakers);
  return schema;
}

const userSchema = makeStubSchema(
  'user',
  'BasicUserModel',
  {
    username: 'internet.userName',
    email: 'internet.email',
    first_name: 'name.firstName',
    last_name: 'name.lastName',
    image: 'internet.avatar',
    jti: 'datatype.uuid'
  }
);

const scenarioSchema = makeStubSchema(
  'scenario',
  'BasicScenarioModel',
  {
    title: 'commerce.productName',
    description: 'lorem.paragraph'
  }
);

const keywordSchema = {
  type: 'object',
  properties: {
    value: {
      type: 'string',
      faker: 'commerce.productAdjective'
    }
  }
};

const datasetSchema = makeStubSchema(
  'dataset',
  'BasicDatasetModel',
  {
    short_description: 'commerce.productName',
    title: 'commerce.productName'
  }
);

const projectSchema = makeStubSchema(
  'project',
  'BasicProjectModel',
  {
    title: 'commerce.productName',
    description: 'lorem.paragraph'
  }
);

const N_USERS = 60;
const N_DATASETS = 1;
const N_SCENARIOS = 1;
const N_PROJECTS = 50;
const N_MEMBERS = 5;
const N_IMPORTED_DATASETS = 5;
const N_IMPORTED_SCENARIOS = 5;
const N_KEYWORDS = 1;

async function generate () {
  /**
   * Function to create a schema with fakers properties
   * @method
   * @memberof Generate_Mock_Data
   * @param {object} schema - Schema
   * @param {number} n - Number to generate mock data
   * @returns {array} List of schemas with mock data
  */
  async function generateN (schema, n) {
    const stubs = [];
    for (let i = 0; i < n; i++) {
      stubs.push(jsf.generate(schema));
    }
    const items = await Promise.all(stubs);
    return items;
  }

  const [users, datasets, scenarios, keywords, projects] =
    await Promise.all([
      generateN(userSchema, N_USERS),
      generateN(datasetSchema, N_DATASETS),
      generateN(scenarioSchema, N_SCENARIOS),
      generateN(keywordSchema, N_KEYWORDS),
      generateN(projectSchema, N_PROJECTS)
    ]);

  return {
    users,
    datasets,
    scenarios,
    keywords,
    projects
  };
}

/**
 * Function to save users in database
 * @method
 * @memberof Generate_Mock_Data
 * @param {array} users - List of users to save
 * @returns {promise}
*/
async function createUsers (entities) {
  const { users } = entities;
  const promiseArray = users.map(user => {
    const data = {
      id: nanoid(),
      email: user.email,
      username: user.username,
      first_name: user.first_name,
      last_name: user.last_name,
      image: user.image,
      jti: user.jti,
      service_providers: {
        create: [{
          id: nanoid(),
          idService: 'datastore',
          name: 'Datastore',
          token: datastoreConfig.apiToken
        }]
      }
    };
    return prisma.user.create({
      data
    });
  });
  return Promise.all(promiseArray);
}

async function addUsersKeycloak (entities) {
  const uriAuth = global.config.services.keycloak;
  const token = await utils.getAccessTokenFromKeycloak('super_admin');
  uriAuth.uri.pathname = `/auth/admin/realms/${uriAuth.realm}/users`;
  uriAuth.uri.query = {
    username: uriAuth.users.super_admin.username
  };
  let user = await got(url.format(uriAuth.uri), { headers: { Authorization: `Bearer ${token.access_token}` } });
  user = JSON.parse(user.body);
  if (user.length) {
    user = user[0];
    const data = {
      id: user.id,
      email: user.email,
      username: user.username,
      first_name: user.firstName,
      last_name: user.lastName,
      jti: await faker.datatype.uuid(),
      service_providers: {
        create: [{
          id: nanoid(),
          idService: 'datastore',
          name: 'Datastore',
          token: datastoreConfig.apiToken
        }]
      }
    };
    await prisma.user.create({
      data
    });
    entities.users.push(data);
  }
}

async function createDatasets (entities) {
  const dataversIds = [datastoreConfig.privateDataverseID, datastoreConfig.userDataverseID, datastoreConfig.openDataverseID];
  const datasetsMeta = await client.listDatasets(dataversIds);
  const listIds = await Promise.all(
    datasetsMeta.map(async (dataset) => {
      const data = {
        id: `${dataset.protocol}:${dataset.authority}/${dataset.identifier}`
      };
      return await prisma.dataset.create({
        data
      });
    })
  );
  return listIds;
}

/**
 * Function to save scenarios in database
 * @method
 * @memberof Generate_Mock_Data
 * @param {array} scenarios - List of scenarios to save
 * @returns {promise}
*/
async function createScenarios (entities) {
  const uriScenarios = { ...global.config.services.risor.uri };
  const adminToken = await utils.getAccessTokenFromKeycloak('super_admin');
  uriScenarios.pathname = '/scenarios';
  let scenarios = await got(url.format(uriScenarios), { headers: { Authorization: `Bearer ${adminToken.access_token}` } });
  scenarios = JSON.parse(scenarios.body);
  const ids = new Set();
  scenarios = scenarios.filter(el => {
    const duplicate = ids.has(el.id);
    ids.add(el.id);
    return !duplicate;
  });
  entities.scenarios = scenarios;
  const promiseArray = scenarios.map(scenario => {
    const data = {
      id: scenario.id
    };
    return prisma.scenario.create({
      data
    });
  });
  return Promise.all(promiseArray);
}

/**
 * Function to save projects in database
 * @method
 * @memberof Generate_Mock_Data
 * @param {array} projects - List of projects to save
 * @param {array} keywords - List of keywords to associate a some projects
 * @returns {promise}
*/
async function createProjects (entities, connectJobs) {
  const { projects, scenarios, datasets, users } = entities;

  const promiseArray = projects.map(project => {
    // Random attributes
    const data = {
      id: nanoid(),
      title: project.title || faker.lorem.word(),
      short_description: project.description ? project.description.substring(0, 180) : faker.lorem.paragraph().substring(0, 180)
    };
    // Connected entities

    // Author
    const randomUser = _.sample(users);

    // Scenario
    const randomScenario = _.sample(scenarios);
    // Members
    const randomMembers = _.sampleSize(users, N_MEMBERS);
    _.remove(randomMembers, (m) => m.id === randomUser.id); // Remove author
    // Datasets
    const randomDatasets = _.sampleSize(datasets, N_IMPORTED_DATASETS);
    // Scenarios
    const randomScenarios = _.sampleSize(scenarios, N_IMPORTED_SCENARIOS);

    data.author = { connect: { id: randomUser.id } };
    // data.keywords = formatKeywords(randomKeywords);
    data.members = { connect: randomMembers.map((member) => ({ id: member.id })) };
    data.owners = { connect: randomMembers.map((member) => ({ id: member.id })) };
    data.datasets = { connect: randomDatasets.map((dataset) => ({ id: dataset.id })) };
    data.scenarios = { connect: randomScenarios.map((scenario) => ({ id: scenario.id })) };
    data.main_scenario = { connect: { id: randomScenario.id } };
    data.state = { connect: { slug: 'ready' } };
    if (connectJobs) {
      data.jobs = {
        connectOrCreate: {
          where: { id: nanoid() },
          create: {
            id: nanoid(),
            slug: `${project.title}-${data.scenarios.connect[0].id}-1.0.0`,
            state: {
              connect: { slug: 'not-ready' }
            },
            scenario: {
              connect: { id: data.scenarios.connect[0].id }
            },
            author: data.author
          }
        }
      };
    }
    return prisma.project.create({
      data
    });
  });
  return Promise.all(promiseArray);
}

/**
 * Function to start generate mock data
 * @method
 * @memberof Generate_Mock_Data
*/
async function init (connectJobs = true) {
  const entities = await generate();
  fs.writeFileSync(path.join(appRoot, 'fixtures/stubs.json'), JSON.stringify(entities, null, 2));
  try {
    entities.users = await createUsers(entities);
    console.log('Users created');
    await addUsersKeycloak(entities);
    console.log('Users Keycloak added');
    entities.scenarios = await createScenarios(entities);
    console.log('Scenarios created');
    entities.datasets = await createDatasets(entities);
    console.log('Datasets created');
    entities.projects = await createProjects(entities, connectJobs);
    console.log('Projects created');
    return entities;
  } catch (e) {
    console.log(e);
    throw e;
  }
}

if (require.main === module) {
  init();
}

module.exports = init;

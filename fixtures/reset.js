const generate = require('./generate');
const appRoot = require('app-root-path').toString();

const config = require('config-lite')({
  config_basedir: appRoot,
  config_dir: 'config'
});
global.config = config;

async function randomData () {
  const connectJobs = false;
  await generate(connectJobs);
  process.exit(0);
}

module.exports = {
  randomData
};

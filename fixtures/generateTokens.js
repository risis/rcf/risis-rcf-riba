
const utils = require('../test/api/setup/utils.js');

async function getSuperAdminToken () {
  const request = await utils.getAccessTokenFromKeycloak('super_admin');
  console.log({ super_admin: request.access_token });
  return request;
}

(async () => {
  await getSuperAdminToken();
})();

module.exports = {
  extends: 'standard',
  root: true,
  env: {
    mocha: true
  },
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2018
  },
  rules: {
    semi: ['error', 'always']
  }
};
